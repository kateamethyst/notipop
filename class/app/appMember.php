<?php

/**
 * appGroup
 *
 * @package notipop
 * @author  Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016. 08. 16.
 */
class appMember
{
    /**
     * aRequest
     * @var array
     */
    private $aRequest;

    /**
     * oOpenapi
     * @var object
     */
    private $oOpenApi;

    /**
     * Construtor for appMember class
     * @param array     $aRequest       Request
     * @param object    $oOpenApi       Openapi Instance
     */
    public function __construct($aRequest, $oOpenApi)
    {
        $this->aRequest = $aRequest;
        $this->oOpenApi = $oOpenApi;
    }

    /**
     * Member Detail Inquiry
     * @param  array $aParams list of parameters
     * @return array          list of members
     */
    public function getMemberDetails($aParams)
    {
        $aParam = array(
            "data"      => "*",
            "condition" => array(
                'member_id' => $aParams['mall_id']
            )
        );
        return $this->oOpenApi->call(
            'member',
            'getlist',
            $aParam,
            'GET',
            2
        );
    }

    /**
     * Get age of member
     * @param  string $sDate Birthday of the member
     * @return integer          Age
     */
    private function getAge($sDate)
    {
        $sFrom = new DateTime($sDate);
        $sTo   = new DateTime('today');
        return $sFrom->diff($sTo)->y;
    }
    /**
     * Search Member Dummy Data
     * @param  array   $aParams   Filters
     * @return array
     */
    public function lookupMember($aParams)
    {
        $aParams = array(
            'member_id'           => 'devsdk0006',
            'regist_date'         => '2015-10-19 17:46:16.293881',
            'total_mileage'       => '0',
            'avail_mileage'       => '0',
            'is_news_mail'        => 'F',
            'sex'                 => 'female',
            'c_name'              => '대표 관리자',
            'c_email'             => 'test@naver.com',
            'wedding_anniversary' => '',
            'birthday'            => '1995-07-28',
            'age'                 => $this->getAge('1995-07-28'),
            'c_phone'             => '02-111-11'
        );
        return $aParams;
        // $aParam = array(
        //     "data"      => "*",
        //     "condition" => array(
        //         'member_id' => $aParams['mall_id']
        //     )
        // );
        // $aMemberDetails = $this->oOpenApi->call(
        //     'member',
        //     'getdetail',
        //     $aParam,
        //     'GET',
        //     2
        // );
        // $aMember = array_map(function($aDetails) {
        //     return array(
        //         'member_id'    => $aDetails['result']['info']['member_id'],
        //         'regist_date'  => $aDetails['result']['info']['regist_date'],
        //         'sex'          => $aDetails['result']['info']['sex'],
        //         'name'         => $aDetails['result']['info']['c_name'],
        //         'email'        => $aDetails['result']['info']['c_email'],
        //         'shop_no'      => $aDetails['result']['info']['shop_no'],
        //         'subscription' => $aDetails['result']['info']['is_news_mail'],
        //         'phone'        => $aDetails['result']['info']['c_phone']
        //     );
        // }, $aMemberDetails);
        // return $aMember['response'];
    }
}
