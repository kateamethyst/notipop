<?php

/**
 * appNoti
 * @package notipop
 * @author  hjung01 <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016. 07. 28.
 */
class appNoti
{

    /**
     * Instance of modelPackage
     * @var object
     */
    private $oModel;

    /**
     * Date
     * @var String
     */
    private $sDate;

    /**
     * Instantiation of model
     * @param modelPackage $oModel object
     */
    public function __construct(modelNoti $oModel)
    {
        $this->oModel = $oModel;
        $this->sDate = date('Y-m-d');
    }

    public function getTotalCount()
    {
        return $this->oModel->getNotificationTotalCount();
    }

    /**
     * Get list of notifications
     * @return array            list of notifications
     */
    public function getNotifications()
    {
        return $this->oModel->getNotificationList();
    }

    /**
     * Paginate notifications
     * @param  int      $iRows          no rows to be displayed
     * @param  int      $iPage          current page of pagination
     * @param  int      $iTotalCount    total count of notifications
     * @return array                    list of paginated notifications
     */
    public function paginateNotifications($iRows, $iPage, $iTotalCount)
    {
        if (libValid::isNumeric($iRows) === true && libValid::isNumeric($iPage) === true && libValid::isNumeric($iTotalCount) === true) {
            return $this->oModel->getNotificationInDesc($iRows, $iPage, $iTotalCount);
        }
        return false;
    }

    /**
     * Get specific details of notiifications according to its filter
     * @param  array    $aFilter    List of filters
     * @return array                Search result
     */
    public function getDetails($aFilter)
    {
        if (libValid::isArray($aFilter) === false) {
            return false;
        }

        $aResult = $this->oModel->getFilteredNotification($aFilter);
        if (libValid::isArray($aResult) === false) {
            return array();
        }
        return $aResult;
    }

    /**
     * Get the inde of a specific notification
     * @param  array    $aSequence      Sequence number of the notification
     * @return int                      index number of the notification
     */
    public function getNotiIndex($aSequence)
    {
        if (libValid::isArray($aSequence) === false) {
            return false;
        }

        return $this->oModel->getNotificationIndex($aSequence);
    }

    /**
     * Get the last ro in notifications
     * @return array            last row in notification list
     */
    public function getLastRow()
    {
        $aResult = $this->oModel->getLastRow();

        if (libValid::isArray($aResult) === false) {
            return array();
        }

        return $aResult;
    }

    /**
     * Add notification in noti_list
     * @param  array     $aParams   Details of new notification]
     * @return boolean               Response
     */
    public function addNotification($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return false;
        }

        $aData = array(
            'name'       => $aParams['name'],
            'position'   => $aParams['position'],
            'connection' => $aParams['connection'],
            'page'       => $aParams['page'],
            'date'       => array(
                'start_date' => $aParams['srh_start_dt'],
                'end_date'   => $aParams['srh_end_dt']
            ),
            'time'       => array(
                'start_time' => $aParams['start_time1'] . ':' . $aParams['start_time2'],
                'end_time'   => $aParams['end_time1'] . ':' . $aParams['start_time2']
            ),
            'group_no'   => (int)$aParams['group_no'],
            'type'       => $aParams['type'],
            'popup'      => array(
                'title'      => $aParams['title'],
                'contents'   => $aParams['description'],
                'coupon_no'  => $aParams['coupon_no'],
                'product_no' => (int)$aParams['product_num']
            ),
            'use'        => true,
            'timestamps' => $this->sDate
        );

        return $this->oModel->addNotification($aData);
    }

    /**
     * Update notification status
     * @param  array        $aParams    Details of the notification
     * @return boolean                       Response
     */
    public function updateStatus($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return false;
        }

        $aSequence = array(
            'seq' => (int)$aParams['seq']
        );

        $aNotiData = $this->getDetails($aSequence);

        $aNotiData[0]['use'] = (boolean)$aParams['use'];

        $iIndex = $this->oModel->getNotificationIndex($aSequence);

        return $this->oModel->updateNotification($iIndex, $aNotiData[0]);
    }

    /**
     * Update notification
     * @param  array       $aNotiDetails       Modified notification details
     * @return boolean                         Response
     */
    public function updateNotification($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return false;
        }

        $aData = array(
            'name'       => $aParams['name'],
            'position'   => $aParams['position'],
            'connection' => $aParams['connection'],
            'page'       => $aParams['page'],
            'date'       => array(
                'start_date' => $aParams['srh_start_dt'],
                'end_date'   => $aParams['srh_end_dt']
            ),
            'time'       => array(
                'start_time' => $aParams['start_time1'] . ':' . $aParams['start_time2'],
                'end_time'   => $aParams['end_time1'] . ':' . $aParams['start_time2']
            ),
            'group_no'   => (int)$aParams['group_no'],
            'type'       => $aParams['type'],
            'popup'      => array(
                'title'      => $aParams['title'],
                'contents'   => $aParams['description'],
                'coupon_no'  => $aParams['coupon_no'],
                'product_no' => (int)$aParams['product_num']
            ),
            'use'        => (bool)$aParams['use'],
            'timestamps' => $aParams['timestamps'],
            'seq'        => (int)$aParams['seq']
        );

        $aSequence = array(
            'seq' => (int)$aParams['seq']
        );

        $iIndex = $this->oModel->getNotificationIndex($aSequence);

        return $this->oModel->updateNotification($iIndex, $aData);
    }

    /**
     * Delete Notification
     * @param  array       $aSequenceList        List of sequence to be deleted
     * @return boolean                           Response
     */
    public function deleteNotification($aSequenceList)
    {
        if (libValid::isArray($aSequenceList) === false) {
            return false;
        }

        foreach ($aSequenceList as $aSequence) {
            $this->oModel->deleteNotification(array('seq' => (int)$aSequence));
        }

        return true;
    }

    public function getCachePopup($aGroupNos)
    {
        // $aGroupNos = array(
        //     'group_no' => array(
        //         2,
        //         3
        //     )
        // );
        if (libValid::isArray($aGroupNos) === false) {
            return array();
        }

        $aCachePopup = $this->oModel->getCachePopup();

        if (libValid::isArray($aCachePopup) === false) {
            $this->oModel->clearCache();
            $aCachePopup = $this->getPopup();
        }

        $aNotifications = array();

        foreach ($aGroupNos['group_no'] as $iGroupNo) {
            $aNoti = $this->oModel->getFilteredCachePopup(array('group_no' => (int)$iGroupNo));
            if (libValid::isArray($aNoti) === true) {
                $aNotifications[] = $aNoti;
            }
        }

        return $aNotifications[0];
    }

    /**
     * Get popup
     * @param  array   $aGroupNos    list of groupnos
     * @return array                 list of popups
     */
    public function getPopup()
    {

        $aNotifications = $this->oModel->getNotificationList();
        if (libValid::isArray($aNotifications) === false) {
            return array();
        }

        $aValidNoti = $this->validateNoti($aNotifications);

        //array map $aValidNoti so that hen we saved it to redis the seq will not override the notification sequences
        $aValidNoti = array_map( function($aNoti){
            return array(
                'noti_id'  => (int)$aNoti['seq'],
                'position' => $aNoti['position'],
                'login'    => $aNoti['connection'],
                'page'     => $aNoti['page'],
                'date'     => $aNoti['date'],
                'time'     => $aNoti['time'],
                'group_no' => (int)$aNoti['group_no'],
                'popup'    => $aNoti['popup'],
                'use'      => (bool)$aNoti['use'],
                'type'     => $aNoti['type']
            );
        }, $aValidNoti);

        if (libValid::isArray($aValidNoti) === false) {
            return array();
        }

        $aPopup = $this->setAddons($aValidNoti);

        $aCachePopup = $this->cachePopup($aPopup);

        $this->setCachePopupExpiration($aCachePopup);

        // return $aCachePopup;
        return $aCachePopup;
    }


    /**
     * Validate the notificaiton
     * @param  array    $aNotifications   list of notifications
     * @return array                      List of validated notifications
     */
    private function validateNoti($aNotifications)
    {
        $aValidatedNoti = array();
        $iCounter = 0;
        foreach ($aNotifications as $aNotification) {
            if (strtotime($aNotification['date']['start_date']) >= $this->sDate && $this->sDate <= $aNotification['date']['end_date'] && $aNotification['use'] === true) {
                $aValidatedNoti[$iCounter] =  $aNotification;
                $iCounter++;
            }
        }

        return $aValidatedNoti;
    }

    /**
     * Set addons (product, coupon) in popup
     * @param   array   $aValidatedNoti  List of validated notification
     * @return  array                    Popup notifications with their specific addons
     */
    private function setAddons($aValidatedNoti)
    {
        foreach ($aValidatedNoti as $iKey => $aNoti) {
            $aAddOns = $this->getAddons($aNoti);
            $aValidatedNoti[$iKey]['add_ons'] =  $aAddOns[0];
        }

        return $aValidatedNoti;
    }

    /**
     * Get addons for popup
     * @param  array  $aNoti   Notification
     * @return array           Notifcation and its addons
     */
    private function getAddons($aNoti)
    {
        if ($aNoti['type'] === 'Advertisement') {
            return $this->oModel->getProductDetails(array('product_no' => (int)$aNoti['popup']['product_no']));
        } else if ($aNoti['type'] === 'Coupon') {
            return $this->oModel->getCouponDetails(array('coupon_no' => $aNoti['popup']['coupon_no']));
        } else {
            return array();
        }
    }

    /**
     * Cache popup in Redis
     * @param  array $aPopups  List of popup that are needed to be cache
     * @return array           List of cache popup
     */
    private function cachePopup($aPopups)
    {
        foreach ($aPopups as $iKey => $aPopup) {
            $this->oModel->addPopup($aPopup);
        }

        return $this->oModel->getCachePopup();
    }

    /**
     * Set Expiration in cache popup
     * @param String $sKey Redis key of cached popup html
     */
    private function setCachePopupExpiration()
    {
        $this->oModel->setCacheExpiration();
    }

    /**
     * Clear Cache
     * @return boolean          response
     */
    public function clearCache()
    {
        return $this->oModel->clearCache();
    }
}
