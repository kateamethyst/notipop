<?php

/**
 * appProduct
 * @package notipop
 * @author  Ma <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016. 07. 28.
 */
class appProduct
{

    /**
     * Model
     * @var object
     */
    private $oModel;

    /**
     * appProduct constructor
     * @param modelProduct $oModel object
     */
    public function __construct(modelProduct $oModel)
    {
        $this->oModel = $oModel;
    }

    /**
     * Get list of products
     * @return array
     */
    public function getProducts()
    {
        return $this->oModel->getProductList();
    }

    /**
     * Check if the product is already existing
     * @param  array    $aProductNo     product number
     * @return boolean                  Response if the product is existing
     */
    private function checkProductExistence($aProductNo)
    {
        if (libValid::isArray($aProductNo) === false) {
            return false;
        }

        $mResult = $this->oModel->getFilteredProduct($aProductNo);

        if (libValid::isArray($mResult) === false) {
            return false;
        }

        return true;
    }

    /**
     * Get Product details according to filter
     * @param  Array $aFilter List of filters
     * @return Array
     */
    public function getProductData($aFilter)
    {
        if (libValid::isArray($aFilter) === false) {
            return false;
        }
        return $this->oModel->getFilteredProduct($aFilter);
    }

    /**
     * Add product
     * @param  array    $aProduct      Product
     * @return array
     */
    public function addProduct($aProduct)
    {
        if (libValid::isArray($aProduct) === false) {
            return false;
        }

        $aProduct['product_price'] = number_format((double)$aProduct['product_price'], 2);

        $aProductNo = array(
            'product_no' => $aProduct['product_no']
        );

        $mCheckResult = $this->checkProductExistence($aProductNo);

        if ($mCheckResult === true) {
            return false;
        }

        $this->oModel->addProduct($aProduct);

        return $aProduct;
    }

    /**
     * Delete products that are selected for
     * @param  array    $aSequenceList  List of sequence
     * @return boolean                  Response of deleteProducts
     */
    public function deleteProducts($aSequenceList)
    {
        if (libValid::isArray($aSequenceList) === false) {
            return false;
        }

        foreach ($aSequenceList as $aSequence) {
            $this->oModel->deleteProduct(array('product_no' => $aSequence));
        }

        return $aSequenceList;
    }

    /**
     * Delete product key
     * @return boolean
     */
    public function deleteProductKey()
    {
        return $this->oModel->deleteProductKey();
    }
}
