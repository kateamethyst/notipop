<?php

/**
 * appGroup
 * @package notipop
 * @author  Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016. 08. 18.
 */
class appGroup
{

    /**
     * Model object
     * @var object
     */
    private $oModel;

    /**
     * Constructor for appGroup
     * @param modelPackage $oModel object
     */
    public function __construct(modelGroup $oModel)
    {
        $this->oModel = $oModel;
    }

    public function getGroupTotalCount()
    {
        return $this->oModel->getTotalGroupCount();
    }
    /**
     * Get grop list
     * @return array      List of group criteria
     */
    public function getGroupList()
    {
        $aGroups = $this->oModel->getGroupList();

        return array_map(function($aGroups) {
            return array(
                'seq'         => (int)$aGroups['seq'],
                'name'        => $aGroups['name'],
                'criteria'    => $aGroups['criteria'],
                'description' => $aGroups['description']
            );
        }, $aGroups);

    }

    /**
     * Get group details 
     * @param  array     $aFilter     List of filters
     * @return array
     */
    public function getGroup($aFilter)
    {
       return $this->oModel->getFilteredGroup($aFilter);
    }

    /**
     * Add new group criteria
     * @param Array $aGroup Group
     */
    public function addGroupCriteria($aGroup)
    {
        $this->oModel->addGroup($aGroup);
        return $this->oModel->getlastGroupRow();

    }

    public function removeGroup($aGroup)
    {
        $aFilter = array(
            'seq' => (int)$aGroup['seq']
        );
        return $this->oModel->deleteGroup($aFilter);
    }
    /**
     * Delete all group criteria
     * @return Boolean  Response
     */
    public function deleteGroup()
    {
        return $this->oModel->deleteGroupKey();
    }
}
