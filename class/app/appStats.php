<?php

/**
 * appStats
 * @package notipop
 * @author  Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016. 08. 22.
 */
class appStats
{
    /**
     * oModel
     * @var object
     */
    private $oModel;

    /**
     * appStats constructor
     * @param modelStats $oModel
     */
    public function __construct(modelStats $oModel)
    {
        $this->oModel = $oModel;
    }

    /**
     * Get list of stats
     * @return array
     */
    public function getStatsList()
    {
        return $this->oModel->getStatsList();
    }

    /**
     * Get stats according to given data
     * @param  array  $aFilter   list of filter
     * @return array             list of stats
     */
    public function getStatsData($aFilter)
    {
        if (libValid::isArray($aFilter) === false) {
            return array();
        }

        return $this->oModel->getFilteredStats($aFilter);

    }

    /**
     * Get statistic summary
     * @param  array  $aParams parameters
     * @return array
     */
    public function getStatsSummary($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return array();
        }

        $aNotiID = array(
            'noti_id' => (int)$aParams['noti_id']
        );

        $aStats = $this->oModel->getFilteredStats($aNotiID);

        if (libValid::isArray($aStats) === false) {
             return array(
                'noti_id'  => (int)$aParams['noti_id'],
                'click'    => 0,
                'view'     => 0,
                'download' => 0
            );
        }

        array_walk_recursive($aStats, function($item, $key) use (&$aSum){
            $aSum[$key] = $aSum[$key] ? $item + $aSum[$key] : $item;
        });

        return array_map(function($aStat){
            return array(
                'noti_id'  => $aStat['noti_id'],
                'click'    => libValid::isNull((int)$aStat['click']) === true ? 0 : (int)$aStat['click'],
                'view'     => libValid::isNull((int)$aStat['view']) === true ? 0 : (int)$aStat['view'],
                'download' => libValid::isNull((int)$aStat['download']) === true ? 0 : (int)$aStat['download']
            );
        }, $aStats);
    }

    /**
     * Add statisstic for specific notification
     * @param array $aParams Parameters for adding stats ['noti_id', options ('click, view, download'), count]
     */
    public function addStats($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return false;
        }

        $aFilters = array(
            'date'    => date('Y-m-d'),
            'noti_id' => (int)$aParams['noti_id']
        );

        $aCheckStats = $this->oModel->getFilteredStats($aFilters);

        if (libValid::isArray($aCheckStats) === true) {
            $aSeq = array(
                'seq' => (int)$aCheckStats[0]['seq']
            );

            $aData = array(
                'date'     => $aCheckStats[0]['date'],
                'view'     => (int)$aCheckStats[0]['view'] + (int)$aParams['view'],
                'click'    => (int)$aCheckStats[0]['click'] + (int)$aParams['click'],
                'download' => (int)$aCheckStats[0]['download'] + (int)$aParams['download'],
                'noti_id'  => (int)$aParams['noti_id'],
                'seq'      => $aCheckStats[0]['seq']
            );

            $iIndex = $this->oModel->getStatsIndex($aSeq);

            return $this->oModel->updateStats($iIndex, $aData);

        }

        $aData = array(
            'date'     => date('Y-m-d'),
            'view'     => (libValid::isNull($aParams['view'])) === true ? 0 : 1,
            'click'    => (libValid::isNull($aParams['click'])) === true ? 0 : 1,
            'download' => (libValid::isNull($aParams['download'])) === true ? 0 : 1,
            'noti_id'  => (int)$aParams['noti_id']
        );

        return $this->oModel->addStats($aData);
    }

    /**
     * Delete Stats
     * @return boolean
     */
    public function deleteStats()
    {
        return $this->oModel->deleteStats();
    }
}
