<?php

/**
 * appCoupon
 * @package notipop
 * @author  hjung01 <hjung01@simplexi.com>, Ma. Angelica B. Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016. 08. 01.
 */
class appCoupon
{

    /**
     * openapi object
     *
     * @var object
     */
    private $oOpenApi;

    /**
     * appCoupon constructor.
     *
     * @param obejct $oOpenApi
     * @param array  $aRequest
     */
    public function __construct($oOpenApi, modelCoupon $oModel)
    {
        $this->oOpenApi = $oOpenApi;
        $this->oModel = $oModel;
    }

    /**
     * Get coupon list
     * @param  Array $aShopNo Shop Number
     * @return Array         List of cache coupon
     */
    public function getCouponList($aShopNo)
    {
        if (libValid::isArray($aShopNo) === false) {
            return false;
        }
        return $this->loadCoupon($aShopNo);
    }

    /**
     * Load Coupon and cache it in redis
     * @param  Array $aShopNo Shop Number
     * @return Array         List of cache coupon
     */
    public function loadCoupon($aShopNo)
    {
        $this->oModel->deleteCouponKey();
        $aCouponList = $this->oOpenApi->call('coupon', 'list', $aShopNo, 'GET', 2);

        if (libValid::isArray($aCouponList['response']['result']) === false) {
            return array();
        }

        foreach ($aCouponList['response']['result'] as $mValue) {

            $aCoupon = array(
                'coupon_no'         => $mValue['coupon_no'],
                'coupon_name'       => $mValue['coupon_name'],
                'coupon_direct_url' => $mValue['coupon_direct_url']
            );

            $this->addNewCoupon($aCoupon);
        }

        return $this->oModel->getCouponList();
    }

    /**
     * Add new coupon in coupon list
     * @param Array $aCoupon Coupon
     */
    private function addNewCoupon($aCoupon)
    {
        if (libValid::isArray($aCoupon) === false) {
            return false;
        }

        return $this->oModel->addCoupon($aCoupon);
    }

    /**
     * Get Coupon Details
     * @param  Array $aFilter List of filters
     * @return Array          list of coupon according to the filter
     */
    public function getCouponData($aFilter)
    {
        if (libValid::isArray($aFilter) === false) {
            return false;
        }

        return $this->oModel->getFilteredCoupon($aFilter);
    }

    /**
     * Set expiration in coupon list
     */
    private function setCouponExpiration()
    {
        return $this->oModel->setCouponExpiration();
    }
}
