<?php

/**
 * appFlags
 * @package notipop
 * @author  Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016. 08. 22.
 */
class appFlags
{
    /**
     * oModel
     * @var object
     */
    private $oModel;

    /**
     * appStats constructor
     * @param modelFlags $oModel
     */
    public function __construct(modelFlags $oModel)
    {
        $this->oModel = $oModel;
    }


    /**
     * Get flags list
     * @return array
     */
    public function getFlagList()
    {
        return $this->oModel->getFlagList();
    }

    /**
     * Get Flags Data
     * @param  array     $aFilter       Filters ['seq|noti_id|date']
     * @return array
     */
    public function getFlagsData($aFilter)
    {
        if (libValid::isArray($aFilter) === false) {
            return array();
        }

        return $this->oModel->getFilteredFlags($aFilter);

    }

    /**
     * Add flag
     * @param array $aParams    Parameters ('member_id, noti_id, downloaded, hide(boolean), display_at(date)')
     */
    public function addFlags($aParams)
    {
        if (libValid::isArray($aParams) === false) {
            return array();
        }

        $aFilters = array(
            'member_id' => $aParams['mall_id'],
            'noti_id'   => (int)$aParams['noti_id']
        );

        $aCheckFlags = $this->oModel->getFilteredFlags($aFilters);

        if (libValid::isArray($aCheckFlags) === true) {
            $aData = array(
                'member_id'  => $aParams['mall_id'],
                'noti_id'    => (int)$aParams['noti_id'],
                'downloaded' => ($aParams['downloaded']) ? (bool)$aParams['downloaded'] : $aCheckFlags[0]['downloaded'],
                'hide'       => ((bool)$aParams['hide']) ? $aParams['hide'] : $aCheckFlags[0]['hide'],
                'display_at' => ($aParams['display_at']) ? $aParams['display_at'] : $aCheckFlags[0]['display_at'],
                'seq'        => $aCheckFlags[0]['seq']
            );

            $iIndex = $this->oModel->getFlagIndex(array('seq' => $aCheckFlags[0]['seq']));

            return $this->oModel->updateFlags($iIndex, $aData);
        }

        $aData = array(
            'member_id'  => $aParams['mall_id'],
            'noti_id'    => (int)$aParams['noti_id'],
            'downloaded' => $aParams['downloaded'] ? (boolean)$aParams['downloaded'] : false,
            'hide'       => (boolean)$aParams['hide'] ? (boolean)$aParams['hide'] : false,
            'display_at' => $aParams['display_at']
        );

        return $this->oModel->addFlags($aData);
    }

    /**
     * Delete Flags
     * @return boolean
     */
    public function deleteFlags()
    {
        return $this->oModel->deleteFlags();
    }
}
