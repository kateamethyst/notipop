<?php

/**
 * appAddProduct
 *
 * @package appAddProduct
 * @author  hjung01 <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016. 08. 02.
 */
class appAddProduct
{
    /**
     * request info
     *
     * @var array
     */
    private $aRequest;

    /**
     * openapi object
     *
     * @var object
     */
    private $oOpenApi;

    /**
     * appAddProduct constructor.
     *
     * @param obejct $oOpenApi
     * @param array  $aRequest
     */
    public function __construct($oOpenApi, $aRequest)
    {
        $this->oOpenApi = $oOpenApi;
        $this->aRequest = $aRequest;
    }

    /**
     * 화폐단위 반환 함수
     *
     * @param int $iShopNo
     * @return string
     */
    public function getCurrency($iShopNo)
    {
        if (libValid::isNumeric($iShopNo) === false) {
            return null;
        }
        $aShop = $this->getMultiShopInfo((int)$iShopNo);
        if ($aShop === false) {
            return null;
        } else {
            return $aShop['sh_currency'];
        }
    }

    /**
     * 멀티샵 정보 조회
     *
     * @param int $iShopNo
     * @return mixed
     */
    public function getMultiShopInfo($iShopNo)
    {
        if (libValid::isNumeric($iShopNo) === false) {
            $aShopInfo = $this->oOpenApi->call('shop', 'list');
            $bArrange = true;
        } else {
            $aShopInfo = $this->oOpenApi->call('shop', 'detail', array('shop_no' => $iShopNo));
            $bArrange = false;
        }

        return libValid::capiReturnValid($aShopInfo, $bArrange);
    }

    /**
     * 제조사브랜드트렌드 리스트함수
     *
     * @param array $aParam
     * @return array $aApiListResult
     */
    public function getEtcSearchCategoryList($aParam)
    {
        $aCategoryMap = array(
            'manufacturer_code' => array(
                'method' => 'searchmanufacturer',
                'column' => array(
                    'manufacturer_code',
                    'manufacturer_name'
                )
            ),
            'supplier_id'       => array(
                'method' => 'searchsupplier',
                'column' => array(
                    'supplier_code',
                    'supplier_name'
                )
            ),
            'brand_code'        => array(
                'method' => 'searchbrand',
                'column' => array(
                    'brand_code',
                    'brand_name'
                )
            ),
            'trend_code'        => array(
                'method' => 'searchtrend',
                'column' => array(
                    'trend_code',
                    'trend_name'
                )
            )
        );
        $aResult = array();
        if (libValid::isArray($aParam['searchCategory']) === true) {
            foreach ($aParam['searchCategory'] as $iKey => $sVal) {
                if ($aCategoryMap[$sVal]['column'] === null) {
                    continue;
                }
                $aApiParam = array(
                    'data'      => $aCategoryMap[$sVal]['column'],
                    'condition' => array(
                        '*'
                    ),
                    'config'    => array(
                        'limit'  => 10,
                        'offset' => 0
                    )
                );
                $aApiListResult = $this->oOpenApi->call('product', $aCategoryMap[$sVal]['method'], $aApiParam, 'POST', 2);
                // capi valid 체크
                $aValidCapi = libValid::capiReturnValid($aApiListResult, true);
                $aResult[$sVal] = $aValidCapi;
            }
        }

        return $aResult;
    }

    /**
     * 카테고리 tree형으로 만드는 함수
     *
     * @return $aCate
     */
    public function getCategoryTree()
    {
        $aParam = array(
            'shop_no' => 1,
            'is_display',
            'T'
        );
        $aCategory = $this->oOpenApi->call('category', 'detail', $aParam);

        $aValidCapiResult = libValid::capiReturnValid($aCategory, true);
        if ($aValidCapiResult === false) {
            return false;
        }
        $aCategoryList = $aCategory['meta'];
        $aCategoryList['data'] = $aCategory['response']['result'];

        return libCategory::getTree($aCategoryList);
    }

    /**
     * 검색 결과 capi
     *
     * @param array $aParam
     * @return array $aResult
     */
    public function getSearchProductApi($aParam)
    {
        // searchparam 가공
        $aSearchParam = $this->_makeSearchParam($aParam);
        $aCondition = $this->_makeSubCondition($aSearchParam);
        // 카테고리 부분 v2 api 호출
        $iOffset = ($aSearchParam['page'] - 1) * $aSearchParam['limit'];
        $aPrdParam = array(
            'data'      => array(
                'product_code',
                'product_name',
                'category',
                'regist_date',
                'is_display',
                'eng_product_name',
                'purchase_product_name',
                'product_price',
                'manufacturer_code',
                'trend_code',
                'brand_code',
                'supplier_id',
                'origin_place_code',
                'product_weight',
                'is_set_product',
                'regist_date',
                'modify_date',
                'product_tag',
                'product_status',
                'model_name',
                'self_item_code',
                'self_product_code',
                'item_code',
                'option',
                'tiny_image',
                'small_image',
                'list_image',
                'detail_image',
                'set_discount',
                'benefit'
            ),
            'shop_no'   => $aSearchParam['shop_no'],
            'condition' => $aCondition,
            'config'    => array(
                'limit'  => $aSearchParam['limit'],
                'offset' => $iOffset
            )
        );
        $aApiResult = $this->oOpenApi->call('product', 'search', $aPrdParam, 'POST', 2);
        $aValidCapiResult = libValid::capiReturnValid($aApiResult, true);
        if ($aValidCapiResult === false) {
            return false;
        }
        $aResult = array();
        foreach ($aValidCapiResult as $iKey => $sVal) {
            $aResult[$iKey]['product_name'] = $sVal['product_name'][1];
            $aResult[$iKey]['product_no'] = $sVal['product_no'];
            $aResult[$iKey]['product_code'] = $sVal['product_code'];
            $aResult[$iKey]['is_set_product'] = $sVal['is_set_product'];
            $aResult[$iKey]['option'] = $sVal['option'];
            $aResult[$iKey]['tiny_image'] = $sVal['tiny_image'];
            $aResult[$iKey]['small_image'] = $sVal['small_image'];
            $aResult[$iKey]['list_image'] = $sVal['list_image'];
            $aResult[$iKey]['detail_image'] = $sVal['detail_image'];
            $aResult[$iKey]['category'] = $sVal['category'] === null ? array() : $sVal['category'];
            $aResult[$iKey]['product_price'] = is_numeric($sVal['product_price'][$aSearchParam['shop_no']]) === false ? 0 : $sVal['product_price'][$aSearchParam['shop_no']];
        }
        // 사진 파일 를 위해서 $aResult 가공
        $aResult = $this->_getProductClassification($aResult);
        $aResult = $this->_makeImageSrc($aResult);
        $aSearchResult['totalcount'] = $aApiResult['response']['total_count'];
        $aSearchResult['list'] = $aResult;
        return $aSearchResult;
    }

    private function _getProductClassification($aSearchParam)
    {
        if (libValid::isArray($aSearchParam) === false) {
            return array();
        }

        foreach ($aSearchParam as $iKey => $aParam) {
            $aCategoryDetails = $this->oOpenApi->call(
                'category',
                'search',
                array(
                    'category_no' => (int)key($aParam['category'])
                ),
                'GET',
                2
            );

            $sProductClassification = $this->_concatFullCategoryName($aCategoryDetails['response']['result'][0]);
            $aSearchParam[$iKey]['product_classification'] =  $sProductClassification;
        }
        return $aSearchParam;
    
    }

    private function _concatFullCategoryName($aCategory)
    {
        $sProdClass = '';
        for ($i=1; $i <= $aCategory['category_depth']; $i++) { 
            $sProdClass = $sProdClass . $aCategory['full_category_name'][$i];
        }

        return $sProdClass;
    }
   
    /**
     * capi 조건 만들어주는함수
     *
     * @param array $aSearchParam
     * @return mixed
     */
    private function _makeSubCondition($aSearchParam)
    {
        $aSearchName = $aSearchParam['search'];
        $sCategoryNum = $aSearchParam['categorynum'];
        $aCondition = array();
        // 검색어가 있을때
        if ($this->_isSearchValueNull($aSearchName) === false) {
            // and or 다중 검색 처리 한 것
            if (libValid::isArray($aSearchParam['searchCategory']) === true) {
                foreach ($aSearchParam['searchCategory'] as $iSearchCateIndex => $sSearchCateValue) {
                    $aCondition[$aSearchParam['searchCategory'][$iSearchCateIndex]][] = $aSearchName[$iSearchCateIndex];
                }
            }
        }
        // 옵션 처리 부분
        if ($aSearchParam['is_sellingT'] !== null && $aSearchParam['is_sellingF'] === null) {
            $aCondition['is_selling'] = 'T';
        } else if ($aSearchParam['is_sellingT'] === null && $aSearchParam['is_sellingF'] !== null) {
            $aCondition['is_selling'] = 'F';
        }
        // display처리부분
        if ($aSearchParam['is_displayT'] !== null && $aSearchParam['is_displayF'] === null) {
            $aCondition['is_display'] = 'T';
        } else if ($aSearchParam['is_displayT'] === null && $aSearchParam['is_displayF'] !== null) {
            $aCondition['is_display'] = 'F';
        }
        // 카테고리가 있는경우만
        if (libValid::isString($sCategoryNum) === true && $sCategoryNum !== null) {
            $aCondition['category'] = $sCategoryNum;
            // 하위 포함 버튼눌렀을때
            if ($aSearchParam['is_sub_category'] === 'T') {
                $aCondition['is_sub_category'] = 'T';
            }
        }
        // 날짜가 존재한다 .
        if ($aSearchParam['startDate'] !== null && $aSearchParam['endDate'] !== null) {
            $aCondition[$aSearchParam['DateType']] = array(
                'startDate' => $aSearchParam['startDate'],
                'endDate'   => $aSearchParam['endDate']
            );
            // 날짜가 없는경우
        } else if ($aSearchParam['startDate'] === null && $aSearchParam['endDate'] === null) {
            return false;
        }
        return $aCondition;
    }

    /**
     * 검색어가 전부 널인지 체크
     *
     * @param array $aSearchName
     * @return boolean
     */
    private function _isSearchValueNull($aSearchName)
    {
        if (libValid::isArray($aSearchName) === false) {
            return true;
        }
        foreach ($aSearchName as $sVal) {
            if (libValid::isString($sVal) === true) {
                return false;
            }
        }
        return true;
    }

    /**
     * 검색을 위한 파람을 만들어주는 함수
     *
     * @param array $aParam
     * @return array $aSearchParam
     */
    private function _makeSearchParam($aParam)
    {
        $sLastDepth = null;
        $aDepthList = array($aParam['depth4'], $aParam['depth3'], $aParam['depth2'], $aParam['depth1']);
        foreach ($aDepthList as $sDepth) {
            if ($sDepth !== 'null') {
                $sLastDepth = $sDepth;
                break;
            }
        }
        $aSearchParam = array(
            'startDate'       => $aParam['srh_start_dt'],
            'endDate'         => $aParam['srh_end_dt'],
            'page'            => $aParam['page'],
            'search'          => $aParam['search'],
            'searchCategory'  => $aParam['searchCategory'],
            'DateType'        => $aParam['DateType'],
            'limit'           => $aParam['limit'],
            'is_sellingT'     => $aParam['is_sellingT'],
            'is_sellingF'     => $aParam['is_sellingF'],
            'is_displayT'     => $aParam['is_displayT'],
            'is_displayF'     => $aParam['is_displayF'],
            'is_sub_category' => $aParam['is_sub_category'],
            'shop_no'         => $aParam['shop_no'],
            'categorynum'     => $sLastDepth
        );
        return $aSearchParam;
    }

    /**
     * $aSearchData 가공 img로 쓰일데이타
     *
     * @param array $aSearchData
     * @return array $aSearchData
     */
    private function _makeImageSrc($aSearchData)
    {
        $aUrlSet = array(
            'tiny_image'   => 'http://' . $this->aRequest['mall_id'] . '.cafe24.com/web/product/tiny/',
            'small_image'  => 'http://' . $this->aRequest['mall_id'] . '.cafe24.com/web/product/small/',
            'list_image'   => 'http://' . $this->aRequest['mall_id'] . '.cafe24.com/web/product/medium/',
            'detail_image' => 'http://' . $this->aRequest['mall_id'] . '.cafe24.com/web/product/big/'
        );

        if (libValid::isArray($aSearchData) === true) {
            foreach ($aSearchData as $iKey => $aVal) {
                // tiny 부터 존재하는 이미지만 사용하게 함
                if (libValid::isString($aVal['tiny_image']) === true) {
                    $aSearchData[$iKey]['thumb_image'] = $aUrlSet['tiny_image'] . $aVal['tiny_image'];
                } else if (libValid::isString($aVal['small_image']) === true) {
                    $aSearchData[$iKey]['thumb_image'] = $aUrlSet['small_image'] . $aVal['small_image'];
                } else if (libValid::isString($aVal['list_image']) === true) {
                    $aSearchData[$iKey]['thumb_image'] = $aUrlSet['list_image'] . $aVal['list_image'];
                } else if (libValid::isString($aVal['detail_image']) === true) {
                    $aSearchData[$iKey]['thumb_image'] = $aUrlSet['detail_image'] . $aVal['detail_image'];
                } else {
                    $aSearchData[$iKey]['thumb_image'] = '//img.echosting.cafe24.com/thumb/44x44.gif';
                }
            }
        }
        return $aSearchData;
    }
}
