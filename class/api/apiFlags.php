<?php

/**
 * Flags
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 22.
 * @version 1.0
 **/
class apiFlags extends apiCommon
{
    /**
     * Flags
     * @var object
     */
    public $oAppFlags;

    /**
     * Get request in Flags
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    public function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppFlags = libAppInstance::getAppFlags($this->oRedis);
        if ($aArgs['option'] === 'getUserFlags') {
            $mResult = $this->oAppFlags->getFlagsData(array('member_id' => $this->aRequest['mall_id']));
        } else {
            $mResult = $this->oAppFlags->getFlagList();
        }
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * POST request in Flags
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    public function post($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppFlags = libAppInstance::getAppFlags($this->oRedis);
        $aArgs['mall_id'] = $this->aRequest['mall_id'];
        $mResult = $this->oAppFlags->addFlags($aArgs);
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * Delte request in Flags
     * @return array        response
     */
    public function delete()
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppFlags = libAppInstance::getAppFlags($this->oRedis);
        $mResult = $this->oAppFlags->deleteFlags();
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
