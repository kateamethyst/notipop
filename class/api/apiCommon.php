<?php

/**
 * apiCommon
 * @package     api
 * @author      Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @since       October 25, 2016
 * @version     1.0
 */
class apiCommon extends Controller_Api
{
    /**
     * Request Info
     * @var array
     */
    public $aRequest;

    /**
     * redis instance
     * @var libRedis
     */
    public $oRedis;

    /**
     * Initialize instance and request
     */
    protected function initialize()
    {
        $this->aRequest = array(
            'mall_id'      => $this->Request->getDomain(),
            'app_id'       => $this->Request->getAppID(),
            'shop_no'      => $this->Request->getService()->getShopNo(),
            'mall_version' => $this->Request->getService()->getServiceType()
        );

        $this->oRedis = libRedis::getInstance($this->Redis);
        $this->oOpenApi = $this->Openapi;
    }
}
