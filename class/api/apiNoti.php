<?php

/**
 * Noti
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 16.
 * @version 1.0
 **/
class apiNoti extends apiCommon
{
    /**
     * AppNoti
     * @var object
     */
    public $oAppNoti;

    /**
     * Get request in notifications
     * @param  array    $aArgs  list of parameters
     * @return array            response
     */
    protected function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        
        $this->oAppNoti = libAppInstance::getAppNoti($this->oRedis);
        $this->oAppStats = libAppInstance::getAppStats($this->oRedis);

        if ($aArgs['find'] === 'true') {
            unset($aArgs['find']);
            $mResult = $this->oAppNoti->getCachePopup($aArgs);
        } else if ($aArgs['method'] === 'DELETE') {
            $mResult = $this->delete($aArgs);
        } else if ($aArgs['method'] === 'clear') {
            $mResult = $this->oAppNoti->clearCache();
        } else {
            $mResult = $this->oAppNoti->getNotifications();
        }

        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * POST request in notification
     * @param  array    $aArgs  list of parameters
     * @return array            response
     */
    protected function post($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();

        $aParam = $aArgs;

        if ($aArgs['method'] === 'PUT') {
            return $this->put($aParam);
        } else {

            $this->oAppNoti = libAppInstance::getAppNoti($this->oRedis);

            $aArgs['modifier'] = $this->aRequest['mall_id'];

            $mResult = $this->oAppNoti->addNotification($aArgs);

            $aLastRowNoti = $this->oAppNoti->getLastRow();

            if ($mResult === false) {
                return libUtilResponse::setResponse($mResult);
            }

            $oCallAppNotiStats = libAppInstance::getAppStats($this->oRedis);

            $mResult = $oCallAppNotiStats->addStats($aLastRowNoti['seq']);

            return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
        }
    }

    /**
     * PUT request in notification
     * @param  array    $aArgs  list of parameters
     * @return array            response
     */
    protected function put($aParams)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();

        $this->oAppNoti = libAppInstance::getAppNoti($this->oRedis);

        if ($aParams['purpose'] === 'updateStatus') {
            $mResult = $this->oAppNoti->updateStatus($aParams);
        }

        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * Delete request in norification
     * @param  array    $aArgs     list of parameters
     * @return array                response
     */
    protected function delete($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();

        $this->oAppNoti = libAppInstance::getAppNoti($this->oRedis);

        $this->oAppStats = libAppInstance::getAppStats($this->oRedis);

        $mResult = $this->oAppNoti->deleteNotification($aArgs['seq']);

        $mResult = $this->oAppStats->removeStats($aArgs['seq']);

        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
