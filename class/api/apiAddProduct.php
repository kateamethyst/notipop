<?php

/**
 * AddProduct ajax
 *
 * @package api
 * @author  정현 <hjung01@simplexi.com>
 * @since   2016. 8. 02.
 * @version 1.0
 **/
class apiAddProduct extends apiCommon
{

    /**
     * get method
     * @param $aParam
     * @return bool
     */
    protected function get($aParam)
    {
        $this->initialize();

        $aRequest = array(
            'mall_id'      => $this->aRequest['mall_id'],
            'mall_version' => $this->aRequest['mall_version']
        );

        $oAppAddProduct = new appAddProduct($this->oOpenApi, $aRequest);

        $aParam['searchCategory'][] = $aParam['CategoryType'];

        $aApiListResult = $oAppAddProduct->getEtcSearchCategoryList($aParam);

        if (libValid::isArray($aApiListResult[$aParam['CategoryType']]) === true) {
            return array(
                'meta'     => array('code' => 200),
                'response' => array('result' => $aApiListResult[$aParam['CategoryType']])
            );
        } else {
            return array(
                'meta'     => array('code' => 400),
                'response' => array('result' => array())
            );
        }
    }
}
