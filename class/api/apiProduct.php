<?php

/**
 * Product
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 16.
 * @version 1.0
 **/
class apiProduct extends apiCommon
{
    /**
     * App product
     * @var object
     */
    public $oAppProduct;

    /**
     * Get request in product
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    protected function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppProduct = libAppInstance::getAppProduct($this->oRedis);
        if ($aArgs['method'] === 'DELETE') {
            $mResult = $this->oAppProduct->deleteProducts($aArgs['products']);
        }
        $mResult = $this->oAppProduct->getProducts();
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * POST request in products
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    protected function post($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppProduct = libAppInstance::getAppProduct($this->oRedis);
        $mResult = $this->oAppProduct->addProduct($aArgs);
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

     /**
     * DELETE request in products
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    protected function delete()
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppProduct = libAppInstance::getAppProduct($this->oRedis);
        $mResult = $this->oAppProduct->deleteProductKey();
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
