<?php

/**
 * Member
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 16.
 * @version 1.0
 **/
class apiMember extends apiCommon
{
    /**
     * Get request in member
     * @param  array    $aArgs  list of parameters
     * @return array            response
     */
    protected function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();

        $oCallAppMember = libAppInstance::getAppMember($this->aRequest, $this->oOpenApi);

        $aArgs['mall_id'] = $this->aRequest['mall_id'];

        if ($aArgs['method'] === 'getMemberList') {
            unset($aArgs['method']);
            $mResult = $oCallAppMember->getMemberDetails($aArgs);
            return libUtilResponse::setResponse($mResult);
        }

        $mResult = $oCallAppMember->lookupMember($aArgs);
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
