<?php

/**
 * Group
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 18.
 * @version 1.0
 **/
class apiGroup extends apiCommon
{
    /**
     * This is for the instance of appGroup
     * @var object
     */
    public $oAppGroup;

    /**
     * Get requst in group
     * @return array       response
     */
    protected function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
            $this->initialize();
            $this->oAppGroup = libAppInstance::getAppGroup($this->oRedis);
            if ($aArgs['options'] === 'count') {
                $mResult = $this->oAppGroup->getGroupTotalCount();
            } else if ($aArgs['method'] === 'delete') {
                $mResult = $this->oAppGroup->removeGroup($aArgs);
            } else {
                $mResult = $this->oAppGroup->getGroupList();
            }
            return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * Post reques in group
     * @param  array    $aArgs  list of parameters
     * @return array            response
     */
    protected function post($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
            $this->initialize();
            $this->oAppGroup = libAppInstance::getAppGroup($this->oRedis);
            $mResult = $this->oAppGroup->addGroupCriteria($aArgs);
            return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * Delete Request
     * @return array        response
     */
    public function delete()
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
            $this->initialize();
            $this->oAppGroup = libAppInstance::getAppGroup($this->oRedis);
            $mResult = $this->oAppGroup->deleteGroup();
            return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
