<?php

/**
 * Stats
 * @package api
 * @author  Ma. Angelica Concepcion <concepcion>
 * @since   2016. 8. 22.
 * @version 1.0
 **/
class apiStats extends apiCommon
{
    /**
     * stats
     * @var object
     */
    public $oAppStats;

    /**
     * Get request in stats
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    public function get($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppStats = libAppInstance::getAppStats($this->oRedis);
        if ($aArgs['method'] === 'DELETE') {
            $this->oAppStats->deleteStatsAndFlags($aArgs['seq']);
        } else {
            $aStatsList = $this->oAppStats->getStatsList();
        }
        return libUtilResponse::setResponse($aStatsList);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * POST request in stats
     * @param  array    $aArgs      list of parameters
     * @return array                response
     */
    public function post($aArgs)
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppStats = libAppInstance::getAppStats($this->oRedis);
        $mResult = $this->oAppStats->addStats($aArgs);
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }

    /**
     * Delte request in stats
     * @return array        response
     */
    public function delete()
    {
        // if ($this->aRequest['mall_version'] === 'P' || $this->aRequest['mall_version'] === 'A') {
        $this->initialize();
        $this->oAppStats = libAppInstance::getAppStats($this->oRedis);
        $mResult = $this->oAppStats->deleteStats();
        return libUtilResponse::setResponse($mResult);
        // }
        // return libUtilResponse::setResponse('no access');
    }
}
