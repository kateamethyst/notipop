<?php

include_once __DIR__ . '/appNotiTest.php';

class unitall
{
    public static function suite()
    {
        $suite = new PHPUnit_Framework_TestSuite('notipop test suite');
        $suite->addTestSuite('appNotiTest');
        return $suite;
    }
}
