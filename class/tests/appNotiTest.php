<?php

/**
 * appNotiTest
 *
 * @package Notipop
 * @author  hjung01 <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016. 07. 29
 */
class appNotiTest extends Unittest_Testcase
{
    /**
     * @var appNoti
     */
    private $oAppNoti;

    /**
     * [$aNoti description]
     * @var array
     */
    private  $aNoti = array(
        'name'       => 'alert for wonan in twenies',
        'position'   => 'top-left',
        'connection' => 'true',
        'page'       => 'Product Details',
        'date'       => array(
            'start_date' => '2015-09-02',
            'end_date'   => '2016-10-31'
        ),
        'time'       => array(
            'start_time' => '08:00',
            'end_time'   => '23:00'
        ),
        'group_no'   => '4',
        'type'       => 'Coupon',
        'popup'      => array(
            'title'     => 'Hello {customer_name}',
            'contents'  => 'We will give a coupon :) Happing Shopping',
            'coupon_no' => '4600010000000000083',
            'button'    => '2'
        ),
        'use'         => 'false',
        'deleted_at'  => 'null',
        'timestamps'  => '2016-10-3',
        'seq'         => '103'
    );

    /**
     * Test getgetNotifications method in appNoti
     */
    public function testGetNotifications()     {
        $aOption = array(
            'getNotifications' => array(
                $this->aNoti
            )
        );
        $this->setAppNoti($aOption);

        $aNotiList = $this->oAppNoti->getNotifications();

        $this->assertEquals(false, is_array($aNotiList));
    }

    /**
     * [setAppNoti description]
     * @param array $aOption [description]
     */
    private function setAppNoti($aOption = array())
    {
        $aMethod = array(
            'getNotifications',
            'getNotificationList',
        );

        $oModelNotiMock = $this->getMockBuilder('modelNoti')->disableOriginalConstructor()->setMethods($aMethod)->getMock();

        foreach ($aOption as $sMethod => $mReturn) {
            $oModelNotiMock->expects($this->any())->method($sMethod)->will($this->returnValue($mReturn));
        }

        $this->oAppNoti = new appNoti($oModelNotiMock);
    }
}
