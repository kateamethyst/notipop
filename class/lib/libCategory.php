<?php

/**
 * 카테고리 데이터 tree 구조로 만들어서 반환하기
 * @package
 * @author  jsyang < jsyang@simplexi.com >
 * @since   2013. 1. 21.
 * @version 1.0
 */
class libCategory
{
    /**
     * 멀티몰 작업으로 추가 생성($aCategory['category_xx']->$aCategory['cat_xx'] 으로 변경)
     *
     * @param array $aCategoryList
     * @return mixed
     */
    public static function getTree($aCategoryList)
    {
        $aCategoryData = array();
        if ($aCategoryList && $aCategoryList['code'] == 200) {
            foreach ($aCategoryList['data'] as $iNo => $aCategory) {
                $childData = array();
                if (count($aCategory['cat_childs']) > 0) {
                    self::_getCategoryChildData($childData, $aCategory['cat_childs']);
                }
                if (isset($aCategory['cat_no'])) {
                    $aCategoryData[$iNo] = array(
                        'categoryNo'   => $aCategory['cat_no'],
                        'categoryName' => $aCategory['cat_name'],
                        'childData'    => $childData
                    );
                }
            }
        }
        return $aCategoryData;
    }

    /**
     * 멀티몰 작업으로 추가 생성($aCategory['category_xx']->$aCategory['cat_xx'] 으로 변경)
     *
     * @param array $Data
     * @param array $aChildData
     */
    private static function _getCategoryChildData(&$aData, $aChildData)
    {
        $childData = array();
        foreach ($aChildData as $iNo => $aCategory) {
            if (count($aCategory['cat_childs']) > 0) {
                self::_getCategoryChildData($childData, $aCategory['cat_childs']);
            }
            if (isset($aCategory['cat_no'])) {
                $aData[$iNo] = array(
                    'categoryNo'   => $aCategory['cat_no'],
                    'categoryName' => $aCategory['cat_name'],
                    'childData'    => $childData
                );
            }
        }
    }
}
