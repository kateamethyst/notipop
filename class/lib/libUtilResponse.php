<?php

/**
 * libUtilResponse Class
 * @author Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 0.1 2016-08-29
 * @package libUtilResponse
 */
class libUtilResponse
{
    /**
     * Set response
     * @param array $mParam Response
     */
    public function setResponse($mParam)
    {
        if ($mParam === false) {
            $this->setStatusCode(400);
            $this->setMessage('Bad Request');
            return false;
        } else if ($mParam === 'no access') {
            $this->setStatusCode(401);
            $this->setMessage('Unauthorized');
            return array('message' => 'Authentication is required. You need to login first.');
        } else {
            $this->setStatusCode(200);
            $this->setMessage('OK');
            return $mParam;
        }
    }
}
