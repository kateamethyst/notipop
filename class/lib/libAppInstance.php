<?php

/**
 * libAppInstance
 * @package lib
 * @author  Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   October 24, 2016
 */
class libAppInstance
{
    /**
     * Coupon business logic
     * @var appNoti
     */
    private $oAppCoupon;

    /**
     * Product
     * @var object
     */
    private $oAppProduct;

    /**
     * Group
     * @var object
     */
    private $oAppGroup;

    /**
     * Stats
     * @var object
     */
    private $oAppStats;

    /**
     * Member
     * @var object
     */
    private $oAppMember;


    /**
     * Member
     * @var object
     */
    private $oAppFlags;

    /**
     * Get appNoti instance
     * @param  object $oRedis Redis Instance
     * @return object         AppNotiInstance
     */
    public static function getAppNoti($oRedis)
    {
        if ($oAppNoti === null) {
            $oAppNoti = new appNoti(new modelNoti($oRedis));
        }
        return $oAppNoti;
    }

    /**
     * Get AppStats instance
     * @param  object $oRedis Redis Instance
     * @return object         AppStatsInstance
     */
    public static function getAppStats($oRedis)
    {
        if ($oAppStats === null) {
            $oAppStats = new appStats(new modelStats($oRedis));
        }
        return $oAppStats;
    }

    /**
     * Get AppGroup instance
     * @param  object $oRedis Redis Instance
     * @return object         AppGroupInstance
     */
    public static function getAppGroup($oRedis)
    {
        if ($oAppGroup === null) {
            $oAppGroup = new appGroup(new modelGroup($oRedis));
        }
        return $oAppGroup;
    }

    /**
     * Get AppCoupon instance
     * @param  object $oRedis Redis Instance
     * @return object         AppCouponInstance
     */
    public static function getAppCoupon($oOpenapi, $oRedis)
    {
        if ($oAppCoupon === null) {
            $oAppCoupon = new appCoupon($oOpenapi, new modelCoupon($oRedis));
        }
        return $oAppCoupon;
    }

    /**
     * Get AppProduct instance
     * @param  object $oRedis Redis Instance
     * @return object         AppProductInstance
     */
    public static function getAppProduct($oRedis)
    {
        if ($oAppProduct === null) {
            $oAppProduct = new appProduct(new modelProduct($oRedis));
        }
        return $oAppProduct;
    }

    /**
     * Get AppCoupon instance
     * @param  object $oRedis Redis Instance
     * @return object         AppMemberInstance
     */
    public static function getAppMember($aRequest, $oOpenapi)
    {
        if ($oAppMember === null) {
            $oAppMember = new appMember($aRequest, $oOpenapi);
        }
        return $oAppMember;
    }

    /**
     * Get AppFlags instance
     * @param  object $oRedis Redis Instance
     * @return object         AppGroupInstance
     */
    public static function getAppFlags($oRedis)
    {
        if ($oAppFlags === null) {
            $oAppFlags = new appFlags(new modelFlags($oRedis));
        }
        return $oAppFlags;
    }
}
