<?php

/**
 * Redis library
 * @package class/lib
 * @author  정현 <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016.05.02
 */
class libRedis
{
    /**
     * instance
     * @var libRedis
     */
    public static $oInstance;

    /**
     * libRedis 인스터스 얻기
     * @param object $oRedis
     * @return NULL|libRedis
     */
    public static function getInstance($oRedis)
    {
        if ($oRedis === null) {
            return null;
        }
        if (self::$oInstance === null) {
            self::$oInstance = new libRedis($oRedis);
        }
        return self::$oInstance;
    }

    /**
     * 세팅된 Redis 인스턴스
     * @var object
     */
    private $oRedis;

    /**
     * 마지막에 삽입된 pk
     * @var int
     */
    private $iLastPk;

    /**
     * 레디스 전역변수 세팅(생성자)
     * @param object $oRedis
     */
    protected function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * 식별키 생성하기 INCR
     * @param string $sKey
     * @return int
     */
    public function getPk($sKey)
    {
        return $this->oRedis->incr($sKey . ':pk');
    }

    /**
     * redis 데이터 인서트 하기
     * @param string $sKey
     * @param mixed  $mData
     * @return int
     */
    public function insert($sKey, $mData)
    {
        $mData['seq'] = $this->iLastPk = $this->getPk($sKey);
        return $this->oRedis->rPush($sKey, $mData);
    }

    /**
     * 인서트시 마지막 생성된 pk 값 추출
     * @return int
     */
    public function getLastPk()
    {
        return $this->iLastPk;
    }

    /**
     * 리스트 가지고오기 페이징 리스트 처리할때 사용
     * @param string $sKey
     * @param int    $iLimit
     * @param int    $iOffset
     * @return array
     */
    public function getList($sKey, $iLimit, $iOffset = 0)
    {
        $iLimit--;
        $iStart = $iOffset;
        $iEnd = $iStart + $iLimit;
        $aResponse = $this->oRedis->lRange($sKey, $iStart, $iEnd);
        $aList = array();
        foreach ($aResponse as $mRow) {
            $aList[] = $mRow;
        }
        return $aList;
    }

    /**
     * 리스트 순서 거꾸로 가지고 오기 DESC
     * @param string $sKey
     * @param int    $iRows
     * @param int    $iPage
     * @param int    $iTotalCount
     * @return array
     */
    public function getListDesc($sKey, $iRows = 10, $iPage = 1, $iTotalCount = 0)
    {
         $iStart = $iTotalCount - ($iPage * $iRows);
        $iEnd = $iStart + ($iRows - 1);
        $iStart = ($iStart < 0) ? 0 : $iStart;

        $aResponse = $this->oRedis->lRange($sKey, $iStart, $iEnd);

        $aList = array();
        foreach ($aResponse as $mRow) {
            $aList[] = $mRow;
        }

        $aList = is_array($aList) ? array_reverse($aList) : $aList;

        return $aList;
    }

    /**
     * 전체 리스트 목록 얻기
     * @param string $sKey
     * @return array
     */
    public function getAllList($sKey)
    {
        $iEnd = $this->getListLength($sKey);

        return $this->getList($sKey, $iEnd);
    }

    /**
     * 해당 키값의 인덱스의 데이터 얻기
     * @param string $sKey
     * @param int    $iIndex
     * @return mixed
     */
    public function getIndexData($sKey, $iIndex)
    {
        return $this->oRedis->lIndex($sKey, $iIndex);
    }

    /**
     * 해당 키값과 필터로 리스트의 인덱스를 반환
     * 필터 배열은 고유키를 포함
     * @param string $sKey
     * @param array  $aFilter
     * @return bool|int
     */
    public function getIndex($sKey, $aFilter)
    {
        $mData = $this->getFilteredList($sKey, $aFilter);
        if ($mData === false || is_array($mData) !== true || count($mData) === 0) {
            return false;
        }
        $aIndex = array_keys($mData);
        return $aIndex[0];
    }

    /**
     * 필터 값에 따른 리스트 배열 얻어오기, 검색 시 사용
     * http://php.net/manual/kr/function.array-intersect.php
     * @param string $sKey
     * @param array  $aFilters
     * @param int    $iLimit
     * @param int    $iOffset
     * @return array
     */
    public function getFilteredList($sKey, $aFilters, $iLimit = 0, $iOffset = 0)
    {
        $iStart = $iAdded = 0;
        $iEnd = $this->getListLength($sKey);
        $aResponse = $this->oRedis->lRange($sKey, $iStart, $iEnd);
        $iLimit = !$iLimit ? $iEnd : $iLimit + $iOffset;
        $aList = array();

        for ($iKey = 0; $iKey < $iEnd; $iKey++) {
            if (libValid::isArray($aResponse[$iKey]) === true) {
                $aIntersection = array_intersect($aFilters, $aResponse[$iKey]);
                if ($aFilters === $aIntersection) {
                    $aList[$iKey] = $aResponse[$iKey];
                }
            }
        }

        return $aList;
    }

    /**
     * 해당 조건 찿아서 값 지우기
     * @param string $sKey
     * @param array  $aFilters
     * @return array
     */
    public function removeByFilter($sKey, $aFilters)
    {
        $aList = $this->getFilteredList($sKey, $aFilters);
        $aResult = array();
        if (count($aList) > 0) {
            foreach ($aList as $aItem) {
                $aResult[] = $this->removeFromList($sKey, $aItem);
            }
        }
        return $aResult;
    }

    /**
     * 값 제거하기
     * @param string $sKey
     * @param string $mValue
     * @param int    $iCount
     * @return int
     */
    public function removeFromList($sKey, $mValue, $iCount = 0)
    {
        return $this->oRedis->lRem($sKey, $mValue, $iCount);
    }

    /**
     * 업데이트 하기
     * @param string $sKeyX
     * @param int    $iIndex
     * @param mixed  $aData
     * @return bool
     */
    public function update($sKey, $iIndex, $aData)
    {
        return $this->oRedis->lSet($sKey, $iIndex, $aData);
    }

    /**
     * 라스트에 등록됀 행 가지고오기
     * @param string $sKey
     * @return mixed
     */
    public function getLastRow($sKey)
    {
        return $this->getIndexData($sKey, -1);
    }

    /**
     * 먼처음 행 얻기
     * @param string $sKey
     * @return mixed
     */
    public function getFirstRow($sKey)
    {
        return $this->getIndexData($sKey, 0);
    }

    /**
     * 첫번째 로우 반환하기(반환 된 값 제거됨)
     * @param string $sKey
     * @return mixed
     */
    public function firstPop($sKey)
    {
        return $this->oRedis->lPop($sKey);
    }

    /**
     * 마지막 로우 반환하기(반환 된 값 제거됨)
     * @param string $sKey
     * @return mixed
     */
    public function lastPop($sKey)
    {
        return $this->oRedis->rPop($sKey);
    }

    /**
     * 리스트의 전체 길이 얻기
     * @param string $sKey
     * @return int
     */
    public function getListLength($sKey)
    {
        return $this->oRedis->lLen($sKey);
    }

    /**
     * 해당 키값의 데이터 설정
     * @param string $sKey
     * @param mixed  $mData
     * @return bool
     */
    public function setData($sKey, $mData)
    {
        return $this->oRedis->set($sKey, $mData);
    }

    /**
     * 해당 키값의 데이터 반환
     * @param string $sKey
     * @return mixed
     */
    public function getData($sKey)
    {
        return $this->oRedis->get($sKey);
    }

    /**
     * 해당 키값 삭제
     * @param string $sKey
     * @return int 성공1 실패0
     */
    public function delData($sKey)
    {
        return $this->oRedis->del($sKey);
    }

    /**
     * 해당 키값의 만료 설정
     * @param string $sKey
     * @param int    $iTtl
     * @return bool
     */
    public function setExpire($sKey, $iTtl)
    {
        return $this->oRedis->expire($sKey, $iTtl);
    }

    /**
     * 해당 키값의 만료 시간을 반환
     * @param string $sKey
     * @return int 이미 만료된 값(없는값) 일때 -1 반환
     */
    public function getExpire($sKey)
    {
        return $this->oRedis->ttl($sKey);
    }

    /**
     * 검색되는 키 반환
     * @param string $sKeys
     * @return mixed
     */
    public function getKeys($sKeys = '*')
    {
        return $this->oRedis->keys($sKeys);
    }
}
