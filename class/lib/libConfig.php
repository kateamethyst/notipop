<?php

/**
 * notipop basic configuration
 * @package lib/config
 * @author  정현 <hjung01@simplexi.com>
 * @version 1.0
 * @since   2016.08.01
 */
class libConfig
{
    /**
     * noti hide time(1day)
     */
    const NOTI_HIDE_TIME = 86400;

    /**
     * Expiration time
     * @constant String
     */
    const EXPIRE = 3600;
}
