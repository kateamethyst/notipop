<?php
/**
 * notification stat ui
 * @package admin/NotiStat
 * @author   Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since 2016.08.01
 */
class adminNotiManageAction extends adminCommon
{
    /**
     * Execute
     */
    public function execute()
    {
        $oAppNoti = libAppInstance::getAppNoti($this->oRedis);

        if (libValid::isNull($this->aArgs['seq']) === false) {
            $mResult = $oAppNoti->updateNotification($this->aArgs);

            // if (libValid::isNull($['popup']['product_no']) === false) {
            //     $this->saveProduct();
            // }

            $this->_setView('update', $mResult);

        } else {

            $mResult = $oAppNoti->addNotification($this->aArgs);

            if (libValid::isNull($this->aArgs['product_num']) === false) {
                $this->saveProduct();
            }

            $this->_setView('create', $mResult);
        }
    }

    /**
     * Set the alert message and redirect page
     * @param string $sMethod method
     * @param mixed $mResult response of the app
     */
    private function _setView($sMethod, $mResult)
    {
        if ($sMethod === 'create') {
            if ($mResult !== false) {
                $this->writeJS('alert("You have successfully added new notification"); location.href="[link=admin/index]";');
            } else {
                $this->writeJS('alert("Invalid data."); location.href="[link=admin/notimanage"];');
            }
        } else {
            if ($mResult !== false) {
                $this->writeJS('alert("You have successfully updated the notification"); location.href="[link=admin/index]";');
            } else {
                $this->writeJS('alert("Invalid data."); location.href="[link=admin/notimanage?seq=' . $this->aArgs['seq'] . '"];');
            }
        }
    }

    /**
     * Cache the seected product
     * @return boolean
     */
    private function saveProduct()
    {
        $oAppProduct = libAppInstance::getAppProduct($this->oRedis);

        $aData = array(
            'product_no'             => (int)$this->aArgs['product_num'],
            'product_classification' => $this->aArgs['product_classification'],
            'product_code'           => $this->aArgs['product_code'],
            'tiny_image'             => $this->aArgs['tiny_image'],
            'product_price'          => $this->aArgs['product_price'],
            'product_name'           => $this->aArgs['product_name']
        );

        $oAppProduct->addProduct($aData);
    }

}
