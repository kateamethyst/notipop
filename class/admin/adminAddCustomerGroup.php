<?php

/**
 * Add Customer Group
 * @package admin
 * @author   Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since 2016.08.01
 */
class adminAddCustomerGroup extends adminCommon
{
    /**
     * Execute
     */
    public function execute()
    {
        $this->importJS('/lib-js/AddGroup');
        $this->setView();
    }

    /**
     * Set view in admin
     */
    protected function setView()
    {
        $bView = $this->View('addCustomerGroup');

        if ($bView === false) {
            return false;
        }

        $this->setStatusCode('200');
        return true;
    }
}
