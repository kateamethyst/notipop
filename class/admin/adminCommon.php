<?php

/**
 * common admin controller(parent)
 * @package abstract class
 * @author  정현 <hjung01@simplexi.com>, Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016.07.18
 */
abstract class adminCommon extends Controller_Admin
{
    /**
     * parameter (POST, GET)
     * @var array
     */
    public $aArgs;

    /**
     * Request Info
     * @var array
     */
    public $aRequest;

    /**
     * redis instance
     * @var libRedis
     */
    public $oRedis;

    /**
     * first run function
     * @param array $aArgs parameter($_GET, $_POST)
     */
    protected function run($aArgs)
    {
        $this->aArgs = $aArgs;
        $this->initialize();
        $this->execute();
    }

    /**
     * child class first run function
     */
    abstract public function execute();

    /**
     * initialize
     */
    private function initialize()
    {
        $this->aRequest = array(
            'mall_id'      => $this->Request->getDomain(),
            'app_id'       => $this->Request->getAppID(),
            'shop_no'      => $this->Request->getService()->getShopNo(),
            'mall_version' => $this->Request->getService()->getServiceType()
        );

        $this->oRedis = libRedis::getInstance($this->Redis);
        $this->oOpenApi = $this->Openapi;
        $this->importJsCss();
    }

    /**
     * import basic resource
     */
    protected function importJsCss()
    {
        $this->externalJS('http://img.echosting.cafe24.com/js/suio.js');
        $this->externalCSS('http://img.echosting.cafe24.com/css/suio.css');
        $this->externalJS('http://img.echosting.cafe24.com/js/calendar/dateUtil.js');
        $this->UIPackage->addPlugin('calendar');
        $this->importCSS('app');
        $this->importJS('/lib-js/Calendar');
        $this->importJS('/lib-js/DropDownGroup');
        $this->importJS('app');
    }

    /**
     * redirect
     * using writeJS function
     *
     * @param string $sUrl target url
     * @param string $sMsg alert message
     */
    protected function redirect($sUrl = '[link=admin/index]', $sMsg = '')
    {
        if (libValid::isString($sMsg) === true) {
            $this->writeJS("alert('" . $sMsg . "');");
        }
        $this->writeJS("window.location.href = '" . $sUrl . "';");
    }

    /**
     * array assign
     *
     * @param $aParam
     * @return bool
     */
    protected function arrayAssign($aParam)
    {
        if (libValid::isArray($aParam) === false) {
            return false;
        }
        foreach ($aParam as $sKey => $sVal) {
            $this->assign(trim($sKey), $sVal);
        }
    }
}
