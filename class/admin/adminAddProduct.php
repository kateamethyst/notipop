<?php

/**
 * AddProduct 컨트롤러
 * @package admin
 * @author  정현 <hjung01@simplexi.com>
 * @since   2016. 8. 02.
 * @version 1.0
 **/
class adminAddProduct extends Controller_Admin
{
    /**
     * currency by multi shop
     * @var string
     */
    private $sCurrency;

    /**
     * @var appAddProduct
     */
    private $oAppAddProduct;

    /**
     * request param
     * @var array
     */
    private $aArgs;

    /**
     * limit array
     * @var array
     */
    private $aLimitList;

    /**
     * js param
     * @var array
     */
    private $aJsParam;

    /**
     * date array
     * @var array
     */
    private $aDate;

    /**
     * 실행 함수
     */
    protected function run($aArgs)
    {
        $this->initialize($aArgs);
        // product search data
        $aSearchData = $this->oAppAddProduct->getSearchProductApi($this->aArgs);
        // paging
        $aPagingParam = array(
            'url'         => '[link=admin/addProduct]',
            'page_record' => $this->aArgs['limit'],
            'now_page'    => $this->aArgs['page'],
            'total_count' => $aSearchData['totalcount'],
            'search_data' => $this->_makePagingParams()
        );
        $aPagingData = libPaging::getInstance()->getPaging($aPagingParam);
        // set js param
        $aCate = $this->oAppAddProduct->getCategoryTree();
        $this->aJsParam = array(
            'oDepthList'    => json_encode(array(
                'depth1' => $this->aArgs['depth1'],
                'depth2' => $this->aArgs['depth2'],
                'depth3' => $this->aArgs['depth3'],
                'depth4' => $this->aArgs['depth4']
            )),
            'oCategoryData' => $aCate === false ? '{}' : json_encode($aCate)
        );
        // set assign
        $aAssign = array(
            'aCodeList'   => $this->oAppAddProduct->getEtcSearchCategoryList($this->aArgs),
            'aDate'       => $this->aDate,
            'aParams'     => $this->aArgs,
            'sCurrency'   => $this->sCurrency,
            'aPaging'     => $aPagingData,
            'aSearchData' => $aSearchData,
            'aLimitList'  => $this->aLimitList
        );
        $this->arrayAssign($aAssign);
        // view
        $this->setView();
    }

    /**
     * initialize
     *
     * @param array $aArgs
     */
    private function initialize($aArgs)
    {
        $this->aArgs = $aArgs;
        $this->_setDefaultCondition();
        $aRequest = array(
            'mall_id'      => $this->Request->getDomain(),
            'mall_version' => $this->Request->getService()->getServiceType()
        );
        $this->oAppAddProduct = new appAddProduct($this->Openapi, $aRequest);
        $this->sCurrency = $this->oAppAddProduct->getCurrency($this->aArgs['shop_no']);
    }

    /**
     * 페이징을 위한 파람 만드는함수
     *
     * @return array $aPagingParams
     */
    private function _makePagingParams()
    {
        $aPagingParams = array(
            'searchCategory'  => $this->aArgs['searchCategory'],
            'search'          => $this->aArgs['search'],
            'depth1'          => $this->aArgs['depth1'],
            'depth2'          => $this->aArgs['depth2'],
            'depth3'          => $this->aArgs['depth3'],
            'depth4'          => $this->aArgs['depth4'],
            'srh_start_dt'    => $this->aArgs['srh_start_dt'],
            'srh_end_dt'      => $this->aArgs['srh_end_dt'],
            'DateType'        => $this->aArgs['DateType'],
            'is_sellingT'     => $this->aArgs['is_sellingT'],
            'is_sellingF'     => $this->aArgs['is_sellingF'],
            'is_displayT'     => $this->aArgs['is_displayT'],
            'is_displayF'     => $this->aArgs['is_displayF'],
            'is_sub_category' => $this->aArgs['is_sub_category'],
            'shop_no'         => $this->aArgs['shop_no'],
            'shop_curr'       => $this->sCurrency
        );
        return $aPagingParams;
    }

    /**
     * $aParams 가공
     *
     * @return array
     */
    private function _setDefaultCondition()
    {
        $this->aLimitList = array(10, 20, 30, 50, 100);
        $this->aDate = array(
            'srh_end'           => date('Y-m-d'),
            'srh_end_yesterday' => date('Y-m-d', strtotime('-1 days')),
            'srh_start_0'       => date('Y-m-d', strtotime('-1 days')),
            'srh_start_1'       => date('Y-m-d'),
            'srh_start_3'       => date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 3, date('Y'))),
            'srh_start_7'       => date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 7, date('Y'))),
            'srh_start_30'      => date('Y-m-d', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y'))),
            'srh_start_90'      => date('Y-m-d', mktime(0, 0, 0, date('m') - 3, date('d'), date('Y'))),
            'srh_start_180'     => date('Y-m-d', mktime(0, 0, 0, date('m') - 12, date('d'), date('Y')))
        );
        $this->aArgs['srh_start_dt'] = libValid::isString($this->aArgs['srh_start_dt']) === true ? $this->aArgs['srh_start_dt'] : $this->aDate['srh_start_180'];
        $this->aArgs['srh_end_dt'] = libValid::isString($this->aArgs['srh_end_dt']) === true ? $this->aArgs['srh_end_dt'] : $this->aDate['srh_end'];
        $this->aArgs['limit'] = is_numeric($this->aArgs['limit']) === true && in_array($this->aArgs['limit'], $this->aLimitList) === true ? $this->aArgs['limit'] : 10;
        $this->aArgs['page'] = libValid::isNumeric($this->aArgs['page']) === true ? $this->aArgs['page'] : 1;
        $this->aArgs['DateType'] = libValid::isString($this->aArgs['DateType']) === true ? $this->aArgs['DateType'] : 'regist_date';
        $this->aArgs['shop_no'] = libValid::isNumeric($this->aArgs['shop_no']) === true ? $this->aArgs['shop_no'] : 1;
        if ($this->aArgs['search_flag'] !== 'T') {
            $this->aArgs['is_sellingT'] = 'T';
            $this->aArgs['is_displayT'] = 'T';
            $this->aArgs['is_Allcontain'] = 'T';
            //            $this->aArgs['is_sellingF'] = 'F';
            //            $this->aArgs['is_displayF'] = 'F';
            //            $this->aArgs['all_status'] = 'T';
            //            $this->aArgs['is_sub_category'] = 'T';
        } else if (libValid::isString($this->aArgs['is_sellingT']) === true && libValid::isString($this->aArgs['is_sellingF']) === true && libValid::isString($this->aArgs['is_displayT']) === true && libValid::isString($this->aArgs['is_displayF']) === true) {
            $this->aArgs['all_status'] = 'T';
        }
    }

    /**
     * array assign
     * @param $aParam
     * @return bool
     */
    protected function arrayAssign($aParam)
    {
        if (libValid::isArray($aParam) === false) {
            return false;
        }
        foreach ($aParam as $sKey => $sVal) {
            $this->assign(trim($sKey), $sVal);
        }
    }

    /**
     * view 함수
     * @return bool
     */
    protected function setView()
    {
        $this->UIPackage->addPlugin('calendar');
        $this->externalJS('http://img.echosting.cafe24.com/js/suio.js');
        $this->externalCSS('http://img.echosting.cafe24.com/css/suio.css');
        $this->importCSS('app');
        $this->importJS('app');
        $this->importJS('/lib-js/AddProduct', $this->aJsParam, 'after');
        $this->importJS('Calender');
        $bView = $this->View('addProduct');
        if ($bView === true) {
            $this->setStatusCode('200');

            return true;
        } else {
            return false;
        }
    }
}

