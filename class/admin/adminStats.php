<?php

/**
 * notification stat ui
 * @package admin/NotiStat
 * @author   Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since 2016.08.01
 */
class adminStats extends adminCommon
{
    /**
     * Execute
     */
    public function execute()
    {
        $this->_setDefaultValue();

        $aData = array(
            'aNotiStats' => $this->getData()
        );
        //print_r($aData);
        $this->arrayAssign($aData);

        $this->setView();
    }

    /**
     * Get stats
     * @return array    Response
     */
    private function getData()
    {
        $oAppNoti = libAppInstance::getAppNoti($this->oRedis);
        $oAppStats = libAppInstance::getAppStats($this->oRedis);

        $aSequence = array(
            'seq' => (int)$this->aArgs['noti_id']
        );

        $aNotification = $oAppNoti->getDetails($aSequence);

        if (libValid::isArray($aNotification) === false) {
            return array();
        }

        $aNotiID = array(
            'noti_id' => (int)$this->aArgs['noti_id']
        );

        $aStats = $oAppStats->getStatsSummary($aNotiID);

        if (libValid::isArray($aStats) === false) {
            return array();
        }

        return array_merge($aNotification, $aStats[0]);
    }

    /**
     * set default value
     */
    private function _setDefaultValue()
    {
        $this->aArgs['seq'] = libValid::isNumeric($this->aArgs['seq']) === true ? (int)$this->aArgs['seq'] : null;
    }

    /**
     * Set view in admin
     */
    protected function setView()
    {
        $bView = $this->View('stats');

        if ($bView === false) {
            return false;
        }

        $this->setStatusCode('200');
        return true;
    }
}
