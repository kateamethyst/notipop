<?php

/**
 * notification manage (register, modify ui)
 * @package admin/NotiManage
 * @author  정현 <hjung01@simplexi.com>, Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016.08.02
 */
class adminNotiManage extends adminCommon
{

    /**
     * controller execute method
     */
    public function execute()
    {
        $this->_setDefaultValue();

        $oAppGroup = libAppInstance::getAppGroup($this->oRedis);

        $oAppCoupon = libAppInstance::getAppCoupon($this->oOpenApi, $this->oRedis);

        $oAppProduct = libAppInstance::getAppProduct($this->oRedis);

        $aGroupList = $oAppGroup->getGroupList();

        $aSeq = array(
            'seq' => (int)$this->aArgs['seq']
        );

        $aShopNo = array(
            'shop_no' => $this->aRequest['shop_no']
        );

        $aCouponList = $oAppCoupon->getCouponList($aShopNo);

        $aAssign = array(
            'aCouponList' => $aCouponList,
            'aGroupList'  => $aGroupList
        );

        if (libValid::isNull($this->aArgs['seq']) === false) {

            $oAppNoti = libAppInstance::getAppNoti($this->oRedis);

            $aNotiData = $oAppNoti->getDetails($aSeq);

            $aAssign['aNotiData'] = $aNotiData[0];
            $aProductNo = array(
                'product_no' => (int)$aNotiData[0]['popup']['product_no']
            );

            $aProduct = $oAppProduct->getProductData($aProductNo);

            $aAssign['aProduct'] = $aProduct[0];

            $sTplFile = 'edit';

        } else {
            $sTplFile = 'create';
        }

        $this->arrayAssign($aAssign);
        $this->_setView($sTplFile);
    }

    /**
     * set default value
     */
    private function _setDefaultValue()
    {
        $this->aArgs['seq'] = libValid::isNumeric($this->aArgs['seq']) === true ? $this->aArgs['seq'] : null;
    }

    /**
     * view method
     *
     * @return bool
     */
    private function _setView($sTplFile)
    {
        $bView = $this->View($sTplFile);
        if ($bView === false) {
            return false;
        }

        $this->setStatusCode('200');
        return true;
    }
}
