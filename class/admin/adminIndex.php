<?php

/**
 * notification list
 * @package admin/index
 * @author  정현 <hjung01@simplexi.com>, Ma. Angelica Concepcion <concepcionmaangelica@gmail.com>
 * @version 1.0
 * @since   2016.08.01
 */
class adminIndex extends adminCommon
{
    /**
     * controller execute method
     */
    public function execute()
    {
        $this->_setDefaultValue();

        $oAppNoti = libAppInstance::getAppNoti($this->oRedis);

        // Cache coupon
        $oAppCoupon = libAppInstance::getAppCoupon($this->oOpenApi, $this->oRedis);
        $aShopNo = array(
            'shop_no' => $this->aRequest['shop_no']
        );

        $aCouponList = $oAppCoupon->getCouponList($aShopNo);

        $this->aArgs['total_count'] = $oAppNoti->getTotalCount();

        $aNotiList = $oAppNoti->paginateNotifications($this->aArgs['limit'], $this->aArgs['page'], $this->aArgs['total_count']);

        $aPagingParam = array(
            'url'         => '[link=admin/index]',
            'page_record' => $this->aArgs['limit'],
            'now_page'    => $this->aArgs['page'],
            'total_count' => $this->aArgs['total_count']
        );

        $aPagingData = libPaging::getInstance()->getPaging($aPagingParam);
        
        if (libValid::isArray($aNotiList) === true) {
            $oAppGroup = libAppInstance::getAppGroup($this->oRedis);
            foreach ($aNotiList as $iKey => $aNoti) {
                $aGroup = $oAppGroup->getGroup(array('seq' => (int)$aNoti['group_no']));
                $aNotiList[$iKey]['group_name'] = $aGroup[0]['name'];
            }
        }

        $aAssign = array(
            'aNotiData'  => $aNotiList,
            'aLimitList' => $this->aLimitList,
            'aPaging'    => $aPagingData,
            'aParams'    => $this->aArgs
        );

        $this->arrayAssign($aAssign);

        $this->_setView();
    }

    /**
     * set default value
     */
    private function _setDefaultValue()
    {

        $this->aLimitList = array(5, 10, 20, 30, 50, 100);

        $this->aArgs['page'] = libValid::isNumeric($this->aArgs['page']) === true ? (int)$this->aArgs['page'] : 1;

        $this->aArgs['limit'] = libValid::isNumeric($this->aArgs['limit']) === true ? (int)$this->aArgs['limit'] : 5;

        $this->aArgs['offset'] = ($this->aArgs['page'] - 1) * $this->aArgs['limit'];
    }

    /**
     * view method
     *
     * @return bool
     */
    private function _setView()
    {
        $bView = $this->View();
        if ($bView === false) {
            return false;
        }

        $this->setStatusCode('200');
        return true;
    }
}
