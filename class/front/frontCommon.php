<?php

/**
 * Front Abstract
 * @package front/Common
 * @author
 * @version 1.0
 * @since
 */
abstract class frontCommon extends Controller_Front
{
    /**
     * parameter (POST, GET)
     * @var array
     */
    protected $aArgs;

    /**
     * controller start point
     * @param array $aArgs params
     */
    protected function run($aArgs)
    {
        $this->aArgs = $aArgs;
        $this->execute();
    }

    /**
     * child class first run function
     */
    abstract public function execute();

    /**
     * Get request
     */
    protected function getRequest()
    {
        return array(
            'seq'    => $this->getSequence(),
            'app_id' => $this->Request->getAppID(),
            'option' => $this->getOption()
        );
    }

    /**
     * Set resources of notification front
     */
    protected function setResources()
    {
        $this->externalJS('http://img.echosting.cafe24.com/js/suio.js');
        $this->externalCSS('http://img.echosting.cafe24.com/css/suio.css');
        $this->importCSS('front');
        $this->externalJS('http://cdnjs.cloudflare.com/ajax/libs/json3/3.3.2/json3.min.js');
        $this->importJS('/lib-js/jquery.cookie');
        $this->importJS('front');
    }

    /**
     * Assign array
     * @param  array $aParams list of parameters
     */
    protected function arrayAssign($aParams)
    {
        foreach ($aParams as $mKey => $mValue) {
            $this->assign(trim($mKey), $mValue);
        }
    }
}
