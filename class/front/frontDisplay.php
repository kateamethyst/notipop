<?php

/**
 * Front Display
 * @package front/Display
 * @author
 * @version 1.0
 * @since
 */
class frontDisplay extends frontCommon
{
    /**
     * controller execute method
     */
    public function execute()
    {
        $this->setResources();
        $this->assign('popupBody', 'popupBody');
    }
}
