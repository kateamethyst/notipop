<?php

/**
 * Model for Coupon
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelCoupon
{
    /**
     * Key for Redis notification
     * @var constant
     */
    const COUPON_KEY = 'noti_coupons';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelCoupon
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get list of Coupon
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getCouponList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::COUPON_KEY, $iLimit, $iOffset);
    }

    /**
     * Get the index of specific Coupon
     * @param  array   $aSequence   Sequence
     * @return array                Index of specific Coupon
     */
    public function getCouponIndex($aSequence)
    {
        return $this->oRedis->getIndex(self::COUPON_KEY, $aSequence);
    }

    /**
     * Get Coupons according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredCoupon($aFilter)
    {
        $counter = 0;
        $mResult = $this->oRedis->getFilteredList($sKey, $aFilter);
        foreach ($mResult as $sKey => $mValue) {
            $aResult[$counter] = $mValue;
            $counter++;
        }
        return $aResult;
    }

    /**
     * Add new Coupon
     * @param  array    $aParams  Details of the Coupon
     * @return boolean            Response
     */
    public function addCoupon($aParams)
    {
        return $this->oRedis->insert(self::COUPON_KEY, $aParams);
    }

    /**
     * Delete coupon key
     * @return boolean           Response
     */
    public function deleteCouponKey()
    {
        return $this->oRedis->delData(self::COUPON_KEY);
    }

    /**
     * Delete Coupon
     * @param   array     $aFilter    List of filter
     * @return  boolean               Response
     */
    public function deleteCoupon($aFilter)
    {
        return $this->oRedis->removeByFilter(self::COUPON_KEY, $aFilter);
    }

    /**
     * Set expiration for Coupon
     */
    public function setCouponExpiration()
    {
        return $this->oRedis->setExpire(self::COUPON_KEY, libConfig::EXPIRE);
    }
}
