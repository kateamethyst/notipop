<?php

/**
 * Model for Notification
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelNoti
{
    /**
     * Key for Redis notification
     * @var constant
     */
    const NOTI_KEY = 'noti_list';

    /**
     * Cache html notifications
     * @constant   string
     */
    const NOTI_CACHE = 'noti_cache';

    /**
     * Key for Redis notification
     * @var constant
     */
    const PRODUCT_KEY = 'noti_products';

    /**
     * Key for Redis notification
     * @var constant
     */
    const COUPON_KEY = 'noti_coupons';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelNoti
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get list of notifications
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getNotificationList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::NOTI_KEY, $iLimit, $iOffset);
    }

    /**
     * Get notifications in descending order
     * @param  integer   $iRows         no. of rows
     * @param  integer   $iPage         no. of page
     * @param  integer   $iTotalCount   no. of totalcount
     * @return array                    list of notification in descending order
     */
    public function getNotificationInDesc($iRows, $iPage, $iTotalCount)
    {
        return $this->oRedis->getListDesc(self::NOTI_KEY, $iRows, $iPage, $iTotalCount);
    }

    /**
     * Get total number of notification
     * @return integer      total count of notification
     */
    public function getNotificationTotalCount()
    {
        return $this->oRedis->getListLength(self::NOTI_KEY);
    }

    /**
     * Get notification last row
     * @return array        Details of last row
     */
    public function getLastRow()
    {
        return $this->oRedis->getLastRow(self::NOTI_KEY);
    }

    /**
     * Get the index of specific notification
     * @param  array   $aSequence   Sequence
     * @return array                Index of specific notification
     */
    public function getNotificationIndex($aSequence)
    {
        return $this->oRedis->getIndex(self::NOTI_KEY, $aSequence);
    }

    /**
     * Get notifications according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredNotification($aFilter)
    {
        $counter = 0;
        $mResult = $this->oRedis->getFilteredList(self::NOTI_KEY, $aFilter);
        foreach ($mResult as $sKey => $mValue) {
            $aResult[$counter] = $mValue;
            $counter++;
        }
        return $aResult;
    }

    /**
     * Get the details of specific notification according to the given index
     * @param  integer  $iIndex   Index number of notification
     * @return array              Details of notification
     */
    public function getNotificationDetails($iIndex)
    {
        return $this->oRedis->getIndexData(self::NOTI_KEY, $iIndex);
    }

    /**
     * Add new notification
     * @param  array    $aParams  Details of the notification
     * @return boolean            Response
     */
    public function addNotification($aParams)
    {
        return $this->oRedis->insert(self::NOTI_KEY, $aParams);
    }

    /**
     * Update notification
     * @param  integer   $iIndex    Index number
     * @param  array     $aParams   Data that are editted in the notification
     * @return boolean              Response
     */
    public function updateNotification($iIndex, $aParams)
    {
        return $this->oRedis->update(self::NOTI_KEY, $iIndex, $aParams);
    }

    /**
     * Delete notification
     * @param   array     $aFilter    List of filter
     * @return  boolean               Response
     */
    public function deleteNotification($aFilter)
    {
        return $this->oRedis->removeByFilter(self::NOTI_KEY, $aFilter);
    }

    /**
     * Set expiration for notification
     */
    public function setNotificationExpiration()
    {
        return $this->oRedis->setExpire(self::NOTI_KEY, libConfig::EXPIRE);
    }

    /**
     * Get product details for addons
     * @param  array    $aFilter    list of filters
     * @return array                list of products
     */
    public function getProductDetails($aFilter)
    {
        $iCounter = 0;
        $aProducts = $this->oRedis->getFilteredList(self::PRODUCT_KEY, $aFilter);
        foreach ($aProducts as $iKey => $aProduct) {
            $aAddOns[$iCounter] = $aProduct;
            $iCounter++;
        }
        return $aProducts;
    }

    /**
     * Get coupon detials for addons
     * @param  array    $aFilter    list of filters
     * @return array                list of coupons
     */
    public function getCouponDetails($aFilter)
    {
        $iCounter = 0;
        $aCoupons = $this->oRedis->getFilteredList(self::COUPON_KEY, $aFilter);
        foreach ($aCoupons as $iKey => $aCoupon) {
            $aAddOns[$iCounter] = $aCoupon;
            $iCounter++;
        }
        return $aAddOns;
    }

    /**
     * [addPopup description]
     * @param  array    $aPopup Popup details
     * @return boolean          Response
     */
    public function addPopup($aPopup)
    {
        return $this->oRedis->insert(self::NOTI_CACHE, $aPopup);
    }

    /**
     * Get cache popup detials
     * @param  integer  $iLimit      number of limit
     * @param  integer  $iOffset     number of offset
     * @return array                 list of cache popup
     */
    public function getCachePopup($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::NOTI_CACHE, $iLimit, $iOffset);
    }

    public function getFilteredCachePopup($aFilter)
    {
        $iCounter = 0;
        $aCachePopups = $this->oRedis->getFilteredList(self::NOTI_CACHE, $aFilter);
        foreach ($aCachePopups as $aCachePopup) {
            $aPopup[$iCounter] = $aCachePopup;
            $iCounter++;
        }
        return $aPopup;
    }

    /**
     * Clear Cache
     * @return  boolean               Response
     */
    public function clearCache()
    {
        return $this->oRedis->delData(self::NOTI_CACHE);
    }

    /**
     * Set cache expiration
     */
    public function setCacheExpiration()
    {
        $this->oRedis->setExpire(self::NOTI_CACHE, libConfig::EXPIRE);
    }
}
