<?php

/**
 * Model for Group Criteria
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelGroup
{
    /**
     * Key for Redis notification
     * @var constant
     */
    const NOTI_GROUP = 'noti_group';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelGroup
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get list of Group
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getGroupList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::NOTI_GROUP, $iLimit, $iOffset);
    }

    /**
     * Get Groups according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredGroup($aFilter)
    {
        $iCounter = 0;
        $mResult = $this->oRedis->getFilteredList(self::NOTI_GROUP, $aFilter);
        foreach ($mResult as $mValue) {
            $aResult[$iCounter] = $mValue;
            $iCounter++;
        }
        return $aResult;
    }

    /**
     * Get the details of specific Group according to the given index
     * @param  integer  $iIndex   Index number of Group
     * @return array              Details of Group
     */
    public function getGroupDetails($iIndex)
    {
        return $this->oRedis->getIndexData(self::NOTI_GROUP, $iIndex);
    }

    /**
     * Get notification last row
     * @return array        Details of last row
     */
    public function getLastGroupRow()
    {
        return $this->oRedis->getLastRow(self::NOTI_GROUP);
    }

    /**
     * Get total count number of group
     * @return array
     */
    public function getTotalGroupCount()
    {
        return $this->oRedis->getListLength(self::NOTI_GROUP);
    }
    /**
     * Add new Group
     * @param  array    $aParams  Details of the Group
     * @return boolean            Response
     */
    public function addGroup($aParams)
    {
        return $this->oRedis->insert(self::NOTI_GROUP, $aParams);
    }

    /**
     * Update Group
     * @param  integer   $iIndex    Index number
     * @param  array     $aParams   Data that are editted in the Group
     * @return boolean              Response
     */
    public function updateGroup($iIndex, $aParams)
    {
        return $this->oRedis->update(self::NOTI_GROUP, $iIndex, $aParams);
    }

    /**
     * Delete group key
     * @return boolean           Response
     */
    public function deleteGroupKey()
    {
        return $this->oRedis->delData(self::NOTI_GROUP);
    }

    /**
     * Delete Group
     * @param   array     $aFilter    List of filter
     * @return  boolean               Response
     */
    public function deleteGroup($aFilter)
    {
        return $this->oRedis->removeByFilter(self::NOTI_GROUP, $aFilter);
    }

    /**
     * Set expiration for Group
     */
    public function setGroupExpiration()
    {
        return $this->oRedis->setExpire(self::NOTI_GROUP, libConfig::EXPIRE);
    }
}
