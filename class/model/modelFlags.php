<?php

/**
 * Model for Notification Flags
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelFlags
{
    /**
     * @var FLAGS
     */
    const FLAGS = 'flags';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelStats
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get Flags according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredFlags($aFilter)
    {
        $iCounter = 0;
        $aFlags = $this->oRedis->getFilteredList(self::FLAGS, $aFilter);
        foreach ($aFlags as $aFlag) {
            $aFlagsData[$iCounter] = $aFlag;
            $iCounter++;
        }
        return $aFlagsData;
    }

    /**
     * Get list of flags
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getFlagList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::FLAGS, $iLimit, $iOffset);
    }

    /**
     * Get the data of a specific flag
     * @param  array $aSeq Sequence
     * @return array
     */
    public function getFlagIndex($aSeq)
    {
        return $this->oRedis->getIndex(self::FLAGS, $aSeq);
    }

    /**
     * Add new FLAGS
     * @param  array    $aParams  Details of the Stats
     * @return boolean            Response
     */
    public function addFlags($aParams)
    {
        return $this->oRedis->insert(self::FLAGS, $aParams);
    }

    /**
     * Updaet flags
     * @param  integer $iIndex  index
     * @param  array  $aParams  details of the flag
     * @return boolean
     */
    public function updateFlags($iIndex, $aParams)
    {
        return $this->oRedis->update(self::FLAGS, $iIndex, $aParams);
    }
     /**
     * Delete falgs
     * @return  boolean               Response
     */
    public function deleteFlags()
    {
        return $this->oRedis->delData(self::FLAGS);
    }
}
