<?php

/**
 * Model for Selected Product
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelProduct
{
    /**
     * Key for Redis notification
     * @var constant
     */
    const PRODUCT_KEY = 'noti_products';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelProduct
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get list of Product
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getProductList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::PRODUCT_KEY, $iLimit, $iOffset);
    }

    /**
     * Get Products in descending order
     * @param  integer   $iRows         no. of rows
     * @param  integer   $iPage         no. of page
     * @param  integer   $iTotalCount   no. of totalcount
     * @return array                    list of Product in descending order
     */
    public function getProductInDesc($iRows, $iPage, $iTotalCount)
    {
        return $this->oRedis->getListDesc(self::PRODUCT_KEY, $iRows, $iPage, $iTotalCount);
    }

    /**
     * Get total number of Product
     * @return integer      total count of Product
     */
    public function getProductTotalCount()
    {
        return $this->oRedis->getListLength(self::PRODUCT_KEY);
    }

    /**
     * Get Product last row
     * @return array        Details of last row
     */
    public function getLastRow()
    {
        return $this->oRedis->getLastRow(self::PRODUCT_KEY);
    }

    /**
     * Get the index of specific Product
     * @param  array   $aSequence   Sequence
     * @return array                Index of specific Product
     */
    public function getProductIndex($aSequence)
    {
        return $this->oRedis->getIndex(self::PRODUCT_KEY, $aSequence);
    }

    /**
     * Get Products according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredProduct($aFilter)
    {
        $iCounter = 0;
        $mResult = $this->oRedis->getFilteredList(self::PRODUCT_KEY, $aFilter);
        // foreach ($mResult as $mValue) {
        //     $aResult[$iCounter] = $mValue;
        //     $iCounter++;
        // }
        return $mResult;
    }

    /**
     * Get the details of specific Product according to the given index
     * @param  integer  $iIndex   Index number of Product
     * @return array              Details of Product
     */
    public function getProductDetails($iIndex)
    {
        return $this->oRedis->getIndexData(self::PRODUCT_KEY, $iIndex);
    }

    /**
     * Add new Product
     * @param  array    $aParams  Details of the Product
     * @return boolean            Response
     */
    public function addProduct($aParams)
    {
        return $this->oRedis->insert(self::PRODUCT_KEY, $aParams);
    }

    /**
     * Update Product
     * @param  integer   $iIndex    Index number
     * @param  array     $aParams   Data that are editted in the Product
     * @return boolean              Response
     */
    public function updateProduct($iIndex, $aParams)
    {
        return $this->oRedis->update(self::PRODUCT_KEY, $iIndex, $aParams);
    }

    /**
     * Delete Product
     * @param   array     $aFilter    List of filter
     * @return  boolean               Response
     */
    public function deleteProduct($aFilter)
    {
        // return $this->oRedis->delData(self::PRODUCT_KEY);
        return $this->oRedis->removeByFilter(self::PRODUCT_KEY, $aFilter);
    }

    /**
     * Set expiration for Product
     */
    public function setProductExpiration()
    {
        return $this->oRedis->setExpire(self::PRODUCT_KEY, libConfig::EXPIRE);
    }

    public function deleteProductKey()
    {
        return $this->oRedis->delData(self::PRODUCT_KEY);
    }
}
