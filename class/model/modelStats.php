<?php

/**
 * Model for Notification Statistics
 * @package model
 * @author Ma. Angelica Concepcion <oncepcionmaangelica@gmail.com>
 * @version 1.0
 * @since November 24, 2016
 */
class modelStats
{
    /**
     * Key for Redis notification
     * @var constant
     */
    const NOTI_STATS = 'noti_stat_list';

    /**
     * Redis
     * @var Object
     */
    private $oRedis;

    /**
     * Constructor for modelStats
     * @param object $oRedis Redis instance
     */
    public function __construct($oRedis)
    {
        $this->oRedis = $oRedis;
    }

    /**
     * Get list of stats
     * @param  integer $iLimit    Limit
     * @param  integer $iOffset   Offset
     * @return array              List of notification
     */
    public function getStatsList($iLimit = 0, $iOffset = 0)
    {
        return $this->oRedis->getList(self::NOTI_STATS, $iLimit, $iOffset);
    }

    /**
     * Get the index of specific Stats
     * @param  array   $aSequence   Sequence
     * @return array                Index of specific Stats
     */
    public function getStatsIndex($aSequence)
    {
        return $this->oRedis->getIndex(self::NOTI_STATS, $aSequence);
    }

    /**
     * Get Statss according to filter
     * @param  array   $aFilter   Filters
     * @return array              Lst of array according to the given filter
     */
    public function getFilteredStats($aFilter)
    {
        $iCounter = 0;
        $aStats = $this->oRedis->getFilteredList(self::NOTI_STATS, $aFilter);
        foreach ($aStats as $aStat) {
            $aStatsData[$iCounter] = $aStat;
            $iCounter++;
        }
        return $aStatsData;
    }


    /**
     * Add new Stats
     * @param  array    $aParams  Details of the Stats
     * @return boolean            Response
     */
    public function addStats($aParams)
    {
        return $this->oRedis->insert(self::NOTI_STATS, $aParams);
    }

    /**
     * Update Stats
     * @param  integer   $iIndex    Index number
     * @param  array     $aParams   Data that are editted in the Stats
     * @return boolean              Response
     */
    public function updateStats($iIndex, $aParams)
    {
        return $this->oRedis->update(self::NOTI_STATS, $iIndex, $aParams);
    }

    /**
     * Delete Stats
     * @return  boolean               Response
     */
    public function deleteStats()
    {
        return $this->oRedis->delData(self::NOTI_STATS);
    }

    /**
     * Remove stats
     * @param  [type] $aFilter [description]
     * @return [type]          [description]
     */
    public function removeNotiStats($aFilter)
    {
        return $this->oRedis->removeByFilter(self::NOTI_STATS, $aFilter);
    }
}
