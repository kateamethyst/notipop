<div class="section">
    <div class="mTab typeNav">
        <ul>
            <li class="">
                <a href="[link=admin/index]">Notification Management</a>
            </li>
        </ul>
    </div>
    <div class="tabCont">
        <div class="section" style="display: block">
        <div class="mTitle">
            <h2><font><font>Notification Statistics</font></font></h2>
        </div>
        <div class="mBoard gSmall">
            <table border="1" summary="">
                <tbody>
                <?php
                    if (libValid::isArray($aNotiStats) === true) {
                ?>
                <tr>
                    <th scope="row"><font><font>Notification Date</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['timestamps']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Alert Name</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['name']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Notifications output location</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['position']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Notification type</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['type']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Destination page</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['page']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Notification period</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['date']['start_date'] . ' ~ ' . $aNotiStats[0]['date']['end_date']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Notifications title</font></font></th>
                    <td><font><font><?php echo $aNotiStats[0]['popup']['title']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Stats Per View</font></font></th>
                    <td><font><font><?php echo $aNotiStats['view']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Link Clicks</font></font></th>
                    <td><font><font><?php echo $aNotiStats['click']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Download coupons</font></font></th>
                    <td><font><font><?php echo $aNotiStats['download']; ?></font></font></td>
                </tr>
                <tr>
                    <th scope="row"><font><font>Transition</font></font></th>
                    <td>
                        <img src="http://img.echosting.cafe24.com/stock/@graph.gif">
                    </td>
                </tr>
                <?php
                    } else {
                ?>
                
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
        <div class="mButton gCenter">
            <a href="[link=admin/index]" class="btnEm "><span><font><font>Go Back</font></font></span></a>
        </div>
    </div>
    </div>
</div>
