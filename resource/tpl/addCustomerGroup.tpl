<body>
<div class="headingArea">
    <div class="mTitle">
        <h1>Add customer group criteria</h1>
    </div>
</div>
<div class="mBoard gSmall">
    <table border="1" summary="" class="eChkColor" style="width:100%;">
        <caption>제목</caption>
        <tbody>
            <tr class="">
                <th scope="row">Group Name <span class="icoRequired"></span></th>
                <td><input id="name" required type="text" class="fText" name="name" placeholder="What is the name of the group?"></td>
            </tr>
            <tr class="">
                <th scope="row">Group Description <span class="icoRequired"></span></th>
                <td>
                    <textarea id="description" required class="fText" name="description" placeholder="What is the description of the group?"></textarea>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="mState">
    <div class="mTitle">
        <h2>List of Conditions <span class="icoRequired"></span></h2>
    </div>
</div>
 <div class="mBoard">
    <div class="mCtrl typeHeader" >
        <div class="gRight">
            <a id="btnAddOption" href="#Option" class="btnCtrl eLayerClick"><span><font>Add Condition</font></span></a>
            </a>
        </div>
    </div>
    <table border="1" summary="" class="eChkColor" style="text-align:center;" id="groupCriteria">
        <caption>Additional customer groups</caption>
        <thead>
            <th width="30%">Title</th>
            <th width="20%">Condition</th>
            <th width="30%">Criteria</th>
            <th width="10%">Actions</th>
        </thead>
        <tbody class="conditionList">

        </tbody>
    </table>
</div>
<div id="Option" class="mLayer gMedium">
    <h2>Create Condition</h2>
    <div class="wrap">
        <div class="mBoard">
            <table border="1" summary="">
            <caption>Create Condition</caption>
            <colgroup>
                <col style="width:50%" />
                <col style="width:50%" />
            </colgroup>
            <thead>
                <tr>
                    <th scope="col">Condition</th>
                    <th scope="col">Option</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                    <select id="slcCondition" name="conditions">
                        <option value="0">Shopping mall customer level</option>
                        <option value="1">Age</option>
                        <option value="2">Gender</option>
                        <option value="3">Points</option>
                        <option value="4">Date of registration</option>
                        <option value="5">Email Subscribers</option>
                        <option value="6">Anniversary</option>
                    </select>
                    </td>
                    <td class="option">
                        <select id="lvl" name="lvl">
                            <option value="lvl1">Level 1</option>
                            <option value="lvl2">Level 2</option>
                        </select>
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
    <div class="footer">
        <a href="#none" id="btnSaveCondition" class="btnCtrl"><span>Save and Add another</span></a>
        <a href="#none" class="btnNormal eClose"><span>취소</span></a>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>
<div id="footer">
    <a href="#laye" id="btnSaveGroupCriteria" class="btnSubmit eModal">
        <span>선택</span>
    </a>
    <a href="#none" class="btnEm" onclick="window.close();">
        <span>닫기</span>
    </a>
</div>
</body>
