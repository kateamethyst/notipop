<div class="section">
    <div class="mTab typeNav">
        <ul>
            <li class="selected">
                <a href="[link=admin/index]">Notification Managment</a>
            </li>
        </ul>
    </div>
    <div class="tabCont">
        <div class="mState">
            <div class="mTitle">
                <h2>Notification List </h2>
            </div>
            <div class="gLeft">
                <p class="total">[총
                    <strong><?=$aParams['total_count']?></strong>
                    개]
                </p>
            </div>
            <div class="gRight">
                <select class="fSelect" id="limit">
                    <?php
                    foreach ($aLimitList as $iLimit) {
                        ?>
                        <option value="<?php echo $iLimit; ?>" <?=(int)$aParams['limit'] === $iLimit ? "selected='selected'" : null?>><?php echo $iLimit; ?>개씩 보기</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="mCtrl typeHeader">
            <div class="gLeft">
                <span class="txtLess">Selected items</span>
                <a href="#none" class="btnNormal" id="btnDeleteNoti">
                    <span>
                        <em class="icoDel"></em>
                        삭제
                    </span>
                </a>
            </div>
            <div class="gRight">
                <a href="#" class="btnNormal" id="btnClearCache">
                    <span>Clear Cache</span>
                </a>
                <a href="[link=admin/notimanage]" class="btnCtrl">
                    <span>알림등록</span>
                </a>
            </div>
        </div>
        <div class="mBoard">
            <table border="1" summary="" class="eChkColor eChkTr" id="tblNotiList">
                <caption>Notification 리스트</caption>
                <colgroup>
                    <col class="chk">
                </colgroup>
                <thead>
                <tr>
                    <th scope="col">
                        <input type="checkbox" class="allChk">
                    </th>
                    <th width= "3%" >번호</th>
                    <th width= "8%" >알림 타입</th>
                    <th width= "20%" >알림 명</th>
                    <th width= "10%" >알림 기준</th>
                    <th width= "5%" >대상 페이지</th>
                    <th width= "10%" >알림 기간</th>
                    <th width= "5%" >등록일</th>
                    <th width= "5%" >통계</th>
                    <th width= "5%" >관리</th>
                </tr>
                </thead>
                    <?php 
                        $iNum = $aParams['total_count'] - (($aParams['page'] - 1) * $aParams["limit"]);
                        if(libValid::isArray($aNotiData) === true) {
                    ?>
                    <tbody class="center" id="secTBody">
                    <?php
                            foreach ($aNotiData as $mKey => $mValue) { ?>
                            <tr>
                                <td>
                                    <input type = "checkbox" class = "rowChk" value="<?php echo $mValue['seq']; ?>">
                                </td>
                                <td><?php echo $mValue['seq']; ?></td>
                                <td><?php echo $mValue['type']; ?></td>
                                <td><a href="[link=admin/notimanage?seq=<?php echo $mValue['seq']; ?>]" class="txtLink"><?php echo $mValue['name']; ?></a></td>
                                <td><?php echo $mValue['group_name']; ?></td>
                                <td style="text-align:center;">
                                    <p><?php echo $mValue['page']; ?></p>
                                </td>
                                <td>
                                    <p><?php echo $mValue['date']['start_date'] . ' to ' . $mValue['date']['end_date']; ?></p>
                                </td>
                                <td>                                    
                                    <?php echo $mValue['timestamps']; ?>
                                </td>
                                <td>
                                    <a class="btnNormal" href="[link=admin/stats?noti_id=<?php echo $mValue['seq']; ?>]"><span><font><font>statistics</font></font></span></a>
                                </td>
                                <td>
                                    <?php 
                                        if ($mValue['use'] === false) {
                                        ?>   
                                            <p class="txtWarn">Not</p>
                                            <a id="btnStatus" data-id="<?php echo $mValue['seq']; ?>" value="1" class="btnNormal" href="#"><span><font><font>In use</font></font></span></a>
                                        <?php
                                        } else {
                                        ?>
                                        <p class="txtEm">In use</p>
                                        <a id="btnStatus"  data-id="<?php echo $mValue['seq']; ?>" value="0" class="btnNormal" href="#"><span><font><font>Not</font></font></span></a>
                                    <?php
                                        }
                                    ?> 
                                </td>
                            </tr>
                        <?php 
                            $iNum--;
                        ?>
                        </tbody>
                        <?php
                            } 
                        } else {
                        ?>
                        <tbody class="empty">
                        <tr>
                            <td colspan="10">등록 된 알림이 없습니다.</td>
                        </tr>
                        </tbody>
                        <?php
                        }
                     ?>
            </table>
        </div>
        <div class="mCtrl typeFooter">
            <div class="gLeft">
                <span class="txtLess">Selected items</span>
                <a href="#none" class="btnNormal" id="btnDeleteNoti2">
                    <span>
                        <em class="icoDel"></em>
                        삭제
                    </span>
                </a>
            </div>
        </div>
        <?php echo $aPaging['sPaging']; ?>

        <div class="mHelp typeInfo">
            <h2><font><font>Help</font></font></h2>
            <div class="content">
                <ul>
                    <li><font><font>help info</font></font></li>
                    <li><font><font>help info</font></font></li>
                    <li><font><font>help info</font></font></li>
                    <li><font><font>help info</font></font></li>
                </ul>
            </div>
        </div>
    </div>
</div>
