<div class="section">
    <div class="mTab typeNav">
        <ul>
            <li class="selected">
                <a href="[link=admin/index]">Notification Management</a>
            </li>
        </ul>
    </div>
    <div class="mTitle" id="secAddNoti">
        <h2>Update Notification </h2>
    </div>
    <div class="mBoard">
        <form method="post" action="[link=admin/notimanageaction]">
        <input type="hidden" name="seq" class="fText" value="<?php echo $aNotiData['seq']; ?>">
        <input type="hidden" name="timestamps" class="fText" value="<?php echo $aNotiData['timestamps']; ?>">
        <input type="hidden" name="use" class="fText" value="<?php echo $aNotiData['use']; ?>">
        <table border="1" summary="" class="eChkColor">
            <caption>제목</caption>
            <tbody>
                <tr>
                    <th scope="row" width="20%">알림기준 선택</th>
                    <td colspan="2">
                        <div class="mFlex flexWrap groupCriteriaBox">
                            <div class="boxAdd"><a href="" class="eLayerClick" id="selectProduct" onclick="window.open('/admin/addcustomergroup', 'mywin', 'left=20,top=20,width=1000,height=500,toolbar=1,resizable=0');"><span>+</span></a></div>
                            <?php
                                if(libValid::isArray($aGroupList) === true) {
                                    foreach ($aGroupList as $mKey => $mValue) {
                            ?>
                                        <label id="labelCriteria" data-id="<?php echo $mValue['seq']?>">
                                            <input  type="radio" data-id="<?php echo $mValue['seq']?>" value="<?php echo $mValue['seq']; ?>" name="group_no" class="rdbGroupCriteria" class="fChk" <?php echo '' . (strval($aNotiData['group_no']) === strval($mValue['seq'])) ? "checked" :  "" ; ?>>
                                            <div><?php echo $mValue['name']; ?></div>
                                        </label>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                        <div class="setting" style="display:none">
                            <a href="#" class="btnNormal" id="btnDeleteGroup" data-id="0"><span>✖</span></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 이름</th>
                    <td colspan="2"><input type="text" placeholder="알림이름" width="100%" value="<?php echo $aNotiData['name']; ?>" class="fText" name="name" id="txtNotiName" required></td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 출력위치</th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="top-left" name="position" id="radioDefault" class="fChk" <?php echo '' . ($aNotiData['position'] === 'top-left') ? 'checked' : ''; ?> >
                            좌상단
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="bottom-left" name="position" id="radioSize" class="fChk" <?php  echo '' .  ($aNotiData['position'] === 'bottom-left') ?'checked' : '';  ?>>
                            좌하단
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="top-right" name="position" id="radioImage" class="fChk" <?php echo '' . ($aNotiData['position'] === 'top-right') ? 'checked' : ''; ?>>
                            우상단
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="bottom-right" name="position" id="radioImage" class="fChk" <?php echo '' . ($aNotiData['position'] === 'bottom-right') ? 'checked' : ''; ?>>
                            우하단
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 타입</th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="TEXT" name="type" id="rdbText" class="fChk" <?php echo '' . ($aNotiData['type'] === 'TEXT') ? 'checked' : ''; ?> >
                            텍스트
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="Coupon" name="type" id="rdbCoupon" class="fChk" <?php echo '' . ($aNotiData['type'] === 'Coupon') ? 'checked' : ''; ?> >
                            쿠폰발급
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="Advertisement" name="type" id="rdbProducts" class="fChk" <?php echo '' . ($aNotiData['type'] === 'Advertisement') ? 'checked' : ''; ?> >
                            상품홍보
                        </label>
                        <!-- <label class="gLabel">
                            <input  type="radio" value="Login/Registration" name="type" id="rdbRegistration" class="fChk" <?php echo '' . ($aNotiData['type'] === 'Login/Registration') ? 'checked' : ''; ?> >
                            Login/Register Invited
                        </label> -->
                    </td>
                </tr>
                <!-- <tr>
                    <th scope="row" width="20%">Login or not </th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="true" name="redbAccess" id="rdbText" class="fChk" checked>
                            Login
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="false" name="redbAccess" id="rdbCoupon" class="fChk">
                            Not Login 
                        </label>
                    </td>
                </tr> -->
                <tr>
                    <th scope="row" width="20%">대상 페이지</th>
                    <td colspan="2">
                        <select id="slcPage" name="page">
                            <option value="All Page" <?php echo '' . ($aNotiData['page'] === 'All Page') ? 'selected' : ''; ?>>All Page</option>
                            <option value="Main Page" <?php echo '' . ($aNotiData['page'] === 'Main Page') ? 'selected' : ''; ?>>Main Page</option>
                            <option value="Product DDetails" <?php echo '' . ($aNotiData['page'] === 'Product Details') ? 'selected' : ''; ?>>Product Details</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Login or not</th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="true" name="connection" id="rdbText" class="fChk" checked>
                            Login
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="false" name="connection" id="rdbCoupon" class="fChk">
                            Not Login 
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Notification Period </th>
                    <td colspan="2">
                        <input type="text" id="srh_start_dt" name="srh_start_dt" class="fText gDate txtStartDate" required value="<?php echo $aNotiData['date']['start_date']; ?>">
                        <a href="javascript:;" id="start_date" class="btnIcon icoCal img_cal1"><span>달력보기</span></a> 
                        <input type="number" id="txtF1Time" maxlength="2" placeholder='01' name="start_time1" class="fText txtNumber" required value="08"> :
                        <input type="number" name="start_time2" id="txtF2Time" maxlength="2" placeholder="00" class="fText txtNumber" required value="00">
                        ~
                        <input type="text" id="srh_end_dt" name="srh_end_dt" class="fText gDate txtEndDate" required value="<?php echo $aNotiData['date']['end_date']; ?>">
                        <a href="javascript:;" id="end_date" class="btnIcon icoCal img_cal2"><span>달력보기</span></a>
                        <input type="number" name="end_time1" id="txtS1Time" maxlength="2" placeholder='23' class="fText txtNumber" required value="23"> :
                        <input type="number" name="end_time2" id="txtS2Time" maxlength="2" placeholder="00" class="fText txtNumber" required value="00">
                    </td>
                </tr>
                <tr>
                    <th scope="row"  width="20%">Notifications title</th>
                    <td>
                        <input type="text" class="fText" name="title" id="txtTitle" placeholder="알림 타이틀" value="<?php echo $aNotiData['popup']['title']?>">
                        <a href="#mLayerTest2" class="btnDate eLayerClick eLayerClick2" >변수추가</a>
                    </td>
                    <td rowspan="3">
                        Preview
                        <div class="displayNotif">
                            <div class="previewBox">
                                <p id="title"><?php echo $aNotiData['popup']['title']; ?></p>
                                <div id="contents">
                                    <p id="descritpion"><?php echo $aNotiData['popup']['contents']; ?></p>
                                    <?php if ($aNotiData['type'] === 'Coupon') { ?><a style="display:inline-block;" href="#none" class="btnGeneral" id="btnCoupon"><span>Download Coupon</span></a> 
                                    <?php } else if ($aNotiData['type'] === 'Advertisement') { ?>
                                    <div id="divProd" style="display:inline-block;">
                                        <div class="mFlex" id="prodPreview">
                                            <div>
                                                <img class="imgBorder" src="<?php echo $aProduct['tiny_image']; ?>" width="100" height="100">
                                            </div>
                                            <div>
                                                <h2><?php echo $aProduct['product_name']; ?></h2>
                                                <p><?php echo $aProduct['product_price']; ?> KRW</p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <!-- <div id="register">
                                        <a href="#none" class="btnSubmit" id="btnLogin"><span>Sign in</span></a>
                                         <a href="#none" class="btnEm" id="btnRegister"><span>Sign up</span></a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Notifications description</th>
                    <td id="tdTextArea">
                        <textarea name="description" class="fTextarea" cols="10" rows="4" style="width:400px; height:250px;" placeholder="알림 내용" id="txtContent"><?php echo $aNotiData['popup']['contents']; ?></textarea>
                        <a href="#mLayerTest3" class="btnDate eLayerClick eLayerClick3" >변수추가</a>
                    </td>
                </tr>
                <?php if ($aNotiData['type'] === 'Coupon') { ?>
                <tr class="secCoupon" style="display:table-row">
                    <th>Choose coupon</th>
                    <td>
                        <select id="slcCoupon" class="fSelect" name="coupon_no">
                           <?php foreach ($aCouponList as $mKey => $mValue) { ?>
                               <option value="<?php echo $mValue['coupon_no']; ?>" <?php echo '' . $mValue['coupon_no'] === $aNotiData['popup']['coupon_no'] ? 'selected' : '' ?> ><?php echo $mValue['coupon_name']; ?></option>
                            <?php } ?>
                        </select>
                        <br>
                        <br>
                    </td>
                </tr>
                <?php } else if ($aNotiData['type'] === 'Advertisement') { ?>
                <!-- <tr class="secRegistration">
                    <th>Button</th>
                    <td>
                        <label class="gLabel">
                            <input  type="radio" value="2" name="button" id="rdbAddBtn" class="fChk" checked>
                            버튼추가
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="2" name="button" id="rdbRemoveBtn" class="fChk" >
                            버튼추가하지 않음
                        </label>
                    </td>
                </tr> -->
                <tr class="secProducts" style="display:table-row">
                    <th>Product Selection </th>
                    <td>
                        <div class="mCtrl typeHeader">
                            <p class="gLeft">
                                Selected Items
                                <a href="#none" class="btnNormal"><span><em class="icoDel"></em>Delete</span></a>
                            </p>
                            <p class="gRight">
                                <a href="[link=admin/addProduct]" class="btnCtrl eLayerClick" id="selectProduct"  onclick="window.open(this.href, 'mywin', 'left=20,top=20,width=800,height=500,toolbar=1,resizable=0'); return false;" ><span>Add</span></a>
                                <input type="hidden" value="<?php echo $aNotiData['popup'][product_no]; ?>" id="productNo" class="fText">
                            </p>
                        </div>
                        <div class="mBoard">
                            <table border="1" summary="" class="eChkColor">
                                <caption>제목</caption>
                                <thead>
                                    <th width="5%"></th>
                                    <th width="10%">NO</th>
                                    <th width="15%">Item Nine Minutes</th>
                                    <th width="15%">Product Code</th>
                                    <th width="40%">Product Name</th>
                                    <th width="20%">Product Price</th>
                                </thead>
                                <tbody id="tbodySelectedProd">
                                        <tr>
                                            <td>
                                                <input type = "checkbox" class="products rowChk" name="product_no[]" id = "<?php echo $aProduct[product_no]; ?>" onclick="Display.displaySelectedProduct(this.value);" value="<?php echo $aProduct['product_no']; ?>" checked>
                                            </td>
                                            <td><input type="hidden" name="product_num" value="<?php echo $aProduct['product_no']; ?>" class="fText" ><?php echo $aProduct['product_no']; ?></td>
                                            <td><input type="hidden" name="product_classification" value="<?php echo $aProduct['product_classification']; ?>" class="fText" ><?php echo $aProduct['product_classification']; ?></td>
                                            <td><input type="hidden" name="product_code" value="<?php echo $aProduct['product_code']; ?>" class="fText" ><?php echo $aProduct['product_code']; ?></td>
                                            <td><input type="hidden" name="product_name" value="<?php echo $aProduct['product_name']; ?>" class="fText" ><img src="<?php echo $aProduct['tiny_image']; ?>" class="imgBorder" width="40" height="40"><?php echo $aProduct['product_name']; ?></td>
                                            <td><input type="hidden" name="product_price" value="<?php echo $aProduct['product_price']; ?>" class="fText" ><?php echo $aProduct['product_price']?></td>
                                            <td style="display:none;"><input type="hidden" name="tiny_image" value="<?php echo $aProduct['tiny_image']; ?>" class="fText" ><?php echo $aProduct['tiny_image']; ?></td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="mCtrl typeHeader">
                            <p class="gLeft">
                                Selected Items
                                <a href="#none" claZs="btnNormal"><span><em class="icoDel"></em>Delete</span></a>
                            </p>
                            <p class="gRight">
                                <a href="[link=admin/addProduct]" class="btnCtrl eLayerClick" id="selectProduct" onclick="window.open(this.href, 'mywin','left=20,top=20,width=800,height=500,toolbar=1,resizable=1'); return false;" ><span>Add</span></a>
                            </p>
                        </div>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <br><br><br>
</div>
<div class="mButton gCenter" id="footer">
         <input type="submit" value="Save" class="btnCtrl">
        <a href="#none" class="btnEm"><span>Cancel</span></a>
</div>
</form>
<!-- Layer for add notification criteria -->
<!-- <div id="mLayerTest1" class="mLayer gLarge" style="width:900px !important;" >
    <h2>고객그룹 추가</h2>
    <div class="wrap">
        <div class="mBoard">
            <table border="1" summary="" class="eChkColor" style="width:100%;">
                <caption>제목</caption>
                <div class="mCtrl typeHeader">
                    <h3>Additional customer groups</h3>
                </div>
                <tbody>
                    <tr>
                        <th>Group Name</th>
                        <td><input type="text" class="fText" name=""></td>
                    </tr>
                    <tr>
                        <th>Group Description</th>
                        <td><input type="text" class="fText" name=""></td>
                    </tr>
                </tbody>
            </table>
        </div>
         <div class="mBoard">
            <table border="1" summary="" class="eChkColor" id="tablePopup">
                <caption>Additional customer groups</caption>
                <tbody id='tbodyGroup'>
                    
                </tbody>
            </table>
        </div>
        <div class="mBoard" id="secGroupList">
            <div class="mTitle">
                <h2>Group list</h2>
            </div> 
            <table border="1" summary="" class="eChkColor" id="tablePopup">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Eemail Address</th>
                    <th>Phone Number</th>
                </thead>
                <tbody id='tbodyMember'>
                </tbody>
            </table>
        </div>

    </div>
    <div class="footer">
        <inoput type="submit" class="btnCtrl" id="" value="Save">
        <a href="#none" class="btnNormal eClose" id="btn"><span>Cancel</span></a>
        <a href="#none" class="btnSearch " id="btnSearch"><span>Search</span></a>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div> -->

<!-- Layer for add variable -->
<div id="mLayerTest2" class="mLayer gSmall" >
    <h2>변수 선택</h2>
    <div class="wrap">
        <div class="mTitle">
            <select multiple="multiple" class="fMultiple" id="slcTitles" style="width:100%;" >
                <option value="{customer_name}">Customer Name</option>
                <option value="{customer_id}">Customer ID</option>
                <option value="{customer_email}">Customer Email</option>
            </select>
        </div>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>

<!-- Layer for add variable -->
<div id="mLayerTest3" class="mLayer gSmall" >
    <h2>변수 선택</h2>
    <div class="wrap">
        <div class="mTitle">
            <select multiple="multiple" class="fMultiple" style="width:100%;" id="slcCntents">
                <option value="{customer_name}">Customer Name</option>
                <option value="{customer_id}">Customer ID</option>
                <option value="{customer_email}">Customer Email</option>
            </select>
        </div>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>

