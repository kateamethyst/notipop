<div class="section">
    <div class="mTab typeNav">
        <ul>
            <li class="selected">
                <a href="[link=admin/index]">Notification Management</a>
            </li>
        </ul>
    </div>
    <div class="mTitle" id="secAddNoti">
        <h2>알림등록</h2>
    </div>
    <div class="mBoard">
        <form method="post" action="[link=admin/notimanageaction]">
        <table border="1" summary="" class="eChkColor">
            <caption>제목</caption>
            <tbody>
                <tr>
                    <th scope="row" width="20%">알림기준 선택<span class="icoRequired"></span></th>
                    <td colspan="2" style="padding:0px;">
                        <div class="mFlex flexWrap groupCriteriaBox">
                            <div class="boxAdd"><a href="" class="eLayerClick" id="selectProduct" onclick="window.open('/admin/addcustomergroup', 'mywin', 'left=20,top=20,width=1000,height=500,toolbar=1,resizable=0');"><span>+</span></a></div>
                            <?php
                                if(libValid::isArray($aGroupList) === true) {
                                    foreach ($aGroupList as $mKey => $mValue) {
                            ?>
                                        <label id="labelCriteria" data-id="<?php echo $mValue['seq']?>">
                                            <input  type="radio" data-id="<?php echo $mValue['seq']?>" value="<?php echo $mValue['seq']; ?>" name="group_no" class="rdbGroupCriteria" class="fChk" checked>
                                            <div><?php echo $mValue['name']; ?></div>
                                        </label>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                        <div class="setting" style="display:none">
                            <a href="#" class="btnNormal" id="btnDeleteGroup" data-id="0"><span>✖</span></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 이름<span class="icoRequired"></span></th>
                    <td colspan="2"><input type="text" placeholder="알림이름" width="100%" class="fText" name="name" id="txtNotiName" required></td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 출력위치</th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="top-left" name="position" id="radioDefault" class="fChk" checked>
                            좌상단
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="bottom-left" name="position" id="radioSize" class="fChk">
                            좌하단
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="top-right" name="position" id="radioImage" class="fChk" >
                            우상단
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="bottom-right" name="position" id="radioImage" class="fChk" >
                            우하단
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">알림 타입<span class="icoRequired"></span></th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="TEXT" name="type" id="rdbText" class="fChk" checked>
                            텍스트
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="Coupon" name="type" id="rdbCoupon" class="fChk">
                            쿠폰발급
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="Advertisement" name="type" id="rdbProducts" class="fChk" >
                            상품홍보
                        </label>
                        <!-- <label class="gLabel">
                            <input  type="radio" value="Login/Registration" name="type" id="rdbRegistration" class="fChk" >
                            Login/Register Invited
                        </label> -->
                    </td>
                </tr>
                <!-- <tr>
                    <th scope="row" width="20%">Login or not </th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="true" name="redbAccess" id="rdbText" class="fChk" checked>
                            Login
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="false" name="redbAccess" id="rdbCoupon" class="fChk">
                            Not Login 
                        </label>
                    </td>
                </tr> -->
                <tr>
                    <th scope="row" width="20%" name="page">대상 페이지<span class="icoRequired"></span></th>
                    <td colspan="2">
                        <select id="slcPage" id="page" name="page">
                            <option value="All Page">전체</option>
                            <option value="Main Page">메인</option>
                            <option value="Product Details">상품상세</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Login or not</th>
                    <td colspan="2">
                        <label class="gLabel">
                            <input  type="radio" value="true" name="connection" id="rdbText" class="fChk" checked>
                            Login
                        </label>
                        <label class="gLabel">
                            <input type="radio"  value="false" name="connection" id="rdbCoupon" class="fChk">
                            Not Login 
                        </label>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Notification Period <span class="icoRequired"></span></th>
                    <td colspan="2">
                        <input type="text" id="srh_start_dt" name="srh_start_dt" class="fText gDate txtStartDate" required value="2015-09-02">
                        <a href="javascript:;" id="start_date" class="btnIcon icoCal img_cal1"><span>달력보기</span></a> 
                        <input type="number" id="txtF1Time" maxlength="2" placeholder='01' class="fText txtNumber" required value="08" name="start_time1"> :
                        <input type="number" id="txtF2Time" name="start_time2" maxlength="2" placeholder="00" class="fText txtNumber" required value="00">
                        ~
                        <input type="text" id="srh_end_dt" name="srh_end_dt" class="fText gDate txtEndDate" required value="<?php echo date('Y-m-d'); ?>">
                        <a href="javascript:;" id="end_date" class="btnIcon icoCal img_cal2"><span>달력보기</span></a>
                        <input type="number" id="txtS1Time" maxlength="2" placeholder='23' class="fText txtNumber" required value="23" name="end_time1"> :
                        <input type="number" id="txtS2Time" name="end_time2" maxlength="2" placeholder="00" class="fText txtNumber" required value="00">
                    </td>
                </tr>
                <tr>
                    <th scope="row"  width="20%">Notifications title<span class="icoRequired"></span></th>
                    <td>
                        <input type="text" required class="fText" name="title" id="txtTitle" placeholder="알림 타이틀">
                        <a href="#mLayerTest2" class="btnDate eLayerClick eLayerClick2" >변수추가</a>
                    </td>
                    <td rowspan="3">
                        Preview
                        <div class="displayNotif">
                            <div class="previewBox">
                                <p id="title"></p>
                                <div id="contents">
                                    <p id="descritpion"></p>
                                    <a href="#none" class="btnGeneral" id="btnCoupon"><span>Download Coupon</span></a>
                                    <div id="divProd"></div>
                                    <!-- <div id="register">
                                        <a href="#none" class="btnSubmit" id="btnLogin"><span>Sign in</span></a>
                                         <a href="#none" class="btnEm" id="btnRegister"><span>Sign up</span></a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row" width="20%">Notifications description <span class="icoRequired"></span></th>
                    <td id="tdTextArea">
                        <textarea class="fTextarea" required name="description" cols="10" rows="4" style="width:400px; height:250px;" placeholder="알림 내용" id="txtContent"></textarea>
                        <a href="#mLayerTest3" class="btnDate eLayerClick eLayerClick3" >변수추가</a>
                    </td>
                </tr>
                <tr class="secCoupon">
                    <th>Choose coupon <span class="icoRequired"></span></th>
                    <td>
                        <select id="slcCoupon" name="coupon_no" class="fSelect">
                           <?php foreach ($aCouponList as $mKey => $mValue) { ?>
                               <option value="<?php echo $mValue['coupon_no']; ?>"><?php echo $mValue['coupon_name']; ?></option>
                            <?php } ?>
                        </select>
                        <br>
                        <br>
                    </td>
                </tr>
                <!-- <tr class="secRegistration">
                    <th>Button</th>
                    <td>
                        <label class="gLabel">
                            <input  type="radio" value="2" name="button" id="rdbAddBtn" class="fChk" checked>
                            버튼추가
                        </label>
                        <label class="gLabel">
                            <input  type="radio" value="2" name="button" id="rdbRemoveBtn" class="fChk" >
                            버튼추가하지 않음
                        </label>
                    </td>
                </tr> -->
                <tr class="secProducts">
                    <th>Product Selection <span class="icoRequired"></span></th>
                    <td>
                        <div class="mCtrl typeHeader">
                            <p class="gLeft">
                                Selected Items
                                <a href="#none" id="btnDelProd" class="btnNormal"><span><em class="icoDel"></em>Delete</span></a>
                            </p>
                            <p class="gRight">
                                <a href="" class="btnCtrl eLayerClick" id="selectProduct"  onclick="window.open('[link=admin/addProduct]', 'mywin', 'left=20,top=20,width=800,height=500,toolbar=1,resizable=0');" ><span>Add</span></a>
                            </p>
                        </div>
                        <div class="mBoard">
                            <table border="1" summary="" class="eChkColor">
                                <caption>제목</caption>
                                <thead>
                                    <th width="5%"></th>
                                    <th width="7%">NO</th>
                                    <th width="25%">Item Nine Minutes</th>
                                    <th width="15%">Product Code</th>
                                    <th width="40%">Product Name</th>
                                    <th width="15%">Product Price</th>
                                </thead>
                                <tbody id="tbodySelectedProd">

                                </tbody>
                            </table>
                        </div>
                        <div class="mCtrl typeHeader">
                            <p class="gLeft">
                                Selected Items
                                <a href="#none" id="btnDelProd" class="btnNormal"><span><em class="icoDel"></em>Delete</span></a>
                            </p>
                            <p class="gRight">
                                <a href="" class="btnCtrl eLayerClick" id="selectProduct" onclick="window.open('[link=admin/addProduct]', 'mywin', 'left=20,top=20,width=800,height=500,toolbar=1,resizable=0'); return false;" ><span>Add</span></a>
                            </p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <br><br><br>
</div>
<div class="mButton gCenter" id="footer">
        <input type="submit" value="Save" class="btnCtrl">
        <a href="[link=admin/index]" class="btnEm"><span>Cancel</span></a>
</div>
</form>
<!-- Layer for add notification criteria -->
<!-- <div id="mLayerTest1" class="mLayer gLarge" style="width:900px !important;" >
    <h2>고객그룹 추가</h2>
    <div class="wrap">
        <div class="mBoard">
            <table border="1" summary="" class="eChkColor" style="width:100%;">
                <caption>제목</caption>
                <div class="mCtrl typeHeader">
                    <h3>Additional customer groups</h3>
                </div>
                <tbody>
                    <tr>
                        <th>Group Name</th>
                        <td><input type="text" class="fText" name=""></td>
                    </tr>
                    <tr>
                        <th>Group Description</th>
                        <td><input type="text" class="fText" name=""></td>
                    </tr>
                </tbody>
            </table>
        </div>
         <div class="mBoard">
            <table border="1" summary="" class="eChkColor" id="tablePopup">
                <caption>Additional customer groups</caption>
                <tbody id='tbodyGroup'>
                    
                </tbody>
            </table>
        </div>
        <div class="mBoard" id="secGroupList">
            <div class="mTitle">
                <h2>Group list</h2>
            </div> 
            <table border="1" summary="" class="eChkColor" id="tablePopup">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email Address</th>
                    <th>Phone Number</th>
                </thead>
                <tbody id='tbodyMember'>
                </tbody>
            </table>
        </div>

    </div>
    <div class="footer">
        <a href="#none" class="btnCtrl btnSubmit" id="btnSaveGroup"><span>Save</span></a>
        <a href="#none" class="btnNormal eClose" id="btn"><span>Cancel</span></a>
        <a href="#none" class="btnSearch " id="btnSearch"><span>Search</span></a>

    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div> -->

<!-- Layer for add variable -->
<div id="mLayerTest2" class="mLayer gSmall" >
    <h2>변수 선택</h2>
    <div class="wrap">
        <div class="mTitle">
            <select multiple="multiple" class="fMultiple" id="slcTitles" style="width:100%;" >
                <option value="{customer_name}">Customer Name</option>
                <option value="{customer_id}">Customer ID</option>
                <option value="{customer_email}">Customer Email</option>
            </select>
        </div>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>

<!-- Layer for add variable -->
<div id="mLayerTest3" class="mLayer gSmall" >
    <h2>변수 선택</h2>
    <div class="wrap">
        <div class="mTitle">
            <select multiple="multiple" class="fMultiple" style="width:100%;" id="slcCntents">
                <option value="{customer_name}">Customer Name</option>
                <option value="{customer_id}">Customer ID</option>
                <option value="{customer_email}">Customer Email</option>
            </select>
        </div>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>

