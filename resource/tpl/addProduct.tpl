<body id="popup">
<div id="wrap">
    <div class="headingArea">
        <div class="mTitle">
            <h1>상품 추가</h1>
        </div>
    </div>
    <div class="section" id="QA_benefit_add1">
        <form name="productAddForm" id="productAddForm" method="post" action="[link=admin/addProduct]">
            <input type="hidden" name="search_flag" value="T">
            <!-- 샵넘버 유지  -->
            <input type="hidden" name="shop_no" value="<?=(int)$aParams['shop_no']?>">
            <div class="mBoard">
                <table border="1" summary="">
                    <caption>상품 검색</caption>
                    <colgroup>
                        <col style="width:154px;*width:135px;">
                        <col style="width:auto;">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">검색분류</th>
                        <td id="searchTd">
                            <ul class="mForm typeVer" id='typeVer'>

                                <!--  시작 하자마자  -->
                                <?php
                                if (count($aParams['search']) === 0) {
                                    ?>

                                    <div id="item">
                                        <li>
                                            <select class="fSelect" id='searchCategory' name='searchCategory[]'>
                                                <option value='product_name'<?=$aParams['searchCategory'] === 'product_name' ? "selected='selected'" : null?>>상품명</option>
                                                <option value='eng_product_name'<?=$aParams['searchCategory'][0] === 'eng_product_name' ? "selected='selected'" : null?>>영문상품명</option>
                                                <option value='product_name_supplier'<?=$aParams['searchCategory'][0] === 'product_name_supplier' ? "selected='selected'" : null?>>공급사 상품명</option>
                                                <option>--------------------</option>
                                                <option value='product_no'<?=$aParams['searchCategory'][0] === 'product_no' ? "selected='selected'" : null?>>상품번호</option>
                                                <option value='product_code'<?=$aParams['searchCategory'][0] === 'product_code' ? "selected='selected'" : null?>>상품코드</option>
                                                <option value='self_product_code'<?=$aParams['searchCategory'][0] === 'self_product_code' ? "selected='selected'" : null?>>자체 상품코드</option>
                                                <option value='item_code'<?=$aParams['searchCategory'][0] === 'item_code' ? "selected='selected'" : null?>>품목코드</option>
                                                <option value='self_item_code'<?=$aParams['searchCategory'][0] === 'self_item_code' ? "selected='selected'" : null?>>자체 품목코드</option>
                                                <option>--------------------</option>
                                                <option value='manufacturer_code'<?=$aParams['searchCategory'][0] === 'manufacturer_code' ? "selected='selected'" : null?>>제조사</option>
                                                <option value='supplier_id' <?=$aParams['searchCategory'][0] === 'supplier_id' ? "selected='selected'" : null?>>공급사</option>
                                                <option value='brand_code' <?=$aParams['searchCategory'][0] === 'brand_code' ? "selected='selected'" : null?>>브랜드</option>
                                                <option value='trend_code' <?=$aParams['searchCategory'][0] === 'trend_code' ? "selected='selected'" : null?>>트렌드</option>
                                                <option>--------------------</option>
                                                <option value='model_name'<?=$aParams['searchCategory'][0] === 'model_name' ? "selected='selected'" : null?>>모델명</option>
                                                <option value='origin_place_value'<?=$aParams['searchCategory'][0] === 'origin_place_value' ? "selected='selected'" : null?>>원산지</option>
                                                <option value='product_status'<?=$aParams['searchCategory'][0] === 'product_status' ? "selected='selected'" : null?>>상품상태</option>
                                                <option value='product_tag' <?=$aParams['searchCategory'][0] === 'product_tag' ? "selected='selected'" : null?>>상품 검색어</option>
                                                <option value='product_weight'<?=$aParams['searchCategory'][0] === 'product_weight' ? "selected='selected'" : null?>>상품 전체중량</option>
                                                <option>--------------------</option>
                                            </select>


                                            <input type="text" class="fText" style="width:300px;" name='search[]' value="<?=$aParams['search'][0] ? $aParams['search'][0] : null?>">

                                            <a href="#none" class="btnIcon icoMinus eMinus">
                                                <span>삭제</span>
                                            </a>
                                            <a href="#none" class="btnIcon icoPlus ePlus">
                                                <span>추가</span>
                                            </a>
                                        </li>
                                    </div><!--  여기 까지  -->
                                    <?php
                                } else {
                                    foreach ($aParams['search'] as $iiiKey => $sssVal) {
                                        ?>
                                        <div id="item">
                                            <li>
                                                <select class="fSelect" id='searchCategory' name='searchCategory[]'>
                                                    <option value='product_name'<?=$aParams['searchCategory'][$iiiKey] === 'product_name' ? "selected='selected'" : null?>>상품명</option>
                                                    <option value='eng_product_name'<?=$aParams['searchCategory'][$iiiKey] === 'eng_product_name' ? "selected='selected'" : null?>>영문상품명</option>
                                                    <option value='product_name_supplier' <?=$aParams['searchCategory'][$iiiKey] === 'product_name_supplier' ? "selected='selected'" : null?>>공급사 상품명</option>
                                                    <option>--------------------</option>
                                                    <option value='product_no'<?=$aParams['searchCategory'][$iiiKey] === 'product_no' ? "selected='selected'" : null?>>상품번호</option>
                                                    <option value='product_code'<?=$aParams['searchCategory'][$iiiKey] === 'product_code' ? "selected='selected'" : null?>>상품코드</option>
                                                    <option value='self_product_code'<?=$aParams['searchCategory'][$iiiKey] === 'self_product_code' ? "selected='selected'" : null?>>자체 상품코드</option>
                                                    <option value='item_code'<?=$aParams['searchCategory'][$iiiKey] === 'item_code' ? "selected='selected'" : null?>>품목코드</option>
                                                    <option value='self_item_code'<?=$aParams['searchCategory'][$iiiKey] === 'self_item_code' ? "selected='selected'" : null?>>자체 품목코드</option>
                                                    <option>--------------------</option>
                                                    <option value='manufacturer_code'<?=$aParams['searchCategory'][$iiiKey] === 'manufacturer_code' ? "selected='selected'" : null?>>제조사</option>
                                                    <option value='supplier_id' <?=$aParams['searchCategory'][$iiiKey] === 'supplier_id' ? "selected='selected'" : null?>>공급사</option>
                                                    <option value='brand_code' <?=$aParams['searchCategory'][$iiiKey] === 'brand_code' ? "selected='selected'" : null?>>브랜드</option>
                                                    <option value='trend_code' <?=$aParams['searchCategory'][$iiiKey] === 'trend_code' ? "selected='selected'" : null?>>트렌드</option>
                                                    <option>--------------------</option>
                                                    <option value='model_name'<?=$aParams['searchCategory'][$iiiKey] === 'model_name' ? "selected='selected'" : null?>>모델명</option>
                                                    <option value='origin_place_value'<?=$aParams['searchCategory'][$iiiKey] === 'origin_place_value' ? "selected='selected'" : null?>>원산지</option>
                                                    <option value='product_status'<?=$aParams['searchCategory'][$iiiKey] === 'product_status' ? "selected='selected'" : null?>>상품상태</option>
                                                    <option value='product_tag' <?=$aParams['searchCategory'][$iiiKey] === 'product_tag' ? "selected='selected'" : null?>>상품 검색어</option>
                                                    <option value='product_weight'<?=$aParams['searchCategory'][$iiiKey] === 'product_weight' ? "selected='selected'" : null?>>상품 전체중량</option>
                                                    <option>--------------------</option>
                                                </select>
                                                <?php if ($aParams['searchCategory'][$iiiKey] === 'manufacturer_code' || $aParams['searchCategory'][$iiiKey] === 'supplier_id' || $aParams['searchCategory'][$iiiKey] === 'brand_code' || $aParams['searchCategory'][$iiiKey] === 'trend_code') { ?>
                                                    <select class="fSelect" name='search[]' id='searchCategoryselect'>
                                                        <!-- 코드명 =>0, 1 이렇게 인데 -->
                                                        <?php foreach ($aCodeList[$aParams['searchCategory'][$iiiKey]] as $iKey => $aVal) {
                                                            //                                         foreach ($aVal as $iSecondRow => $aSecondVal) {?><?php if ($aParams['searchCategory'][$iiiKey] === 'manufacturer_code') { ?>
                                                                <option value=<?php echo $aVal['manufacturer_code'] ?> <?=$aVal['manufacturer_code'] === $aParams['search'][$iiiKey] ? "selected='selected'" : null?>><?php echo $aVal['manufacturer_name'] ?></option>
                                                            <?php } else if ($aParams['searchCategory'][$iiiKey] === 'supplier_id') { ?>
                                                                <option value= <?php echo $aVal['supplier_code'] ?> <?=$aVal['supplier_code'] === $aParams['search'][$iiiKey] ? "selected='selected'" : null?>><?php echo $aVal['supplier_name'] ?></option>
                                                            <?php } else if ($aParams['searchCategory'][$iiiKey] === 'brand_code') { ?>
                                                                <option value= <?php echo $aVal['brand_code'] ?> <?=$aVal['brand_code'] === $aParams['search'][$iiiKey] ? "selected='selected'" : null?>><?php echo $aVal['brand_name'] ?></option>
                                                            <?php } else if ($aParams['searchCategory'][$iiiKey] === 'trend_code') { ?>
                                                                <option value= <?php echo $aVal['trend_code'] ?> <?=$aVal['trend_code'] === $aParams['search'][$iiiKey] ? "selected='selected'" : null?>><?php echo $aVal['trend_name'] ?></option>
                                                            <?php } ?>


                                                        <?php } ?>
                                                    </select>
                                                <?php } else { ?>
                                                    <input type="text" class="fText" style="width:300px;" name='search[]' value="<?=$aParams['search'][$iiiKey] ? $aParams['search'][$iiiKey] : null?>">
                                                <?php } ?>
                                                <a href="#none" class="btnIcon icoMinus eMinus">
                                                    <span>삭제</span>
                                                </a>
                                                <a href="#none" class="btnIcon icoPlus ePlus">
                                                    <span>추가</span>
                                                </a>
                                            </li>
                                        </div>


                                    <?php } ?>

                                <?php } ?>
                            </ul>
                            <p class="txtInfo">최대 10개까지 복수검색 가능</p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">상품분류</th>
                        <td>
                            <label class="gLabel">
                                <input type="checkbox" name="is_Allcontain" id="containState1" value="T" <?=$aParams['is_Allcontain'] === 'T' ? "checked='checked'" : null?> class="fChk">
                                전체 상품분류 보기
                            </label>
                            <div class="gSingle">
                                <input type="hidden" class="eCurrDepth1" value="<?php echo $aParams['depth1']; ?>">
                                <select class="prdCategory1" name='depth1' id="prdCategory1">
                                    <option value=null>- 대분류 선택 -</option>


                                </select>
                                <select class="prdCategory2" name='depth2' id="prdCategory2">
                                    <option value=null>- 중분류 선택 -</option>

                                    <!-- <option class="txtHide">중분류3</option> --><!-- 표시하지 않는 분류일 경수 class="txtHide" 추가 -->
                                </select>
                                <select class="prdCategory3" name='depth3' id="prdCategory3">
                                    <option value=null>- 소분류 선택 -</option>

                                </select>
                                <select class="prdCategory4" name='depth4' id="prdCategory4">
                                    <option value=null>- 상세분류 선택 -</option>

                                </select>
                                <label>
                                    <input type="checkbox" name='is_sub_category' id='containState2' value='T' <?=$aParams['is_sub_category'] === 'T' ? "checked='checked'" : null?>class="fChk">
                                    하위분류 포함검색
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">상품등록일</th>
                        <td>
                            <select class="fSelect category" name="DateType">
                                <option value='regist_date' <?=$aParams['DateType'] === 'regist_date' ? "selected='selected'" : null?>>상품등록일</option>
                                <option value='modify_date' <?=$aParams['DateType'] === 'modify_date' ? "selected='selected'" : null?>>상품최종수정일</option>
                            </select>
                            <!-- <?php echo $aSrhDate['srh_start_1'] == $aParam['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> -->
                            <a href="#none" <?php echo $aDate['srh_start_1'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_1']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>오늘</span>
                            </a>
                            <a href="#none" <?php echo $aDate['srh_start_3'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_3']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>3일</span>
                            </a>
                            <a href="#none" <?php echo $aDate['srh_start_7'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_7']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>7일</span>
                            </a>
                            <a href="#none" <?php echo $aDate['srh_start_30'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_30']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>1개월</span>
                            </a>
                            <a href="#none" <?php echo $aDate['srh_start_90'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_90']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>3개월</span>
                            </a>
                            <a href="#none" <?php echo $aDate['srh_start_180'] === $aParams['srh_start_dt'] ? "class='btnDate eDateBtn btnSrhDate selected'" : "class='btnDate eDateBtn btnSrhDate'" ?> start_dt="<?php echo $aDate['srh_start_180']; ?>" end_dt="<?php echo $aDate['srh_end']; ?>">
                                <span>1년</span>
                            </a>
                            <input type="text" value="<?php echo $aParams['srh_start_dt']; ?>" id="srh_start_dt" name="srh_start_dt" class="fText" style="width:65px;"/>
                            <a href="javascript:;" id="start_date" class="btnIcon icoCal img_cal1">
                                <span>달력보기</span>
                            </a>
                            ~
                            <input type="text" value="<?php echo $aParams['srh_end_dt']; ?>" id="srh_end_dt" name="srh_end_dt" class="fText" style="width:65px;"/>
                            <a href="javascript:;" id="end_date" class="btnIcon icoCal img_cal2">
                                <span>달력보기</span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">진열/판매상태</th>
                        <td>
                            <label class="gLabel glabelallstate <?=$aParams['all_status'] === 'T' ? 'eSelected' : ''?> ">
                                <input type="checkbox" id="checkAllState" class="fChk allChk" <?=$aParams['all_status'] === 'T' ? "checked='checked'" : ''?>>
                                전체
                            </label>
                            <label class="gLabel glabelstate <?=$aParams['is_sellingT'] === 'T' ? 'eSelected' : ''?>">
                                <input type="checkbox" id="state1" name='is_sellingT' value='T' <?=$aParams['is_sellingT'] === 'T' ? "checked='checked'" : ''?> class="fChk">
                                판매함
                            </label>
                            <label class="gLabel glabelstate <?=$aParams['is_sellingF'] === 'F' ? 'eSelected' : ''?>">
                                <input type="checkbox" id="state2" name='is_sellingF' value='F' <?=$aParams['is_sellingF'] === 'F' ? "checked='checked'" : ''?> class="fChk">
                                판매안함
                            </label>
                            <label class="gLabel glabelstate <?=$aParams['is_displayT'] === 'T' ? 'eSelected' : ''?>">
                                <input type="checkbox" id="state3" name='is_displayT' value='T' <?=$aParams['is_displayT'] === 'T' ? "checked='checked'" : ''?> class="fChk">
                                진열함
                            </label>
                            <label class="gLabel glabelstate <?=$aParams['is_displayF'] === 'F' ? 'eSelected' : ''?>">
                                <input type="checkbox" id="state4" name='is_displayF' value='F' <?=$aParams['is_displayF'] === 'F' ? "checked='checked'" : ''?> class="fChk">
                                진열안함
                            </label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="mButton gCenter">
                <a href="#none" class="btnSearch eSearch">
                    <span>검색</span>
                </a>
            </div>
        </form>
    </div>

    <div class="section" id="QA_benefit_add2">
        <div class="mTitle">
            <h2>상품 목록</h2>
        </div>
        <!-- productAddCheckForm -->
        <!-- 여기 처리  -->
        <form name="productAddCheckForm" id="productAddCheckForm" method="post" action="[link=admin/Test2]">
            <div class="mState">
                <div class="gLeft">
                    <p class="total">[총
                        <strong><?=$aSearchData['totalcount']?></strong>
                        개]
                    </p>
                </div>
                <div class="gRight">
                    <select class="fSelect" id="limit">
                        <?php
                        foreach ($aLimitList as $iLimit) {
                            ?>
                            <option value="<?php echo $iLimit; ?>" <?=(int)$aParams["limit"] === $iLimit ? "selected='selected'" : null?>><?php echo $iLimit; ?>개씩 보기</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="mBoard">
                <table border="1" summary="" class="eChkColor">
                    <caption>상품 목록</caption>
                    <colgroup>
                        <col class="chk">
                        <col style="width:50px">
                        <col style="width:80px">
                        <col style="width:80px">
                        <col style="width:auto">
                        <col style="width:120px">
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope="col">
                            <input type="checkbox" class="allrowCheckList">
                        </th>
                        <th scope="col">No</th>
                        <th scope="col">상품구분</th>
                        <th scope="col">상품코드</th>
                        <th scope="col">상품명</th>
                        <th scope="col">판매가</th>
                    </tr>
                    </thead>
                    <?php
                    $iNum = $aSearchData['totalcount'] - (($aParams['page'] - 1) * $aParams["limit"]);
                    if (libValid::isArray($aSearchData['list']) === true) {
                        ?>
                        <tbody class="center">
                        <?php
                        unset($aSearchData['totalcount']);
                        $aOptionValue = array();
                        foreach ($aSearchData['list'] as $iKey => $aVal) { ?>

                            <tr class="">
                                <td>
                                    <input type="checkbox" class="rowCheckList" name="rowCheckList[]" value="<?=$iKey?>">
                                </td>
                                <td><?php echo $iNum; ?></td>
                                <td>
                                    <input type="hidden" name="product_no[]" value="<?php echo $aVal['product_no']; ?>">
                                    <input type="hidden" name="is_set_product[]" value="<?=$aVal['is_set_product']?>"/>
                                    <?php if ($aVal['is_set_product'] === 'T') {
                                    ?>
                                    <input type="hidden" name="product_classification[]" value="<?=$aVal['product_classification']?>">
                                    <?php
                                        echo $aVal['product_classification'];
                                    } else {
                                    ?>

                                    <input type="hidden" name="product_classification[]" value="<?=$aVal['product_classification']?>">
                                    <?php
                                        echo $aVal['product_classification'];
                                    }
                                    ?></td>
                                <td class="txtCode">
                                    <input type="hidden" name="product_code[]" value="<?=$aVal['product_code']?>"/>
                                    <?php echo $aVal['product_code']; ?></td>
                                <td class="left">
                                    <div class="gGoods gMedium">
                                        <input type="hidden" name="tiny_image[]" value="<?=$aVal['thumb_image']?>"/>
                                        <span class="frame">
                                            <img src=<?php echo $aVal['thumb_image'] ?> width="44" height="44" alt="">
                                        </span>
                                        <input type="hidden" name="product_name[]" value="<?=$aVal['product_name']?>">

                                        <p><?php echo $aVal['product_name']; ?></p>
                                        <ul class="etc">
                                            <?php

                                            if (empty($aVal['option']) === false) {
                                                foreach ($aVal['option'] as $iiiIndex => $sssVal) {
                                                    if (count($sssVal) < 2) {
                                                        ?>

                                                        <li><?php echo $iiiIndex ?> : <?php echo $sssVal[0] ?></li>
                                                    <?php } else { ?>
                                                        <li><?php echo $iiiIndex ?> : <?php
                                                            $iCheckCount = 0;
                                                            foreach ($sssVal as $aOptionVal) {
                                                                if (count($sssVal) - 1 === $iCheckCount) {// 마지막인경우
                                                                    echo $aOptionVal;
                                                                } else {
                                                                    echo $aOptionVal . ", ";
                                                                }

                                                                $iCheckCount++;
                                                            } ?></li>
                                                    <?php } ?><?php }
                                            } ?>
                                        </ul>

                                        <?php
                                        if (empty($aVal['option']) === false) {
                                            $iCount = count($aVal['option']) - 1;
                                            $iCheckIndex = 0;
                                            foreach ($aVal['option'] as $iiiiIndex => $ssssVal) {
                                                $aOptionValue[] = $iiiiIndex . ': ' . implode(', ', $ssssVal);
                                                ?>

                                                <?php $iCheckIndex++;
                                            }

                                        } ?>
                                    </div>
                                </td>
                                <td class="right">
                                    <?php
                                    if ($sCurrency === 'KRW') {
                                        $sCurrency = '';
                                    }
                                    ?>
                                    <input type="hidden" name="product_price[]" value="<?=$aVal['product_price']?>">
                                    <input type="hidden" name="option[]" value="<?php echo $aOptionValue[$iKey]; ?>">
                                    <?php echo $sCurrency;
                                    echo $aVal['product_price']; ?>
                                </td>

                            </tr>

                            <?php $iNum--;
                        }
                        ?>

                        </tbody>
                        <?php
                    } else { ?>
                        <tbody class="empty">
                        <tr>
                            <td colspan="6">등록된 상품이 없습니다.</td>
                        </tr>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
            <? echo $aPaging['sPaging'] ?>
    </div>
</div>
<div id="footer">
    <a href="#layerAddPrd" class="btnSubmit eModal">
        <span>선택</span>
    </a>
    <a href="#none" class="btnEm" onclick="window.close();">
        <span>닫기</span>
    </a>
</div>
<input type="hidden" class="fText" value="" id="setter">
</form>

<style type="text/css">
    /* 딤드처리는 하지 않기 위해 css 에서 display:none 처리합니다 */
    .dimmed { display: none; }
</style>

<!-- 상품추가 팝업 -->
<div id="layerAddPrd" class="mLayer gSmall">
    <h2>알림</h2>
    <div class="wrap">
        <p class="gCenter">
            <strong class="txtEm eAddCount">0000</strong>
            개의 상품이 추가되었습니다.
            <br>
            동일한 상품은 한개만 추가됩니다.
        </p>
    </div>
    <button type="button" class="btnClose eClose">닫기</button>
</div>

<div id="baseitem" style="display:none;">
    <li>
        <select class="fSelect" id='searchCategory' name='searchCategory[]'>
            <option value='product_name'>상품명</option>
            <option value='eng_product_name'>영문상품명</option>
            <option value='product_name_supplier'>공급사 상품명</option>
            <option>--------------------</option>
            <option value='product_no'>상품번호</option>
            <option value='product_code'>상품코드</option>
            <option value='self_product_code'>자체 상품코드</option>
            <option value='item_code'>품목코드</option>
            <option value='self_item_code'>자체 품목코드</option>
            <option>--------------------</option>
            <option value='manufacturer_code'>제조사</option>
            <option value='supplier_id'>공급사</option>
            <option value='brand_code'>브랜드</option>
            <option value='trend_code'>트렌드</option>
            <option>--------------------</option>
            <option value='model_name'>모델명</option>
            <option value='origin_place_value'>원산지</option>
            <option value='product_status'>상품상태</option>
            <option value='product_tag'>상품 검색어</option>
            <option value='product_weight'>상품 전체중량</option>
            <option>--------------------</option>
        </select>

        <input type="text" class="fText" style="width:300px;" name='search[]' value="">

        <a href="#none" class="btnIcon icoMinus eMinus">
            <span>삭제</span>
        </a>
        <a href="#none" class="btnIcon icoPlus ePlus">
            <span>추가</span>
        </a>
    </li>
</div>
</body>
