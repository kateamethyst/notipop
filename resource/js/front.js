$(document).ready(function() {
    var iMinutes = 30;

    var date = new Date();

    var aCondiResult = [];

    var aGroups = [];

    var aOperators = {
        'Equals' : '===',
        'Is less than' : '<',
        'Is less than or equals' : '<=',
        'Is greater than' : '>',
        'Is greater than or equals' : '>='
    };

    date.setTime(date.getTime() + (iMinutes * 60 * 1000));

    var Conditions = {
        init : function () {
            var aUserProfile = $.parseJSON($.cookie('user_profile'));
            var aConditions = $.parseJSON($.cookie('conditions'));
            this.checkConditions(aConditions, aUserProfile);
        },

        checkConditions : function(aConditions, aUserProfile) {
            var aResult = [];
            var aResultList = [];
            $.each(aConditions, function(mKey, aCondition) {
                $.each(aCondition.criteria, function(mKey, aCriteria){
                    if (aCriteria.option === 'Age') {
                        var bResult = Conditions.checkAge(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else if (aCriteria.option === 'Gender') {
                        var bResult = Conditions.checkGender(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else if (aCriteria.option === 'Shopping mall customer level') {
                        var bResult = Conditions.checkLevel(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else if (aCriteria.option === 'Points') {
                        var bResult = Conditions.checkPoints(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else if (aCriteria.option === 'Date of Registration') {
                        var bResult = Conditions.checkRegistration(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else if (aCriteria.option === 'Email') {
                        var bResult = Conditions.checkEmailSubscription(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    } else {
                        var bResult = Conditions.checkAnniversary(aCriteria, aUserProfile);
                        aResult.push(bResult);
                    }
                });
                var tempData = {
                    'result' : aResult,
                    'group_no' : aCondition.seq
                };
                aResultList.push(tempData);
                aResult = [];
            });
            Conditions.getMeetConditions(aResultList);
        },

        checkAge : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.age, aCriteria.details.data);
                return bResult;
        },

        checkGender : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.sex, aCriteria.details.data);
                return bResult;
        },

        checkLevel : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.level, aCriteria.details.data);
                return bResult;
        },

        checkPoints : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.total_mileage, aCriteria.details.data);
                return bResult;
        },

        checkRegistration : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.regist_date, aCriteria.details.data);
                return bResult;
        },

        checkEmailSubscription : function (aCriteria, aUserProfile) {
                var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.is_news_mail, aCriteria.details.data);
                return bResult;

        },

        checkAnniversary : function (aCriteria, aUserProfile) {
                // var bResult = Conditions.checkOperator(aCriteria.details.condition, aUserProfile.regist_date, aCriteria.details.data);
                // return bResult;
        },

        checkOperator : function(sOperator, mGiven, mBasis) {
            if (sOperator === 'Equals') {
                if (mGiven === mBasis[0]) {
                    return true;
                }
                return false;
            } else if (sOperator === 'Is less than') {
                if (mGiven < mBasis[0]) {
                    return true;
                }
                return false;
            } else if (sOperator === 'Is less than or equals') {
                if (mGiven <= mBasis[0]) {
                    return true;
                }
                return false;
            } else if (sOperator === 'Is greater than') {
                if (mGiven > mBasis[0]) {
                    return true;
                }
                return false;
            } else if (sOperator === 'Is greater than or equals') {
                if (mGiven >= mBasis[0]) {
                    return true;
                }
                return false;
            } else {
                if (mGiven >= mBasis[0] && mGiven <= mBasis[1]) {
                    return true;
                }
                return false;
            }
        },

        checkValue : function (bResult) {
            return bResult === false;
        },

        getMeetConditions : function (aResultList) {
            var aGroupNos = [];
            $.each(aResultList, function(mKey, aResult) {
                var mResult = aResult.result.find(Conditions.checkValue);
                if (mResult !== false) {
                    aGroupNos.push(aResult.group_no);
                }
            });
            if (aGroupNos.length !== 0) {
                
                Popup.getPopup(aGroupNos);
            }
        },

        getFlags : function () {
            
        }
    }

    /**
     * [Popup description]
     * @type {Object}
     */
    var Popup = {
        init : function () {
            this.checkStorage();
        },

        checkStorage : function () {
            if (!$.cookie('user_profile') || !$.cookie('conditions') ) {
                this.getUserProfile();
                this.getConditions();
                $(".popupBody").load(document.URL);
            } else {
                Conditions.init();
            }
        },

        getUserProfile : function () {
            $.ajax({
                type : 'GET',
                url : '[link=api/member]',
                encode : 'json',
                success : function (mResponse) {
                    $.cookie('user_profile', JSON.stringify(mResponse.Data), { expires : date });
                }
            });
        },

        getConditions : function () {
            $.ajax({
                type : 'GET',
                url : '[link=api/group]',
                encode : 'json',
                success : function (mResponse) {
                    $.cookie('conditions', JSON.stringify(mResponse.Data), { expires : date });
                }
            });
        },

        getPopup : function (aGroupNos) {
            var oData = {
                'group_no' : aGroupNos,
                'find' : true
            };
            $.ajax({
                type : 'GET',
                url : '[link=api/noti]',
                encode : 'json',
                data : oData,
                success : function (mResponse) {
                    console.log(mResponse);
                    Popup.checkPopup(mResponse.Data);
                }
            });
        },

        checkPopup : function (aPopups) {
            var oData = {
                'option' : 'getUserFlags'
            };
            var aCheckPopup = [];
            var dDate = new Date();
            var sDate = dDate.getFullYear() + '-' + (dDate.getMonth() + 1) + '-' + dDate.getDay();
             $.ajax({
                type : 'GET',
                url : '[link=api/flags]',
                encode : 'json',
                data : oData,
                success : function (mResponse) {
                    var aFlags = mResponse.Data;
                    if (aFlags === null) {
                        Popup.setupIframe(aPopups);
                    } else {
                        $.each(aPopups, function(iKey, aPopup) {
                            var aResult = Popup.checkExistence(aFlags, aPopup.noti_id);
                            var iResult = aResult.indexOf(true);
                            if (iResult === -1) {
                                aCheckPopup.push(aPopup);
                                // console.log('push');
                            } else {
                                $.each(aFlags, function(iKey, aFlag) {
                                    if (aFlag.noti_id === aPopup.noti_id) {
                                        if (aFlag.downloaded === false || aFlag.downloaded === 'false') {
                                            if (new Date(aFlag.display_at) <= new Date(sDate)) {
                                                aCheckPopup.push(aPopup);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                        Popup.setupIframe(aCheckPopup); 
                    }
                }
            });
        },

        checkExistence : function (aParams, mValue) {
            return aParams.map(function(aParam) {
                if (aParam.noti_id === mValue) {
                    return true;
                }
            });
        },

        setupIframe : function (aNotification) {
            var sIframe = '';
            $.each(aNotification, function (iKey, aValue) {
                if (aValue.type === 'Advertisement') {
                    // sIframe = sIframe + '<iframe src="http://cstoredemoph.cafe24.com/apps/notipop/dummy.html?product_no=' + aValue.add_ons.product_no + '" id="' + aValue.add_ons.product_no + '" class="product-iframe"></iframe>';
                }
            });

            $('.popupBody').html(sIframe);
            setTimeout(function() { Popup.displayPopup(aNotification); }, 1000);
        },

        displayPopup : function (aNotiPopup) {
            $.each(aNotiPopup, function(mKey, aNoti) {
                
                if (aNoti.type === 'Advertisement') {
                    aNoti.popup.product_discount = $( '#' + aNoti.popup.product_no).contents().find('#span_product_price_sale').find('span').text();
                }

                var sStartLayout = [
                    '<div id="layer' + aNoti.noti_id + '" data-id="' + aNoti.noti_id + '" class="popupBox gSmall ' + aNoti.position + '">',
                    '<h2>' + aNoti.popup.title + '</h2>'
                ].join('');

                var sEndLayout = [
                    '<button data-id="' + aNoti.noti_id + '" type="button" class="btnClose eClose">닫기</button>',
                    '</div>'
                ].join('');

                var sContent = Popup.generatePopupHtml(aNoti);

                var sHtml  = sStartLayout + sContent + sEndLayout;

                var sPopup = Popup.replaceClosure(sHtml);
                $('.popupBody').append(sPopup);

                oData = {
                    'view' : 1,
                    'noti_id' : aNoti.noti_id
                };
                Popup.updateStats(oData);
            });
        },

        generatePopupHtml : function (aNoti) {
            if (aNoti.type === 'Coupon') {
                return Popup.setCouponHtmlContent(aNoti);
            } else if (aNoti.type === 'Advertisement') {
                return Popup.setProductHtmlContent(aNoti);
            } else {
                return Popup.setTextHtmlContent(aNoti);
            }
        },

        setCouponHtmlContent : function (aNoti) {
            return [
                '<div class="wrap">',
                '<p>' + aNoti.popup.contents + '</p>',
                '<div class="contents">',
                '<a target="blank" data-id="' + aNoti.noti_id + '" href="[link=' + aNoti.add_ons.coupon_direct_url + aNoti.add_ons.coupon_no + ']" class="btnEm btnCouponDownload"><span>Download Coupon</span></a>',
                '</div>',
                '</div>'
            ].join('');
        },

        setProductHtmlContent : function (aNoti) {
            return [
                '<div class="wrap" id="product-details" data-id="' + aNoti.add_ons.product_no + '">',
                '<p>' + aNoti.popup.contents + '</p>',
                '<div class="mFlex" id="prod' + aNoti.add_ons.product_no + '">',
                '<img src="' + aNoti.add_ons.tiny_image + '">',
                '<div class="contents">',
                '<h3><a class="text-blue click-stats" target="_blank" href="http://devsdk0006.cafe24.com/product/detail.html?product_no=' + aNoti.add_ons.product_no + '" data-id="' + aNoti.noti_id + '">' + aNoti.add_ons.product_name + ' - ' + aNoti.add_ons.product_code + '</a></h3>',
                '<h4>Original Price : ' + aNoti.add_ons.product_price + '</h4>',
                '<h4>Discount : ' + aNoti.popup.product_discount + '</h4>',
                '</div>',
                '</div>',
                '</div>'
            ].join('');
        },

        setTextHtmlContent : function(aNoti) {
            return [
                '<div class="wrap">',
                '<p>' + aNoti.popup.contents + '</p>',
                '</div>'
            ].join('');
        },

        replaceClosure : function (sData) {
            var aUserProfile = $.parseJSON($.cookie('user_profile'));
            var aClosure = {
                '{customer_name}' : aUserProfile.name,
                '{customer_email}' : aUserProfile.email,
                '{customer_id}' : aUserProfile.member_id
            };
            $.each(aClosure, function(mKey, mValue){
                sData = sData.split(mKey).join(mValue);
            });

            return sData;
        },

        updateStats : function(oData) {
            $.ajax({
                type : 'POST',
                url : '[link=api/stats]',
                encode : 'json',
                data : oData,
                success : function (mResponse) {
                    
                }
            });
        },

        updateFlags : function (oData) {
            $.ajax({
                type : 'POST',
                url : '[link=api/flags]',
                encode : 'json',
                data : oData,
                success : function (mResponse) {
                    console.log(mResponse);
                }
            });
        }
    }

    Popup.init();

    $('.btnClose').live('click', function(){
        sID = '#layer' + $(this).attr('data-id');
        $(sID.trim()).remove();
        var sDate = new Date();
        sDate.setDate(sDate.getDate() + 1);
        var oData = {
            'hide' : true,
            'display_at' : sDate.getFullYear() + '-' + (sDate.getMonth() + 1) + '-' + sDate.getDay(),
            'noti_id' : $(this).attr('data-id'),
            'option' : 'flags'
        };
        
        Popup.updateFlags(oData);
    });

    $('.click-stats').live('click', function(){
        oData = {
            'click' : 1,
            'noti_id' : $(this).attr('data-id')
        };

        Popup.updateStats(oData);
    });

    $('.btnCouponDownload').live('click', function(){
        oData = {
            'download' : 1,
            'noti_id' : $(this).attr('data-id')
        };

        Popup.updateStats(oData);

        var oData = {
            'downloaded' : true,
            'noti_id' : $(this).attr('data-id'),
            'option' : 'flags'
        };

        Popup.updateFlags(oData);
    });
});
