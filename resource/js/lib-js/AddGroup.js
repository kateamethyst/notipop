$(document).ready(function() {
    var d = new Date();
    var sDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();
    var fields = 'level';
    var aData = [];
    var Options = {

        getOption : function (iOption) {
            if (iOption === 0) {
               $('.option').html('<select id="lvl" name="lvl"><option value="lvl1">Level 1</option> <option value="lvl2">Level 2</option></select>');
                fields = 'level';
            } else if (iOption === 1) {
               $('.option').html('<select name="age" id="age"  class="slcOption"> <option value="Equals">equals</option> <option value="Is greater than">is greater than</option> <option value="Is greater than or equals">is greater than or equals</option> <option value="Is less than">is less than</option> <option value="Is less than or equals">is less than or equals</option> <option value="Between">between</option> </select> <input type="number" placeholder="From" class="fText" name="start_age" id="start_age"> <input type="number" style="display:none;" placeholder="To" class="fText" name="end_age" id="end_age">');
                fields = 'age';
            } else if (iOption === 2) {
                $('.option').html('<select name="gender" id="gender"> <option value="male">Male</option> <option value="female">Female</option> </select>');
                fields = 'gender';
            } else if (iOption === 3) {
                $('.option').html('<select name="point" id="point"  class="slcOption"> <option value="Equals">equals</option> <option value="Is greater than">is greater than</option> <option value="Is greater than or equals">is greater than or equals</option> <option value="Is less than">is less than</option> <option value="Is less than or equals">is less than or equals</option> <option value="Between">between</option> </select> <input type="number" placeholder="From" class="fText" name="start_point" id="start_point"> <input type="number" style="display:none;" placeholder="To" class="fText" name="end_point" id="end_point">');
                fields = 'points';
            } else if (iOption === 4) {
                $('.option').html('<select name="registration" id="registration"  class="slcOption"> <option value="Equals">equals</option> <option value="Is greater than">is greater than</option> <option value="Is greater than or equals">is greater than or equals</option> <option value="Is less than">is less than</option> <option value="Is less than or equals">is less than or equals</option> <option value="Between">between</option> </select><input type="text" id="srh_start_dt" name="srh_start_dt" class="fText gDate txtStartDate" required value="2015-09-02" > <a id="start_link_registration" href="javascript:;" id="start_date" class="btnIcon icoCal img_cal3"> <span>달력보기</span> </a> <div id="end_registration" style="display:none"> ~ <input type="text" id="srh_end_dt" name="srh_end_dt" class="fText gDate txtEndDate" required value=""><a href="javascript:;" id="end_link_registration" class="btnIcon icoCal img_cal4"><span>달력보기</span></a></div>');
                fields = 'registration';
            } else if (iOption === 5) {
                $('.option').html('<label class="gLabel"><input type="radio" value="T" name="email" class="fChk email" checked>All</label><label class="gLabel"><input type="radio" value="T" name="email" class="fChk email" >Subscriber</label><label class="gLabel"><input type="radio" value="F" name="email" class="fChk email">Unsubscriber</label><label class="gLabel"><input type="radio" value="F" name="email" class="fChk email">Absolutely unsubscriber</label>');
                fields = 'email';
            } else {
                $('.option').html('<select name="optionAnniv" id="optionAnniv"><option value="Birthday">Birthday</option> <option value="Wedding Anniversary ">Wedding Anniversary</option> <option value="Spouse`s Birthday">Spouse`s Birthday</option> </select> <select name="anniversary" id="anniversary" class="slcOption"> <option value="Equals">equals</option> <option value="Is greater than">is greater than</option> <option value="Is greater than or equals">is greater than or equals</option> <option value="Is less than">is less than</option> <option value="Is less than or equals">is less than or equals</option> <option value="Between">between</option> </select><input type="text" id="srh2_start_dt" name="srh2_start_dt" class="fText gDate txtStartDate" required value="2015-09-02" > <a id="start_link_anniversary" href="javascript:;" id="start_date" class="btnIcon icoCal img_cal3"> <span>달력보기</span> </a> <div id="end_anniversary" style="display:none"> ~ <input type="text" id="srh2_end_dt" name="srh2_end_dt" class="fText gDate txtEndDate" required value=""><a href="javascript:;" id="end_link_anniversary" class="btnIcon icoCal img_cal4"><span>달력보기</span></a></div>');
                fields = 'anniversary';
            }
        },

        getData : function (sFields) {
            if (sFields === 'level') {
                var tempData = {
                    'criteria' : 'Shopping mall customer level',
                    'details' : {
                        'condition' : 'Equals',
                        'data' : {
                            0 : $('#lvl').val()
                        }
                    }
                };
            } else if (sFields === 'age') {
                var tempData = {
                    'criteria' : 'Age',
                    'details' : {
                        'condition' : $('#age').val(),
                        'data' : {
                           0 : parseInt($('#start_age').val()),
                           1 : parseInt($('#end_age').val())
                        }
                    }
                };
            } else if (sFields === 'gender') {
                var tempData = {
                    'criteria' : 'Gender',
                    'details' : {
                        'condition' : 'Equals',
                        'data' : {
                            0 : $('#gender').val(),
                        }
                    }
                };
            } else if (sFields === 'points') {
                var tempData = {
                    'criteria' : 'Points',
                    'details' : {
                        'condition' : $('#point').val(),
                        'data' : {
                            0 : parseInt($('#start_point').val()),
                            1 :parseInt($('#end_point').val())
                        }
                    }
                };
            } else if (sFields === 'email') {
                var tempData = {
                    'criteria' : 'Email',
                    'details' : {
                        'condition' : 'Equals',
                        'data' : {
                            0 : $("input[name='email']:checked").val()
                        }
                    }
                };
            } else if (sFields === 'registration') {
                var tempData = {
                    'criteria' : 'Date of Registration',
                    'details' : {
                        'condition' : $('#registration').val(),
                        'data' : {
                            0 : $('#srh_start_dt').val(),
                            1 : $('#srh_end_dt').val()
                        }
                    }
                };
            } else {
                var tempdata = {
                    'criteria' : 'Anniversary',
                    'details' : {
                        'type' : $('#optionAnniv').val(),
                        'condition' : $('#anniversary').val(),
                        'data' : {
                            0 : $('#srh2_start_dt').val(),
                            1 : $('#srh2_end_dt').val()
                        }
                    }
                };
            }

            //Push data in global variable this will be used in saving the conditions
            aData.push(tempData);

            Options.appendData(tempData);
        },

        appendData : function (aData) {
            if (aData.details.data[1]) {
                var details = aData.details.data[0] + ' ~ ' + aData.details.data[1];
            } else {
                var details = aData.details.data[0];
                aData.details.data[1] = null;
            }

            // if (aData.details.type){
            //     type = aData.details.type;
            // } else {
            //     var type = '';
            // }
            $('.conditionList').append('<tr><td class="mainOption">' + aData.criteria + '</td> <td>' + aData.details.condition + '</td> <td> ' + details + '</td> <td style="display:none";>' + aData.details.data[0] + '</td> <td style="display:none";>' + aData.details.data[1] + '</td> <td> <!-- <a href="#" class="btnNormal" id="btnAdd" data-id="0"><span>&#10004;</span></a><a href="#" class="btnNormal" id="btnAdd" data-id="0"><span>&#9998;</span></a> --><a href="#" class="btnNormal" id="btnDelete" data-id="0"><span>&#10006;</span></a> </td> </tr>')
        }

    };

    var GroupCriteria = {
        appendBox : function(mResponse) {
            window.opener.$('.groupCriteriaBox').append('<label id="labelCriteria"> <input  type="radio" data-id="' + mResponse.seq + '" value="' + mResponse.seq + '" name="group_no" class="rdbGroupCriteria" class="fChk" checked> <div>' + mResponse.name + '</div> </label>');
        }
    };

    $('#slcCondition').live('change', function(){
        Options.getOption(parseInt($(this).val()));
    });

    $('#btnSaveCondition').live('click', function(){
        Options.getData(fields);
    });

    $('#btnDelete').live('click', function(){
        $(this).parent().parent().remove();
    });

    $('#btnSaveGroupCriteria').live('click', function() {
        var tdContent = [];
        var trContent = [];
        var iCount = 0;
        //get content in table
        $('#groupCriteria').children('tbody').each(function(){
            $(this).children('tr').each(function(){
                $(this).children('td').each(function(){
                    tdContent.push($(this).html());
                });
                trContent.push(tdContent);
                tdContent = [];
            });
        });

        //getTotalCount
        $.ajax({
            type    : 'GET',
            url     : '[link=api/group]',
            encode  : 'json',
            data    : {'options' : 'count'},
            success : function (mResponse) {
                iCount = mResponse.Data;
            }
        });

        if ($('#name').val() === '' || $('#description') === '' || trContent.length === 0) {
            alert('Fill out the required fields');
        } else if (iCount > 10) {
            alert('You already accumulated 10 maximum group criteria.');
        } else {
            var aCriteria = trContent.map(function(obj){
                return {
                 'option' : obj[0],
                 'details' : {
                    'condition' : obj[1],
                    'data' : {
                        0 : obj[3],
                        1 : obj[4]
                     }
                  }
                }            
            });

            var oGroupCriteria = {
                'name' : $('#name').val(),
                'description' : $('#description').val(),
                'criteria' : aCriteria
            };

            $.ajax({
                type    : 'POST',
                url     : '[link=api/group]',
                encode  : 'json',
                data    : oGroupCriteria,
                success : function (mResponse) {
                    GroupCriteria.appendBox(mResponse.Data);
                    window.close();
                }
            });
        }
    });

    $('.slcOption').live('change', function() {
        var sElement = $(this).attr('id');
        if ($(this).val() === 'Between') {
            $('#end_' + sElement).css('display', 'block');

        } else {
            $('#end_' + sElement).css('display', 'none');
            $('#end_' + sElement).val('');
        }
    });

    $('#start_link_registration').live('click', function(){
        var now = new Date();
        var year = '' + now.getFullYear();
        $(this).Calendar({
            input_target : 'input[name=srh_start_dt]',
            years_between : [2010, year]
        });
    });

    $('#end_link_registration').live('click', function(){
        var now = new Date();
        var year = '' + now.getFullYear();
        $(this).Calendar({
            input_target : 'input[name=srh_end_dt]',
            years_between : [2010, year]
        });
    });

    $('#start_link_anniversary').live('click', function(){
        var now = new Date();
        var year = '' + now.getFullYear();
        $(this).Calendar({
            input_target : 'input[name=srh2_start_dt]',
            years_between : [2010, year]
        });
    });

    $('#end_link_anniversary').live('click', function(){
        var now = new Date();
        var year = '' + now.getFullYear();
        $(this).Calendar({
            input_target : 'input[name=srh2_end_dt]',
            years_between : [2010, year]
        });
    });

});
