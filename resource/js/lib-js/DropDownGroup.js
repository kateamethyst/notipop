var iCounter = 0

var row = '';

var aMainOptions = [
    'Shopping mall customer level',
    'Age',
    'Gender',
    'Points',
    'Date of registration',
    'Email Subscribers',
    'Anniversary'
];

var aLvl = [
    'lvl1',
    'lvl2'
];

var aStartAge = {
    'type' : 'number',
    'placeholder' : 'Age',
    'class' : 'fText',
    'name' : 'startAge',
    'id' : 'startAge'
}

var aEndAge = {
    'type' : 'number',
    'placeholder' : 'Age',
    'class' : 'fText',
    'name' : 'endAge',
    'id' : 'endAge'
}

var aMathConditions = [
    'equals',
    'is greater than',
    'is greater than or equals',
    'is less than',
    'is less than or equals',
    'between'
];

var aGender = {
    0 : {
        'type' : 'radio',
        'name' : 'rdbGender',
        'label' : 'Male',
        'value' : 'male'
    },
    1 : {
        'type' : 'radio',
        'name' : 'rdbGender',
        'label' : 'Female',
        'value' : 'female'
    }
};

var aEmail = {
    0 : {
        'type' : 'radio',
        'name' : 'rdbEmail',
        'label' : 'All',
        'value' : 'all'
    },
    1 : {
        'type' : 'radio',
        'name' : 'rdbEmail',
        'label' : 'Subscriber',
        'value' : 'subscriber'
    },
    2 : {
        'type' : 'radio',
        'name' : 'rdbEmail',
        'label' : 'Unsubscriber', 
        'value' : 'unsubscriber'
    },
    3 : {
        'type' : 'radio',
        'name' : 'rdbEmail',
        'label' : 'Absolutely unsubscriber',
        'value' : 'abs_unsubscriber'
    }
};

var aStartPoints = {
     'type' : 'number',
    'placeholder' : 'Points',
    'class' : 'fText',
    'name' : 'startPoints',
    'id' : 'startPoints'
};

var aEndPoints = {
     'type' : 'number',
    'placeholder' : 'Points',
    'class' : 'fText',
    'name' : 'endPoints',
    'id' : 'endPoints'
};

var aAnniversary = [
    'Birthday',
    'Wedding Anniversary',
    'Spouse`s Birthday'
];

var aStartDate = {
    'input' : 'text',
    'name' : 'srh_start_dt' ,
    'id' : 'srh_start_dt',
    'class' : 'txtStartDate',
    'anchor_id' : 'anchor_start_date'
};
var aEndDate = {
    'input' : 'text',
    'name' : 'srh_end_dt' ,
    'id' : 'srh_end_dt',
    'class' : 'txtEndDate',
    'anchor_id' : 'anchor_end_date'
};

$(document).ready(function () {
    function generateRow(iCount) {
        row = '<tr id="trParent' + iCount + '">' + '<td class="tdMainCondition' + iCount + '"></td>' + '<td class="tdConditions' + iCount + '"></td>' + '<td class="tdActions' + iCount + '"><a href="#" class="btnCtrl" id="btnAdd" data-id="' + iCount + '"><span>Add</span></a> <a href="#" class="btnNormal" id="btnDelete" data-id="' + iCount + '"><span>Delete</span></a></td>' + '<tr>';
        $('#tbodyGroup').append(row);
        generateOptions('tdMainCondition' + iCount, 'slcConditions', aMainOptions, iCount);
    }

    function generateOptions(sHtmlElement, sID, aOptions, iCount) {
        $('.' + sHtmlElement).append('<select class="fSelect ' + sID + '" name="' + sID + iCount + '" id="' + sID + iCount + '" data-id="' + iCount + '"></select>');

        $.each(aOptions, function(mKey, mValue) {
            $('#' + sID + iCount).append('<option>' + mValue + '</option>');
        });
    }

    function generateInput(sHTMLElement, aAtrributes, iCount) {
        $('.' + sHTMLElement).append('<input type="' + aAtrributes['type'] + '" placeholder="' + aAtrributes['placeholder'] + '" class="' + aAtrributes['class'] + '" name="' + aAtrributes['name'] + iCount + '" id="' + aAtrributes['id'] + iCount + '">');
    }

    function generateRadioButton(sHTMLElement, aAtrributes, iCount) {
        $.each(aAtrributes, function(mKey, mValue){
            $('.' + sHTMLElement).append('<label class="gLabel"><input type="' + mValue.type + '" value="' + mValue.value + '" name="'+ mValue.name + iCount +'" class="fChk '+ mValue.name + '" checked>' + mValue.label + '</label>')
        })
    }

    function generateCalendar(sHTMLElement, aAtrributes, iCount) {
        $('.' + sHTMLElement).append('<input type="' + aAtrributes['type'] + '" id="' + aAtrributes['id'] + '" name="' + aAtrributes['name'] + '" class="fText gDate ' + aAtrributes['class'] + iCount + '" required=" value="2015-09-02"> <a href="javascript:;" id="' + aAtrributes['anchor_id'] + iCount + '" class="btnIcon icoCal img_cal1"><span>달력보기</span></a>');
    }

    function displayConditions(sCondition, iCount) {
        if (sCondition === 'Shopping mall customer level') {
            generateOptions('tdConditions' + iCount, 'lvl', aLvl, iCount);
        } else if (sCondition === 'Age') {
            generateOptions('tdConditions' + iCount, 'aAgeCondi', aMathConditions, iCount);
            generateInput('tdConditions' + iCount, aStartAge, iCount);
        } else if (sCondition === 'Gender') {
            generateRadioButton('tdConditions' + iCount, aGender, iCount);
        } else if (sCondition === 'Points') {
            generateOptions('tdConditions' + iCount, 'aPointsCondi', aMathConditions, iCount);
            generateInput('tdConditions' + iCount, aStartPoints, iCount);
        } else if (sCondition === 'Email Subscribers') {
            generateRadioButton('tdConditions' + iCount, aEmail, iCount);
        } else if (sCondition === 'Date of registration') {
            generateOptions('tdConditions' + iCount, 'aRegistCondi', aMathConditions, iCount);
            generateCalendar('tdConditions' + iCount, aStartDate, iCount);
        } else {
            generateOptions('tdConditions' + iCount, 'aAnniversary', aAnniversary, iCount);
            generateOptions('tdConditions' + iCount, 'aAnniCalenCondi', aMathConditions, iCount);
            generateCalendar('tdConditions' + iCount, aStartDate, iCount);
        }
    }

    $('#tbodyGroup').show(function() {
        generateRow(iCounter);
        displayConditions($('#slcConditions' + iCounter).val(), iCounter);
    });

    $('.slcConditions').live('change', function(){
        $('.tdConditions' + $(this).attr('data-id')).empty();
        displayConditions($('#slcConditions' + $(this).attr('data-id')).val(), $(this).attr('data-id'));
    });

    $('#btnAdd').live('click', function() {
        iCounter++;
        generateRow(iCounter);
        displayConditions($('#slcConditions' + iCounter).val(), iCounter);
    });

    $('#btnDelete').live('click', function() {
        if ($(this).attr('data-id') !== '0') {
            $('#' + 'trParent' + $(this).attr('data-id')).remove();
        }
    });

    $('.btnSubmit').click(function() {
        console.log(iCounter);
        // for (var i = iCounter; i >= 0; i--) {
        //     if ($('#slcConditions' + i).val() === 'Age') {
        //         console.log($('#age' + i).val());
        //     }
        // }
    });

    $('.aAgeCondi').live('change', function() {
        var sDropdownValue = $('#aAgeCondi' + $(this).attr('data-id')).val();
        if (sDropdownValue === 'between') {
            generateInput('tdConditions' + $(this).attr('data-id'), aEndAge, $(this).attr('data-id'));
        } else {
            $('#endAge' + $(this).attr('data-id')).remove();
        }
    });

    $('.aPointsCondi').live('change', function() {
        var sDropdownValue = $('#aPointsCondi' + $(this).attr('data-id')).val();
        if (sDropdownValue === 'between') {
            generateInput('tdConditions' + $(this).attr('data-id'), aEndPoints, $(this).attr('data-id'));
        } else {
            $('#endPoints' + $(this).attr('data-id')).remove();
        }
    });

    $('.aAnniCalenCondi').live('change', function() {
        var sDropdownValue = $('#aAnniCalenCondi' + $(this).attr('data-id')).val();
        if (sDropdownValue === 'between') {
            generateCalendar('tdConditions' + $(this).attr('data-id'), aEndDate, $(this).attr('data-id'));
        } else {
            $('.txtEndDate' + $(this).attr('data-id')).remove();
            $('#anchor_end_date' + $(this).attr('data-id')).remove();
        }
    });

    $('.aRegistCondi').live('change', function() {
        var sDropdownValue = $('#aRegistCondi' + $(this).attr('data-id')).val();
        if (sDropdownValue === 'between') {
            generateCalendar('tdConditions' + $(this).attr('data-id'), aEndDate, $(this).attr('data-id'));
        } else {
            $('.txtEndDate' + $(this).attr('data-id')).remove();
            $('#anchor_end_date' + $(this).attr('data-id')).remove();
        }
    });
});
