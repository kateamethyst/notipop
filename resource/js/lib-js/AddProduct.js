$(document).ready(function() {


    function AddProductPageMethod() {
        return {
            oCategoryList : {},
            aAddedProductNo : [],
            bSlideChange : false,
            bModelChange : false,
            sDepth1 : '.prdCategory1',
            sDepth2 : '.prdCategory2',
            sDepth3 : '.prdCategory3',
            sDepth4 : '.prdCategory4',
            sSearchedList : '.gProductList',
            sSearchedTotal : '.gTotal .total_record',
            sSortMethod : 'select[name="sort_method"]',

            init : function() {
                var self = this;
                self.event();
                var oCategoryData = <?= $oCategoryData ?>;
                if (Object.keys(oCategoryData).length > 0) {
                    self.setCategoryData(0, oCategoryData);
                }
                self.firstDepthAppend();

                var oDepthParam = <?= $oDepthList ?>;
                if (Object.keys(oDepthParam).length > 0) {
                    self.isDepthValueform(oDepthParam);
                }
                self.SearchCategoryChange();
                self.checklistEventFunction();
            },

            setCategoryData : function(iParentNo, aData) {
                var self = this;
                self.oCategoryList[iParentNo] = {};
                $.each(aData, function(iKey, aRow) {
                    self.oCategoryList[iParentNo][iKey] = aRow['categoryName'].replace('\"', '');
                    self.setCategoryData(iKey, aRow['childData']);
                });
            },
            firstDepthAppend : function () {
                var self = this;

                if ( self.oCategoryList[0] ) {
                    $.each(self.oCategoryList[0], function(key, value) {
                        $('<option value="' + key + '" class="item">' + value + '</option>').appendTo($(self.sDepth1));
                    });
                }
            },
            //form 으로 쐈을때 값 유지 페이징시 유지
            isDepthValueform : function (aDepthParam) {
                var self = this;
                var sDepth1Val = aDepthParam['depth1'];
                var sDepth2Val = aDepthParam['depth2'];
                var sDepth3Val = aDepthParam['depth3'];
                var sDepth4Val = aDepthParam['depth4'];

                if (typeof(sDepth1Val) !== "object" && sDepth1Val !== "null") {
                    $('.prdCategory1').val(sDepth1Val).attr("selected", "selected");

                    $.each(self.oCategoryList[sDepth1Val], function(key, value) {
                        $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth2);
                    });
                }

                if (typeof(sDepth2Val) !== "object" && sDepth2Val !== "null") {
                    $('.prdCategory2').val(sDepth2Val).attr("selected", "selected");

                    $.each( self.oCategoryList[sDepth2Val], function(key, value) {
                        $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth3);
                    });
                }

                if (typeof(sDepth3Val) !== "object" && sDepth3Val !== "null") {
                    $('.prdCategory3').val(sDepth3Val).attr("selected", "selected");

                    $.each( self.oCategoryList[sDepth3Val], function(key, value) {
                        $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth4);
                    });
                }

                if (typeof(sDepth4Val) !== "object" && sDepth4Val !== "null") {
                    $('.prdCategory4').val(sDepth4Val).attr("selected", "selected");
                }
            },
            makeOptionValue : function (sOptionCheck) {
                if (sOptionCheck === 'brand_code') {
                    var sOptionType = 'brand';
                } else if (sOptionCheck === 'trend_code') {
                    var sOptionType = 'trend';
                } else if (sOptionCheck === 'manufacturer_code') {
                    var sOptionType = 'manufacturer';
                } else if (sOptionCheck === 'supplier_id') {
                    var sOptionType = 'supplier';
                }

                return [sOptionType + '_name', sOptionType + '_code'];
            },
            SearchCategoryChange : function () {
                var self = this;
                $(document).delegate('#searchCategory', 'change', function() {
                    var sOptionCheck = $(this).val();
                    // 브랜드 제조사 경우

                    //이것은 제조사로 골랐다가  상품명이나 상품 번호로 바꿔준 경우
                    if ( sOptionCheck !== 'trend_code' && sOptionCheck !== 'manufacturer_code' && sOptionCheck !== 'supplier_id' && sOptionCheck !== 'brand_code') {
                        if ($(this).siblings("#searchCategoryselect").length > 0 ) {

                            $(this).siblings("#searchCategoryselect").remove();
                        }
                        $(this).siblings('input').remove();
                        var sInputSelector = "<span> </span><input type='text' style='width:300px;' class='fText' name= 'search[]' > </input>";

                        $(sInputSelector).insertAfter(this);
                    }

                    if ( sOptionCheck === 'trend_code' || sOptionCheck === 'manufacturer_code' || sOptionCheck === 'supplier_id' || sOptionCheck === 'brand_code') {
                        //자기 옆에것만 지워져야한다.
                        if ($(this).siblings("#searchCategoryselect").length > 0 ) {
                            $(this).siblings("#searchCategoryselect").remove();
                        }

                        var aListName = [];
                        var aListValue = [];
                        var aOptionResult = [];
                        aOptionResult = self.makeOptionValue(sOptionCheck);
                        var sOptionName = aOptionResult[0];
                        var sOptionCode = aOptionResult[1];

                        $.ajax({
                            type : "GET",
                            url : "[link=api/AddProduct]",
                            dataType : "json",
                            async : false,
                            error : function (err) {
                                alert('err\n리스트 출력실패');
                            },
                            data : {'CategoryType' : sOptionCheck},
                            success : function (response) {
                                //성공 코드
                                if (response !== false) {
                                    //성공 코드
                                    if (response.Data.meta['code'] == 200) {
                                        resultData = response['Data']['response']['result'];
                                        for (iiIndex in resultData) {
                                            aListName[iiIndex] = resultData[iiIndex][sOptionName];
                                            aListValue[iiIndex] = resultData[iiIndex][sOptionCode];
                                        }
                                    }
                                } else {
                                    alert('리스트 출력 실패');
                                    return false;
                                }
                            }
                        });

                        // 헤당 이름은  alist[iIndex]안에다가  value는 코드로
                        for (iIndex in aListName) {
                            var sOption = "<option value=" + aListValue[iIndex] + ">" + aListName[iIndex] + "</option>";
                            var sResultOption = sResultOption + sOption;
                        }

                        var sSelector = '<span> </span><select class="fSelect" id="searchCategoryselect" name="search[]" >' + sResultOption + '</select>';
                        $(this).siblings('input').remove();
                        $(this).after(sSelector);
                    }
                });
            },
            checklistEventFunction : function () {
                $(document).delegate('.allrowCheckList', 'change', function(){
                    $('.rowCheckList').attr('checked', this.checked);
                });

                //체크리스트  form 버튼
                $(".btnSubmit").click(function() {
                    var checkedList = [];
                    var aProductInfo = {};
                    var aOption = [];
                    $("input:checkbox[name='rowCheckList[]']:checked").each(function(iiIndex) {
                        var oRow = $(this).closest('tr');
                        aProductInfo['product_no'] = $(oRow).find("input[name='product_no[]']").val();
                        aProductInfo['is_set_product'] = $(oRow).find("input[name='is_set_product[]']").val();
                        aProductInfo['product_code'] = $(oRow).find("input[name='product_code[]']").val();
                        aProductInfo['tiny_image'] = $(oRow).find("input[name='tiny_image[]']").val();
                        aProductInfo['product_name'] = $(oRow).find("input[name='product_name[]']").val();
                        aProductInfo['product_price'] = $(oRow).find("input[name='product_price[]']").val();
                        aProductInfo['product_classification'] = $(oRow).find("input[name='product_classification[]']").val();
                        var aOptionLi = $(oRow).find("li");
                        $(aOptionLi).each(function() {
                            aOption.push($(this).html());
                        });
                        aProductInfo['option'] = aOption;
                        checkedList.push(aProductInfo);
                        Product.appendProduct(aProductInfo);
                    });
                    var iAddCount = checkedList.length;
                    $(".eAddCount").html(iAddCount);
                });
            },
            event : function () {
                var self = this;
                $(document).delegate('.ePlus', 'click', function(){
                    if ($('#typeVer #item').length < 10) {
                        var div = document.createElement('div');
                        div.id = 'item';
                        div.innerHTML = document.getElementById('baseitem').innerHTML;
                        document.getElementById('typeVer').appendChild(div);
                    }
                });

                $(document).delegate('.eMinus', 'click', function(){
                    if ($('#typeVer #item').length > 1) {
                        $(this).closest('div').remove();
                    }
                });

                $('#limit').change(function(){
                    var sUrl = 'addProduct?page=1&' + $('#productAddForm').serialize();
                    sUrl += '&limit=' + $(this).val();
                    location.href = sUrl;
                });

                // set date event
                $('.eDateBtn').click(function() {
                    $('.btnSrhDate').removeClass('selected');

                    $(this).addClass('selected');
                    $('#srh_start_dt').attr('value', $(this).attr('start_dt'));
                    $('#srh_end_dt').attr('value', $(this).attr('end_dt'));
                });

                // form 검색 버튼
                $('.eSearch').click(function() {

                    // start dt
                    var aStartDt = $('input[name=srh_start_dt]').val().split('-');
                    var sStartDt = new Date(aStartDt[0], aStartDt[1], aStartDt[2]).valueOf();

                    // end dt
                    var aEndDt = $('input[name=srh_end_dt]').val().split('-');
                    var sEndDt = new Date(aEndDt[0], aEndDt[1], aEndDt[2]).valueOf();

                    //기간 체크
                    if (sStartDt > sEndDt) {
                        alert('기간을 정확하게 설정해 주세요');
                        return false;
                    }

                    var fname = ('productAddForm');
                    var form = document.getElementById(fname);

                    form.submit();
                });

                //checkAllState 전체 체크 체크했을경우
                $('#checkAllState').click( function(){
                    if ($('#checkAllState').attr('checked') ) {
                        $('#state1').attr('checked', true);
                        $('#state2').attr('checked', true);
                        $('#state3').attr('checked', true);
                        $('#state4').attr('checked', true);
                        $('.glabelstate').addClass('eSelected');
                        $('.glabelallstate').addClass('eSelected');
                    } else {
                        $('.glabelstate').removeClass('eSelected')
                        $('#state1').attr('checked', false);
                        $('#state2').attr('checked', false);
                        $('#state3').attr('checked', false);
                        $('#state4').attr('checked', false);
                    }
                });

                $('#state1, #state2, #state3, #state4').change(function(){
                    var bCheckBox1 = $('#state1').attr('checked');
                    var bCheckBox2 = $('#state2').attr('checked');
                    var bCheckBox3 = $('#state3').attr('checked');
                    var bCheckBox4 = $('#state4').attr('checked');

                    //한개 가 비었을때는  전체는 체크에서 빼주는것
                    if (bCheckBox1 === false || bCheckBox2 === false || bCheckBox3 === false || bCheckBox4 === false) {
                        $('#checkAllState').attr('checked', false);
                        $('.glabelallstate').removeClass('eSelected');
                    }
                });

                $(self.sDepth1).change( function() {
                    var iParentNo = this.value;
                    $(self.sDepth2).find('.item').remove();
                    console.log(iParentNo);
                    if ( iParentNo !== 'null' ) {
                        $.each(self.oCategoryList[iParentNo], function (key, value) {
                            $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth2);
                        });
                    };

                    $(self.sDepth3).find('.item').remove();
                    $(self.sDepth4).find('.item').remove();
                });

                $(self.sDepth2).change( function() {
                    var iParentNo = this.value;
                    $(self.sDepth3).find('.item').remove();
                    if (iParentNo !== 'null') {
                        $.each(self.oCategoryList[iParentNo], function (key, value) {
                            $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth3);
                        });
                    };
                    $(self.sDepth4).find('.item').remove();
                });

                $(self.sDepth3).change( function() {
                    var iParentNo = this.value;
                    $(self.sDepth4).find('.item').remove();
                    if (iParentNo !== 'null') {
                        $.each(self.oCategoryList[iParentNo], function (key, value) {
                            $('<option value="' + key + '" class="item">' + value + '</option>').appendTo(self.sDepth4);
                        });
                    };
                });
            }
        }
    }

    var oAddProductPageMethod = new AddProductPageMethod();
    oAddProductPageMethod.init();
});
