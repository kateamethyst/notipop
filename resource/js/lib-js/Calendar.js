/**
 * index 화면 스크립트
 */
$(document).ready(function(){

    var now = new Date();
    var year = '' + now.getFullYear();

    $('.img_cal1').Calendar({
        input_target : 'input[name=srh_start_dt]',
        years_between : [2010, year]
    });

    $('.img_cal2').Calendar({
        input_target : 'input[name=srh_end_dt]',
        years_between : [2010, year]
    });
});
