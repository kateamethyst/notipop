/**
 * [aSelectedProduct description]
 * @type {Array}
 */
var aSelectedProduct = [];

/**
 * [mResponse description]
 * @type {Array}
 */
var mResponse = [];

/**
 * [counter description]
 * @type {Number}
 */
var counter = 0;

/**
 * [values description]
 * @type {Array}
 */
var values = [];

/**
 * [mStatus description]
 * @type {String}
 */
var mStatus = '';

/**
 * [oData description]
 * @type {String}
 */
var oData = new Object();

/**
 * [Display description]
 * @type {Object}
 */
var Display = {

    displaySelectedProduct : function (event) {
        values = [];
        values.length = 0;
        if (counter === 0 ) {
            $("input[name='product_no[]']:checked").parent().parent().children().each(function(){
                values.push($(this).text());
            });
            $('#divProd').html("<div class='mFlex' id='prodPreview'> <div><img class='imgBorder' src='" + values[6] + "' width='100' height='100'></div> <div> <h2>" + values[4] + "</h2> <p>" + values[5] + " KRW</p></div> </div>");
            counter = 1;
        } else {
            counter = 0;
            $('#divProd').html('');
        }
    },

    displaySelectedType : function (sNotiType) {
        if (sNotiType === 'TEXT') {
            $('.secCoupon').css('display', 'none');
            $('#btnCoupon').css('display', 'none');
            $('.secProducts').css('display', 'none');
            $('.secRegistration').css('display', 'none');
            $('#register').css('display', 'none');
            $('#divProd').html('');
        } else if (sNotiType === 'Coupon') {
            $('.secCoupon').css('display', 'table-row');
            $('#btnCoupon').css('display', 'inline');
            $('.secProducts').css('display', 'none');
            $('.secRegistration').css('display', 'none');
            $('#register').css('display', 'none');
            $('#divProd').html('');
        } else if (sNotiType === 'Advertisement') {
            $('.secCoupon').css('display', 'none');
            $('#btnCoupon').css('display', 'none');
            $('.secProducts').css('display', 'table-row');
            $('.secRegistration').css('display', 'none');
            $('#register').css('display', 'none');
        } else if (sNotiType === 'Login/Registration') {
            $('.secCoupon').css('display', 'none');
            $('#btnCoupon').css('display', 'none');
            $('.secProducts').css('display', 'none');
            $('.secRegistration').css('display', 'table-row');
            $('#register').css('display', 'inline');
            $('#divProd').html('');
        }
    }
}

/**
 * [product description]
 * @return {[type]} [description]
 */
var Product = {
    addProduct : function (aProducts) {
        $.ajax({
            type    : 'POST',
            encode  : 'json',
            url     : '[link=api/product]',
            data    : aProducts,
            success : function (mResponse) {
                if (mResponse.Data !== false) {
                    Product.appendProduct(mResponse.Data);
                }
            }
        });
    },

    appendProduct : function (aProducts) {
        window.opener.$('#tbodySelectedProd').append('<tr> <td> <input type="checkbox" class="products" name="product_no[]" onclick="Display.displaySelectedProduct(this.value);" value="' + aProducts.product_no + '"> </td> <td><input type="hidden" class="fText" name="product_num" value="' + aProducts.product_no + '">' + aProducts.product_no + '</td> <td><input class="fText" type="hidden" name="product_classification" value="' + aProducts.product_classification + '">' + aProducts.product_classification + '</td> <td><input class="fText" type="hidden" name="product_code" value="' + aProducts.product_code + '">' + aProducts.product_code + '</td> <td><input class="fText" type="hidden" name="tiny_image" value="' + aProducts.tiny_image + '"><input class="fText" type="hidden" name="product_name" value="' + aProducts.product_name + '"><img src="' + aProducts.tiny_image + '" class="imgBorder" width="40" height="40">' + aProducts.product_name + '</td> <td><input class="fText" type="hidden" name="product_price" value="' + aProducts.product_price + '">' + aProducts.product_price + '</td> <td style="display:none;">' + aProducts.tiny_image + '</td> </tr>');
    },

    deleteProd : function (event) {
        var aCheckedValues = $('input:checkbox:checked').map(function() {
            return $.trim(this.value);
        }).get(aCheckedValues);

        oData = {
            'products' : aCheckedValues,
            'method' : 'DELETE'
        };

        var parentDiv = document.getElementById('tbodySelectedProd');

        $.ajax({
            type    : 'GET',
            encode  : 'json',
            url     : '[link=api/product]',
            data    : oData,
            success : function (mResponse) {
                $.each(mResponse.Data, function (sKey, aValues) {
                    $('#prod' + aValues).remove();
                });
            }
        });
    }
}

$(document).ready(function() {
    /**
     * [sContents description]
     * @type {String}
     */
    var sContents = '';

    /**
     * [sCriteria description]
     * @type {String}
     */
    var sCriteria = '';

    /**
     * [sCriteriaName description]
     * @type {String}
     */
    var sCriteriaName = '';

    /**
     * [page description]
     * @type {String}
     */
    var page = '';

    /**
     * [d description]
     * @type {Date}
     */
    var d = new Date();

    /**
     * [aMergeNotiInfo description]
     * @type {Array}
     */
    var aMergeNotiInfo = [];

    /**
     * [sDate description]
     * @type {[type]}
     */
    var sDate = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();

    /**
     * [Noti description]
     * @type {Object}
     */
    var Noti = {
        setUpStatus : function (iSeq, mStatus) {
            oData = {
                'purpose' : 'updateStatus',
                'method' : 'PUT',
                'seq'    : iSeq,
                'use' : mStatus
            };
            $.ajax({
                type    : 'POST',
                url     : '[link=api/noti]',
                encode  : 'json',
                data    : oData,
                success : function () {
                    alert('Youy have successfully updated the status of the popup.');
                    window.location.href = '[link=admin/index]';
                }
            });
        },

        addNoti : function (oData) {
            var error = 0;

            error = Noti.checkErrors();

            if (error !== 0) {
                alert('Fill out the required fields.');
            }
        },

        checkErrors : function () {
            var iErrorCounter = 0;

            if ($('#txtNotiName').val() === '') {
                iErrorCounter++;
            }
            if ($('#txtTitle').val() === '') {
                iErrorCounter++;
            }
            if ($('#txtContent').val() === '') {
                iErrorCounter++;
            }

            return iErrorCounter;
        },

        deleteNoti : function (aCheckedValues) {
            oData = {
                'seq' : aCheckedValues,
                'method' : 'DELETE'
            };

            $.ajax({
                type : 'GET',
                encode : 'json',
                url : '[link=api/noti]',
                data : oData,
                success : function() {
                    alert('Successfully deleted.');
                    window.location.href = '[link=admin/index]';
                }
            });
        },

        cacheUpdate : function() {
            oData = {
                'method' : 'clear'
            };

            $.ajax({
                type : 'GET',
                data : oData,
                encode : 'json',
                url : '[link=api/noti]',
                success : function() {
                    alert('Successfully updated cached notifications');
                }
            });
        }
    }

    /**
     * [Member description]
     * @type {Object}
     */
    var Member = {
        getMembers : function () {
            oData = {
                'method' : 'getMemberList'
            };
            $.ajax({
                type : 'GET',
                encode : 'json',
                url : '[link=api/member]',
                data : oData,
                success : function(mResponse) {
                    $('#tbodyMember').empty();
                    $('#secGroupList').css('display', 'block');
                    $.each(mResponse.Data.response.result.list, function(mKey, mValue){
                       $('#tbodyMember').append('<tr> <td>' + mValue.member_id + '</td> <td>' + mValue.name + '</td> <td></td> <td></td> <td>' + mValue.addr1 + '</td> <td>' + mValue.email + '</td> <td>' + mValue.c_phone + '</td> <tr>');
                    });
                }
            });
        }
    }

    $('#btnStatus').live('click', function() {
        Noti.setUpStatus($(this).attr('data-id'), $(this).attr('value'));
    });

    $('#rdbText').click(function() {
        Display.displaySelectedType($(this).attr('value'));
    });

    $('#rdbCoupon').click(function() {
        Display.displaySelectedType($(this).attr('value'));
    });

    $('#rdbProducts').click(function() {
       Display.displaySelectedType($(this).attr('value'));
    });

    $('#rdbRegistration').click(function() {
        Display.displaySelectedType($(this).attr('value'));
    });

    $('#txtTitle').keyup(function(event) {
        $('#title').text($(this).val());
    });

     $('#txtContent').keyup(function(event) {
        $('#descritpion').text($(this).val());
    });

    $('#rdbAddBtn').click(function() {
        $('#register').css('visibility', 'visible');
    });

    $('#rdbRemoveBtn').click(function() {
        $('#register').css('visibility', 'hidden');
    });

    $('#optTitle0').select(function() {
        $('#mLayerTest2').css('display', 'none');
    });

    $('#btnSaveNoti').click(function() {
        Noti.addNoti();
    });

    $('#slcTitles').change(function() {
       $('#txtTitle').val($('#txtTitle').val() + $(this).val());
       $('#mLayerTest2').css('visibility', 'hidden');
    });

    $('.eLayerClick2').click(function() {
         $('#mLayerTest2').css('visibility', 'visible');
    });

    $('#slcCntents').change(function() {
       $('#txtContent').val($('#txtContent').val() + $(this).val());
       $('#mLayerTest3').css('visibility', 'hidden');
    });

    $('.eLayerClick3').click(function() {
         $('#mLayerTest3').css('visibility', 'visible');
    });

    $('#btnDeleteNoti').click(function(){
        var aCheckedValues = $('input:checkbox:checked').map(function() {
            return $.trim(this.value);
        }).get();
        Noti.deleteNoti(aCheckedValues);
    });

    $('#btnDeleteNoti2').click(function(){
        var aCheckedValues = $('input:checkbox:checked').map(function() {
            return $.trim(this.value);
        }).get();
        Noti.deleteNoti(aCheckedValues);
    });

    $('#btnDelProd').live('click', function() {
        Product.deleteProd(this);
    });

    $('#btnSearch').click(function(){
        Member.getMembers();
    });

    $('#btnUpdateNoti').live('click', function(){
        Noti.updateNoti($(this));
    });

    $('#secAddNoti').show(function(){
        var sNotiType = $("input[name='notifTab']:checked").val();
        Display.displaySelectedType(sNotiType);
        var prodNo = $('#productNo').val();
        if (sNotiType === 'Advertisement') {
            $('#' + prodNo).attr("checked", true);
            Display.displaySelectedProduct($('#' + prodNo).val());
        }
    });

    $('#btnClearCache').click(function(){
        Noti.cacheUpdate();
    });

    $('#limit').change(function(){
        var sUrl = 'index?page=1&';
        sUrl = sUrl + '&limit=' + $(this).val();
        location.href = sUrl;
    });

    $('#labelCriteria').live('dblclick', function() {
        $('.setting').toggle(2000).css('display', 'block');
    })

    $('#btnDeleteGroup').live('click', function(){
        var oData = {
            'method' : 'delete',
            'seq' : $("input[name='group_no']:checked").val()
        };
        // console.log(oData);
        var bAnswer = confirm('Are you sure you want to delete this group criteria?');
        if (bAnswer === true) {
            $.ajax({
                type : 'GET',
                encode : 'json',
                url : '[link=api/group]',
                data : oData,
                success : function(mResponse) {
                    $("label[data-id='" + oData.seq + "']").remove();
                }
            });
        }
    });

});
